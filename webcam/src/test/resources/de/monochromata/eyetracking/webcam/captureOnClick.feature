Feature: Capture on click

 Scenario: Capture on click
 
 Given a test webcam with empty images that sends images every 50ms
   And a CaptureOnClick instance with registered capture listeners
  When there is a delay of 300ms
   And there is a manual context at 300x200px
  Then there is at least 1 image without a manual context
   And there is 1 image with manual context 300x200px