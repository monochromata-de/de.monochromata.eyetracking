Feature: Sarxos webcam
	How to configure and use a Sarxos webcam.

@physicalWebcamRequired
Scenario: Get image from default webcam.
Given A SarxosWebcam is available 
When an image is obtained
Then the buffered image should be non-null
 And the buffered image should have the dimension of the webcam's view size