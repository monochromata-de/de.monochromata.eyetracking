package de.monochromata.eyetracking.webcam.stubs;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.function.Consumer;

import de.monochromata.eyetracking.webcam.Webcam;

/**
 * A webcam whose methods return {@literal null} and do nothing.
 */
public class NoopWebcam implements Webcam {

	/**
	 * @return {@literal null}
	 */
	@Override
	public BufferedImage getImage() {
		return null;
	}

	/**
	 * @return {@literal null}
	 */
	@Override
	public Dimension getViewSize() {
		return null;
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void addImageListener(Consumer<BufferedImage> imageListener) {		
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void removeImageListener(Consumer<BufferedImage> imageListener) {
	}

}
