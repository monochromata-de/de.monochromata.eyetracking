package de.monochromata.eyetracking.webcam;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * A wrapper to run Cucumber tests with JUnit.
 */
@RunWith(Cucumber.class)
// Cucumber scenarios that require a physical webcam are disabled, because
// Gitlab CI does not have a webcam.
@CucumberOptions(tags = "~@physicalWebcamRequired")
public class RunCucumberTest {

}
