package de.monochromata.eyetracking.webcam;

import static org.assertj.core.api.Assertions.assertThat;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.webcam.state.WebcamState;
import de.monochromata.eyetracking.webcam.tools.CaptureOnClick;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CaptureOnClickStepdefs {

    private final List<Pair<BufferedImage, Point>> imagesWithManualContext = new LinkedList<>();
    private final List<BufferedImage> imagesWithoutManualContext = new LinkedList<>();
    private final AtomicReference<Consumer<Point>> manualContextConnector = new AtomicReference<>();
    private final WebcamState webcamState;
    private CaptureOnClick captureOnClick;

    public CaptureOnClickStepdefs(final WebcamState webcamState) {
        this.webcamState = webcamState;
    }

    @Given("^a CaptureOnClick instance with registered capture listeners$")
    public void instance() {
        captureOnClick = new CaptureOnClick(manualContextConnector::set, webcamState.getWebcam());
        captureOnClick.addListenerForImagesWithManualContext(
                (image, manualContext) -> imagesWithManualContext.add(new ImmutablePair<>(image, manualContext)));
        captureOnClick.addListenerForImagesWithoutManualContext(imagesWithoutManualContext::add);
    }

    @When("^there is a delay of (\\d+)ms$")
    public void delay(final Integer delayMs) {
        synchronized (this) {
            try {
                wait(delayMs);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    @When("^there is a manual context at (\\d+)x(\\d+)px$")
    public void getManualContext(final Integer x, final Integer y) {
        manualContextConnector.get().accept(new PointImpl(x, y));
    }

    @Then("^there is at least (\\d+) image without a manual context$")
    public void assertNumberOfImages(final Integer size) {
        assertThat(imagesWithoutManualContext).hasAtLeastOneElementOfType(BufferedImage.class);
    }

    @Then("^there is (\\d+) image with manual context (\\d+)x(\\d+)px$")
    public void assertImage(final Integer count, final Integer x, final Integer y) {
        assertThat(imagesWithManualContext).hasAtLeastOneElementOfType(Pair.class);
        assertThat(imagesWithManualContext.get(0).getRight()).isEqualTo(new PointImpl(x, y));
    }

}
