package de.monochromata.eyetracking.webcam;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;

import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;
import de.monochromata.eyetracking.webcam.state.WebcamState;
import de.monochromata.eyetracking.webcam.stubs.TestableWebcam;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class WebcamStepdefs {

    private final WebcamState webcamState;
    private BufferedImage image;

    public WebcamStepdefs(final WebcamState webcamState) {
        this.webcamState = webcamState;
    }

    @Given("^A SarxosWebcam is available$")
    public void sarxosWebcam() {
        webcamState.setWebcam(new SarxosWebcam());
    }

    @Given("^a test webcam with empty images that sends images every (\\d+)ms$")
    public void emptyImageTestWebcam(final Integer sendDelayMs) {
        webcamState.setWebcam(new TestableWebcam(sendDelayMs));
    }

    @Given("^a test webcam with images from directory ([^ ]+) that does not send images$")
    public void testInstanceFromDir(final String directoryPath) {
        final List<BufferedImage> images = getImagesFromDirectory(directoryPath);
        final Dimension viewSize = getDimensionOfFirstImage(images);
        webcamState.setWebcam(new TestableWebcam(Integer.MAX_VALUE, images, viewSize));
    }

    @When("^an image is obtained$")
    public void getImage() {
        image = webcamState.getWebcam().getImage();
    }

    @Then("^the buffered image should be non-null$")
    public void assertImageNotNull() {
        assertThat(image).isNotNull();
    }

    @Then("^the buffered image should have the dimension of the webcam's view size$")
    public void assertImageDimensions() {
        assertThat(image.getWidth()).isEqualTo((int) webcamState.getWebcam().getViewSize().getWidth());
        assertThat(image.getHeight()).isEqualTo((int) webcamState.getWebcam().getViewSize().getHeight());
    }

    private Dimension getDimensionOfFirstImage(final List<BufferedImage> images) {
        final BufferedImage firstImage = images.get(0);
        return new Dimension(firstImage.getWidth(), firstImage.getHeight());
    }

    private List<BufferedImage> getImagesFromDirectory(final String directoryPath) {
        try {
            final URL directoryUrl = requireNonNull(getClass().getResource(directoryPath),
                    directoryPath + " not found on classpath");
            final File directory = new File(directoryUrl.toURI());
            return stream(directory.listFiles(file -> file.getName().startsWith("webcam"))).map(this::readImage)
                    .collect(toList());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private BufferedImage readImage(final File file) {
        try {
            return ImageIO.read(file);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
