package de.monochromata.eyetracking.webcam.state;

import de.monochromata.eyetracking.webcam.Webcam;

public class WebcamState {

	private Webcam webcam;

	public Webcam getWebcam() {
		return webcam;
	}

	public void setWebcam(final Webcam webcam) {
		this.webcam = webcam;
	}
	
}
