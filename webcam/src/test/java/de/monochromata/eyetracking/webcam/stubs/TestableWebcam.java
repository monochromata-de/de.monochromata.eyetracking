package de.monochromata.eyetracking.webcam.stubs;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static java.util.Collections.emptyList;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;

import de.monochromata.eyetracking.webcam.Webcam;

/**
 * A testable webcam sends and provides empty {@link BufferedImage}s.
 */
public class TestableWebcam implements Webcam {

	private final Timer timer = new Timer();
	private final List<Consumer<BufferedImage>> listeners = new ArrayList<>();
	private final Iterator<BufferedImage> images;
	private final Dimension viewSize;
	
	/**
	 * Create a test webcam that provides only empty images of size 1x1 pixels
	 * with a delay of sendDelayMs between the images.
	 */
	public TestableWebcam(final int sendDelayMs) {
		this(sendDelayMs, emptyList(), new Dimension(1, 1));
	}
	
	public TestableWebcam(final int sendDelayMs, final List<BufferedImage> images,
			final Dimension viewSize) {
		this.images = images.iterator();
		this.viewSize = viewSize;
		timer.scheduleAtFixedRate(new Task(), sendDelayMs, sendDelayMs);
	}
	
	@Override
	public BufferedImage getImage() {
		if(images.hasNext()) {
			return images.next();
		}
		return createEmptyImage();
	}

	private BufferedImage createEmptyImage() {
		return new BufferedImage(1, 1, TYPE_INT_RGB);
	}

	@Override
	public Dimension getViewSize() {
		return viewSize;
	}

	@Override
	public void addImageListener(final Consumer<BufferedImage> imageListener) {
		listeners.add(imageListener);
	}

	@Override
	public void removeImageListener(final Consumer<BufferedImage> imageListener) {
		listeners.remove(imageListener);
	}
	
	protected void notifyListeners() {
		listeners.forEach(consumer -> consumer.accept(getImage()));
	}

	private class Task extends TimerTask {

		@Override
		public void run() {
			notifyListeners();
		}
		
	}
	
}
