package de.monochromata.eyetracking.webcam.calibration;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.svm.Gaze;

/**
 * @param <I>
 *            The input representation used for calibration. Can be e.g. a
 *            {@link BufferedImage} an intermediate representation, e.g. the
 *            positions that represent the face in an image.
 */
public class ContinuousCalibration<I> {

	private final List<Consumer<ValidationResult>> validationListeners = new ArrayList<>();

	/**
	 * Always contains multiples of {@link #collectionThreshold}, or no element.
	 */
	private final List<Pair<Gaze, Gaze>> data = new LinkedList<>();

	/**
	 * Contains up to {@link #collectionThreshold} (inclusive) elements.
	 */
	private final List<Pair<Gaze, Gaze>> newData;
	private final Executor calibrationExecutor;
	private final Calibration calibration;
	private final FeatureExtractor<I> featureExtractor;
	private final int trainingThreshold;
	private final int collectionThreshold;

	public ContinuousCalibration(final Executor calibrationExecutor, final Calibration calibration,
			final FeatureExtractor<I> featureExtractor, final int trainingThreshold, final int validationThreshold) {
		this.calibrationExecutor = calibrationExecutor;
		this.calibration = calibration;
		this.featureExtractor = featureExtractor;
		this.trainingThreshold = trainingThreshold;
		this.collectionThreshold = trainingThreshold + validationThreshold;
		this.newData = new ArrayList<>(collectionThreshold);
	}

	public void addValidationListener(final Consumer<ValidationResult> validationListener) {
		this.validationListeners.add(validationListener);
	}

	public void removeValidationListener(final Consumer<ValidationResult> validationListener) {
		this.validationListeners.remove(validationListener);
	}

	protected void notifyListeners(final ValidationResult validationResult) {
		validationListeners.forEach(listener -> listener.accept(validationResult));
	}

	public synchronized void addDatum(final I input, final int x, final int y) {
		addNewDatum(input, x, y);
		maybeTriggerCalibration();
	}

	protected void addNewDatum(final I input, final int x, final int y) {
		final EyeFeatures features = featureExtractor.apply(input);
		final Gaze leftGaze = new Gaze(features.getLeftEyeFeatures(), x, y);
		final Gaze rightGaze = new Gaze(features.getRightEyeFeatures(), x, y);
		newData.add(new ImmutablePair<>(leftGaze, rightGaze));
	}

	protected void maybeTriggerCalibration() {
		if (newData.size() >= collectionThreshold) {
			data.addAll(newData);
			final ArrayList<Pair<Gaze, Gaze>> buffer = new ArrayList<>(data.size());
			buffer.addAll(data);
			newData.clear();
			calibrationExecutor.execute(() -> calibrate(buffer));
		}
	}

	private void calibrate(final List<Pair<Gaze, Gaze>> data) {
		final ValidationResult validationResult = calibration.train(getTrainingData(data, Pair::getLeft),
				getValidationData(data, Pair::getLeft), getTrainingData(data, Pair::getRight),
				getValidationData(data, Pair::getRight));
		notifyListeners(validationResult);
	}

	private List<Gaze> getTrainingData(final List<Pair<Gaze, Gaze>> data,
			final Function<Pair<Gaze, Gaze>, Gaze> accessor) {
		return getData(data, accessor, true);
	}

	private List<Gaze> getValidationData(final List<Pair<Gaze, Gaze>> data,
			final Function<Pair<Gaze, Gaze>, Gaze> accessor) {
		return getData(data, accessor, false);
	}

	private List<Gaze> getData(final List<Pair<Gaze, Gaze>> data, final Function<Pair<Gaze, Gaze>, Gaze> accessor,
			final boolean firstHalf) {
		return createRange(data, firstHalf).mapToObj(data::get).map(accessor::apply).collect(toList());
	}

	protected IntStream createRange(final List<Pair<Gaze, Gaze>> data, final boolean firstHalf) {
		final int length = data.size();
		final int boundary = trainingThreshold * (length / collectionThreshold);
		return firstHalf ? range(0, boundary) : range(boundary, length);
	}

}
