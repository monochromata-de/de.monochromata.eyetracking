package de.monochromata.eyetracking.webcam.svm;

import java.util.Arrays;

/**
 * An eye-specific data structure to represent eye features and associated gaze coordinates.
 */
public class Gaze {

	private final double[] eyeFeatures;
	private final double xEstimate;
	private final double yEstimate;
	
	public Gaze(final double[] eyeFeatures, final double xEstimate, final double yEstimate) {
		this.eyeFeatures = eyeFeatures;
		this.xEstimate = xEstimate;
		this.yEstimate = yEstimate;
	}

	public double[] getEyeFeatures() {
		return eyeFeatures;
	}

	public double getXEstimate() {
		return xEstimate;
	}

	public double getYEstimate() {
		return yEstimate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(eyeFeatures);
		long temp;
		temp = Double.doubleToLongBits(xEstimate);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(yEstimate);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gaze other = (Gaze) obj;
		if (!Arrays.equals(eyeFeatures, other.eyeFeatures))
			return false;
		if (Double.doubleToLongBits(xEstimate) != Double.doubleToLongBits(other.xEstimate))
			return false;
		if (Double.doubleToLongBits(yEstimate) != Double.doubleToLongBits(other.yEstimate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Gaze [eyeFeatures=" + Arrays.toString(eyeFeatures) + ", xEstimate=" + xEstimate + ", yEstimate="
				+ yEstimate + "]";
	}
	
}
