package de.monochromata.eyetracking.webcam.svm;

import java.awt.image.BufferedImage;
import java.util.function.BiConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.webcam.Webcam;
import de.monochromata.eyetracking.webcam.calibration.ValidationResult;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.svm.GazeSvm.Predictor;

/**
 * A Clmtrackr-based gaze tracker that can be started and stopped once, but
 * cannot be re-started.
 *
 * TODO: Add support for re-starting
 * <p>
 * TODO: Use a thread-pool instead of dedicated thread.
 */
public class GazeTracker extends AbstractTracker<BiConsumer<Gaze, Gaze>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(GazeTracker.class);

	private final Thread thread = new Thread(this::run);

	private final Webcam webcam;
	private final FeatureExtractor<BufferedImage> featureExtractor;

	private boolean predictorsAvailable = false;
	private Predictor leftPredictor;
	private Predictor rightPredictor;

	private boolean running = false;
	private boolean stopped = false;

	public GazeTracker(final Webcam webcam, final FeatureExtractor<BufferedImage> featureExtractor) {
		this.webcam = webcam;
		this.featureExtractor = featureExtractor;
	}

	public void setPredictors(final ValidationResult result) {
		setPredictors(result.getLeftEyeResult().getPredictor(), result.getRightEyeResult().getPredictor());
	}

	public void setPredictors(final Predictor leftPredictor, final Predictor rightPredictor) {
		this.leftPredictor = leftPredictor;
		this.rightPredictor = rightPredictor;
		predictorsAvailable = true;
	}

	private void run() {
		while (running) {
			runASafeCycle();
		}
	}

	private void runASafeCycle() {
		try {
			runACycle();
		} catch (final Exception e) {
			LOGGER.error("Exception during tracking", e);
		}
	}

	private void runACycle() {
		final BufferedImage image = webcam.getImage();
		predictGaze(image);
	}

	private void predictGaze(final BufferedImage image) {
		if (predictorsAvailable) {
			final EyeFeatures eyeFeatures = featureExtractor.apply(image);
			final Gaze leftEstimate = leftPredictor.getPredictor()
					.apply(new Gaze(eyeFeatures.getLeftEyeFeatures(), 0, 0));
			final Gaze rightEstimate = rightPredictor.getPredictor()
					.apply(new Gaze(eyeFeatures.getRightEyeFeatures(), 0, 0));
			notifyListeners(listener -> listener.accept(leftEstimate, rightEstimate));
		}
	}

	public void start() {
		if (!running) {
			thread.start();
			running = true;
		}
	}

	/**
	 * Stopping is gracefully. The current tracking cycle might be completed.
	 * This method might return immediately before stopping completed.
	 */
	public void stop() {
		if (running && !stopped) {
			running = false;
			stopped = true;
		}
	}

}
