package de.monochromata.eyetracking.webcam.calibration;

import java.util.List;

import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.svm.GazeSvm;
import de.monochromata.eyetracking.webcam.svm.GazeSvm.TrainingResult;
import de.monochromata.eyetracking.webcam.svm.Gaze;
import libsvm.svm_parameter;

public class Calibration {

	private final GazeSvm leftEyeSvm;
	private final GazeSvm rightEyeSvm;
	private final svm_parameter hyperParameters;
	
	public <I> Calibration(final FeatureExtractor<I> featureExtractor,
			final svm_parameter hyperParameters) {
		this(new GazeSvm(featureExtractor), new GazeSvm(featureExtractor), hyperParameters);
	}
	
	public Calibration(final GazeSvm leftEyeSvm, final GazeSvm rightEyeSvm,
			final svm_parameter hyperParameters) {
		this.leftEyeSvm = leftEyeSvm;
		this.rightEyeSvm = rightEyeSvm;
		this.hyperParameters = hyperParameters;
	}
	
	public ValidationResult train(final List<Gaze> leftEyeTrainingData, final List<Gaze> leftEyeValidationData,
			final List<Gaze> rightEyeTrainingData, final List<Gaze> rightEyeValidationData) {
		final EyeValidationResult leftEyeResult = train(leftEyeSvm, leftEyeTrainingData, leftEyeValidationData);
		final EyeValidationResult rightEyeResult = train(rightEyeSvm, rightEyeTrainingData, rightEyeValidationData);
		return new ValidationResult(leftEyeResult, rightEyeResult);
	}
	
	protected EyeValidationResult train(final GazeSvm svm,
			final List<Gaze> trainingData, final List<Gaze> validationData) {
		final TrainingResult result = svm.train(trainingData, validationData, hyperParameters);
		return new EyeValidationResult(
				result.getErrorValuesByDimension().get(0),
				result.getErrorValuesByDimension().get(1),
				result);
	}
	
}
