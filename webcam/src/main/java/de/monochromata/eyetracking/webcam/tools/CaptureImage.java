package de.monochromata.eyetracking.webcam.tools;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;

/**
 * Capture an image from the default webcam and save it to disk.
 */
public class CaptureImage {

	public static void main(final String[] args) throws IOException {
		final SarxosWebcam webcam = new SarxosWebcam(2);
		final BufferedImage image = webcam.getImage();
		ImageIO.write(image, "PNG", new File("image.png"));
	}
	
}
