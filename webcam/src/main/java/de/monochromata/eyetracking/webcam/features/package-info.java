/**
 * Feature vectors and feature extraction from eye patches.
 */
package de.monochromata.eyetracking.webcam.features;