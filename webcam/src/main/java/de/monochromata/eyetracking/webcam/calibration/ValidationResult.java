package de.monochromata.eyetracking.webcam.calibration;

public class ValidationResult {

	private final EyeValidationResult leftEyeResult;
	private final EyeValidationResult rightEyeResult;
	
	public ValidationResult(final EyeValidationResult leftEyeResult,
			final EyeValidationResult rightEyeResult) {
		this.leftEyeResult = leftEyeResult;
		this.rightEyeResult = rightEyeResult;
	}
	
	public EyeValidationResult getLeftEyeResult() {
		return leftEyeResult;
	}
	
	public EyeValidationResult getRightEyeResult() {
		return rightEyeResult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((leftEyeResult == null) ? 0 : leftEyeResult.hashCode());
		result = prime * result + ((rightEyeResult == null) ? 0 : rightEyeResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValidationResult other = (ValidationResult) obj;
		if (leftEyeResult == null) {
			if (other.leftEyeResult != null)
				return false;
		} else if (!leftEyeResult.equals(other.leftEyeResult))
			return false;
		if (rightEyeResult == null) {
			if (other.rightEyeResult != null)
				return false;
		} else if (!rightEyeResult.equals(other.rightEyeResult))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ValidationResult [leftEye=" + getLeftEyeResult() + ", rightEye="
				+ getRightEyeResult() + "]";
	}
	
}
