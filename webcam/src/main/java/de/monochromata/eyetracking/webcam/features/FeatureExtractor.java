package de.monochromata.eyetracking.webcam.features;

import java.awt.image.BufferedImage;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Used to extract features from a representation of a face or a pair of eyes.
 * 
 * @param <I>
 *            The input that the feature extractor expects. Can be e.g. a
 *            {@link BufferedImage} an intermediate representation, e.g. the
 *            positions that represent the face in an image.
 */
public interface FeatureExtractor<I> extends Function<I, EyeFeatures> {

	/**
	 * @return the number of features contained in the arrays provided by the
	 *         {@link EyeFeatures} instance returned by {@link #apply(Object)}.
	 */
	public int getNumberOfFeatures();

	/**
	 * Creates a new instance that uses the given visualizer.
	 */
	public FeatureExtractor<I> withVisualizer(final Consumer<Object[]> visualizer);

}
