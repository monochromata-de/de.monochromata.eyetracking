package de.monochromata.eyetracking.webcam.svm;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import de.monochromata.ml.libsvm.LibsvmSupport;
import de.monochromata.ml.lowlevel.correction.CorrectionDatum;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

public interface GazeSvmSupport extends LibsvmSupport {

	// TODO: CorrectionDatum isn't a suitable name maybe?
	default CorrectionDatum<svm_node[], Double> reader(final Gaze gaze) {
		final libsvm.svm_node[] inputFeatureValues = svm_nodes(gaze.getEyeFeatures());
		final List<Double> outputFeatureValues = asList(gaze.getXEstimate(), gaze.getYEstimate());
		return CorrectionDatum.of(inputFeatureValues, outputFeatureValues);
	}
	
	default Gaze writer(final CorrectionDatum<svm_node[], Double> corrected, final Gaze input) {
		final List<Double> outputFeatureValues = corrected.getOutputFeatureValues();
		final Double xEstimate = outputFeatureValues.get(0);
		final Double yEstimate = outputFeatureValues.get(1);
		return new Gaze(input.getEyeFeatures(), xEstimate, yEstimate);
	}
	
	default CorrectionDatum<svm_node[], Double> corrector(
			final CorrectionDatum<svm_node[], Double> datum, final List<svm_model> models) {
		final double xEstimate = svm.svm_predict(models.get(0), datum.getInputFeatureValues());
		final double yEstimate = svm.svm_predict(models.get(1), datum.getInputFeatureValues());
		return CorrectionDatum.of(datum.getInputFeatureValues(), asList(xEstimate, yEstimate));
	}
	
	default List<svm_node[]> createInputFeatureValues(final List<Gaze> data) {
		return data.stream()
				.map(Gaze::getEyeFeatures)
				.map(this::svm_nodes)
				.collect(toList());
	}
	
	default List<List<Double>> createOutputFeatureValuesByDimension(final List<Gaze> data) {
		final List<Double> xEstimates = new ArrayList<>(data.size());
		final List<Double> yEstimates = new ArrayList<>(data.size());
		data.stream().forEach(gaze -> {
			xEstimates.add(gaze.getXEstimate());
			yEstimates.add(gaze.getYEstimate());
		});
		return asList(xEstimates, yEstimates);
	}
	
	default List<List<Double>> getTruthValuesByDatum(final List<Gaze> data) {
		return data.stream()
				.map(gaze -> asList(gaze.getXEstimate(), gaze.getYEstimate()))
				.collect(toList());
	}
	
}
