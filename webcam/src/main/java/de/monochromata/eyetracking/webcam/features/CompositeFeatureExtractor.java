package de.monochromata.eyetracking.webcam.features;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A feature extractor composed of a pre-processor function and a delegate
 * feature extractor.
 *
 * @param <I>
 *            the representation input into the pre-processor
 * @param <O>
 *            the representation output by the pre-processor and input into the
 *            delegate feature extractor
 */
public class CompositeFeatureExtractor<I, O> implements FeatureExtractor<I> {

	private final Function<I, O> preprocessor;
	private final FeatureExtractor<O> delegate;

	public CompositeFeatureExtractor(final Function<I, O> preprocessor, final FeatureExtractor<O> delegate) {
		this.preprocessor = preprocessor;
		this.delegate = delegate;
	}

	@Override
	public EyeFeatures apply(final I input) {
		return delegate.apply(preprocessor.apply(input));
	}

	@Override
	public int getNumberOfFeatures() {
		return delegate.getNumberOfFeatures();
	}

	@Override
	public FeatureExtractor<I> withVisualizer(final Consumer<Object[]> visualizer) {
		return new CompositeFeatureExtractor<>(preprocessor, delegate.withVisualizer(visualizer));
	}

	/**
	 * Creates a feature extractor composed of a pre-processor function and a
	 * delegate feature extractor.
	 *
	 * @param <I>
	 *            the representation input into the pre-processor
	 * @param <O>
	 *            the representation output by the pre-processor and input into
	 *            the delegate feature extractor
	 */
	public static <I, O> FeatureExtractor<I> compose(final Function<I, O> preprocessor,
			final FeatureExtractor<O> delegate) {
		return new CompositeFeatureExtractor<>(preprocessor, delegate);
	}

}
