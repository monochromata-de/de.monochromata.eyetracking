/**
 * Java-based webcam eye-tracking inspired by <a href="https://webgazer.cs.brown.edu/">WebGazer</a>
 * 
 * @see <a href="https://github.com/brownhci/WebGazer">WebGazer (Github)</a>
 * @see <a href="http://jeffhuang.com/Final_WebGazer_IJCAI16.pdf">WebGazer: Scalable Webcam Eye Tracking Using User Interactions (Paper)</a>
 * @see <a href="https://github.com/PrincetonVision/TurkerGaze">TurkerGaze</a>
 */
package de.monochromata.eyetracking.webcam;