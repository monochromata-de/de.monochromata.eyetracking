/**
 * A webcam provider using <a href="https://github.com/sarxos/webcam-capture">Github:sarxos/webcam-capture</a>
 */
package de.monochromata.eyetracking.webcam.sarxos;