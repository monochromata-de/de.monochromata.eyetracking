package de.monochromata.eyetracking.webcam.features;

public class EyeFeatures {

	private final double[] leftEyeFeatures;
	private final double[] rightEyeFeatures;
	
	public EyeFeatures(final double[] leftEyeFeatures, final double[] rightEyeFeatures) {
		this.leftEyeFeatures = leftEyeFeatures;
		this.rightEyeFeatures = rightEyeFeatures;
	}

	public double[] getLeftEyeFeatures() {
		return leftEyeFeatures;
	}

	public double[] getRightEyeFeatures() {
		return rightEyeFeatures;
	}
	
}
