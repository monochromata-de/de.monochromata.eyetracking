package de.monochromata.eyetracking.webcam.sarxos;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.github.sarxos.webcam.WebcamEvent;

import de.monochromata.eyetracking.webcam.Webcam;

/**
 * A wrapper around {@link com.github.sarxos.webcam.Webcam}.
 */
public class SarxosWebcam implements Webcam {

	private final List<Consumer<BufferedImage>> imageListeners = new ArrayList<>();
	private final com.github.sarxos.webcam.Webcam webcam;
	private final boolean ASYNCHRONOUS = true;
	
	/**
	 * Open the default webcam using the second largest view size.
	 */
	public SarxosWebcam() {
		this(1);
	}
	
	/**
	 * Open the default webcam using the n'th largest view size (0-based,
	 * larger {@code n} implies larger view size). At least {@code 0 <= n <= 2}
	 * is supported.
	 */
	public SarxosWebcam(final int viewSizeIndex) {
		webcam = com.github.sarxos.webcam.Webcam.getDefault();
		webcam.setViewSize(webcam.getViewSizes()[viewSizeIndex]);
		final boolean webcamWasOpened = webcam.open(ASYNCHRONOUS);
		if(!webcamWasOpened) {
			throw new IllegalStateException("Failed to open webcam.");
		}
		webcam.addWebcamListener(new ImageNotifier());
	}
	
	@Override
	public BufferedImage getImage() {
		return webcam.getImage();
	}
	
	public double getFramesPerSecond() {
		return webcam.getFPS();
	}

	@Override
	public Dimension getViewSize() {
		return webcam.getViewSize();
	}
	
	public void addImageListener(final Consumer<BufferedImage> imageListener) {
		imageListeners.add(imageListener);
	}
	
	public void removeImageListener(final Consumer<BufferedImage> imageListener) {
		imageListeners.remove(imageListener);
	}
	
	protected void notifyImageListeners(final BufferedImage newImage) {
		imageListeners.forEach(listener -> listener.accept(newImage));
	}
	
	private class ImageNotifier extends WebcamAdapter {

		@Override
		public void webcamImageObtained(final WebcamEvent we) {
			// When the webcam is opened in async mode, this event is only sent when
			// Sarxos obtained the image from the device, not when the image is
			// obtained from Sarxos via Webcam.getImage();
			notifyImageListeners(we.getImage());
		}
		
	}
	
}
