package de.monochromata.eyetracking.webcam.sarxos;

import com.github.sarxos.webcam.WebcamEvent;
import com.github.sarxos.webcam.WebcamListener;

public class WebcamAdapter implements WebcamListener {

	@Override
	public void webcamOpen(final WebcamEvent we) {
	}

	@Override
	public void webcamClosed(final WebcamEvent we) {
	}

	@Override
	public void webcamDisposed(final WebcamEvent we) {
	}

	@Override
	public void webcamImageObtained(final WebcamEvent we) {
	}

}
