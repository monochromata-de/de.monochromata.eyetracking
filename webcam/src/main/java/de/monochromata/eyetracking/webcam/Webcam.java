package de.monochromata.eyetracking.webcam;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.function.Consumer;

/**
 * A webcam interface.
 */
public interface Webcam {

	/**
	 * Obtain a new image from the webcam.
	 * 
	 * @return the image
	 */
	BufferedImage getImage();

	/**
	 * Return the dimensions of the images provided by this webcam.
	 * 
	 * @return the dimensions in pixels
	 */
	Dimension getViewSize();
	
	/**
	 * Add a listener for asynchronously obtained webcam images, i.e. those images that
	 * were not obtained via {@link #getImage()}.
	 * <p>
	 * Note that images obtained via {@link #getImage()} might have been provided to the listeners
	 * registered via this method.
	 * 
	 * @see #removeImageListener(Consumer)
	 */
	void addImageListener(final Consumer<BufferedImage> imageListener);
	
	/**
	 * Removes the given listener for asynchronousĺy obtained webcam images.
	 * 
	 * @see #addImageListener(Consumer)
	 */
	void removeImageListener(final Consumer<BufferedImage> imageListener);

}
