package de.monochromata.eyetracking.webcam.image;

import static java.awt.RenderingHints.KEY_INTERPOLATION;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_BICUBIC;
import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

public interface ImageManipulation {
	
	static BufferedImage ensureGrayscale(final BufferedImage image) {
		if(image.getType() == TYPE_BYTE_GRAY) {
			return image;
		}
		return convert(image, TYPE_BYTE_GRAY);
	}
	
	static BufferedImage convert(final BufferedImage input, final int targetType) {
		final BufferedImage converted = new BufferedImage(input.getWidth(), input.getHeight(), targetType);
		new ColorConvertOp(null).filter(input, converted);
		return converted;
	}

	static BufferedImage scaleBicubic(final BufferedImage input, final int targetWidth,
			final int targetHeight) {
		return scale(input, targetWidth, targetHeight, VALUE_INTERPOLATION_BICUBIC);
	}
	
	static BufferedImage copy(final BufferedImage image) {
		// Based on http://stackoverflow.com/a/26894825
	    final ColorModel colorModel = image.getColorModel();
	    final boolean isAlphaPremultiplied = colorModel.isAlphaPremultiplied();
	    final WritableRaster targetRaster = image.getRaster().createCompatibleWritableRaster();
	    image.copyData(targetRaster);
	    return new BufferedImage(colorModel, targetRaster, isAlphaPremultiplied, null);
	}

	/**
	 * @param interpolation One of {@link RenderingHints#VALUE_INTERPOLATION_BICUBIC},
	 * 	{@link RenderingHints#VALUE_INTERPOLATION_BILINEAR},
	 *  {@link RenderingHints#VALUE_INTERPOLATION_NEAREST_NEIGHBOR} 
	 */
	static BufferedImage scale(final BufferedImage input, final int targetWidth, final int targetHeight,
			final Object interpolation) {
		// Based on https://community.oracle.com/docs/DOC-983611
		final BufferedImage scaledImage = 
				new BufferedImage(targetWidth, targetHeight, input.getType());
		final Graphics2D targetGraphics = scaledImage.createGraphics();
		targetGraphics.setRenderingHint(KEY_INTERPOLATION, interpolation);
		final boolean completed = targetGraphics.drawImage(input, 0, 0, targetWidth, targetHeight, null);
		if(!completed) {
			throw new RuntimeException("Scaling did not complete immediately. An ImageObserver is required.");
		}
		return scaledImage;
	}
	
}
