package de.monochromata.eyetracking.webcam.svm;

import static de.monochromata.ml.libsvm.LibsvmScaler.fromMinus1ToPlus1;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.ml.libsvm.Libsvm;
import de.monochromata.ml.libsvm.LibsvmCorrectionModel;
import de.monochromata.ml.libsvm.LibsvmScaler;
import de.monochromata.ml.libsvm.LibsvmTrainingData;
import de.monochromata.ml.libsvm.LibsvmTransformer;
import de.monochromata.ml.libsvm.config.ModelType;
import de.monochromata.ml.lowlevel.Feature;
import de.monochromata.ml.lowlevel.ML;
import de.monochromata.ml.lowlevel.correction.CorrectionModel;
import de.monochromata.ml.lowlevel.training.validation.ErrorFunction;
import de.monochromata.ml.lowlevel.training.validation.Validation;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

public class GazeSvm implements GazeSvmSupport {
	
	private final List<Feature> inputFeatures;
	private final List<Feature> outputFeatures;

	private final ML<ModelType, svm_parameter, svm_node[], Double, List<svm_model>, Gaze> ml =
			ML.getInstance(new Libsvm<Gaze>(this::reader, this::writer));
	
	public <I> GazeSvm(final FeatureExtractor<I> featureExtractor) {
		final int numberOfInputFeatures = featureExtractor.getNumberOfFeatures();
		this.inputFeatures = createInputFeatures(numberOfInputFeatures);
		this.outputFeatures = createOutputFeatures();
	}
	
	private static List<Feature> createInputFeatures(final int numberOfInputFeatures) {
		return IntStream.range(0, numberOfInputFeatures)
				.mapToObj(i -> new Feature(i+"th feature"))
				.collect(toList());
	}

	private static List<Feature> createOutputFeatures() {
		return asList(new Feature("xEstimate"), new Feature("yEstimate"));
	}
	
	public TrainingResult train(final List<Gaze> trainingData, final List<Gaze> validationData,
			final svm_parameter hyperParameters) {
		// TODO: Don't use the common pool
		final LibsvmTrainingData<List<svm_model>, Gaze> trainingDataObj = new LibsvmTrainingData<List<svm_model>, Gaze>(
				ml.getSpi(), inputFeatures, outputFeatures,
				createInputFeatureValues(trainingData), createOutputFeatureValuesByDimension(trainingData));
		final LibsvmScaler scaler = fromMinus1ToPlus1();
		final LibsvmTransformer transformer = new LibsvmTransformer(scaler);
		
		final CorrectionModel<libsvm.svm_node[], Double, Gaze> correctionModel = ml.trainingData(inputFeatures, outputFeatures)
			.populate(trainingDataObj)
			.preprocess(singletonList(scaler))
			.thenCompose(state -> state.train(hyperParameters, this::reader, this::corrector, this::writer))
			.toCompletableFuture().join();
		
		final Validation<libsvm.svm_node[], Double, Gaze> validation = ml.getSpi().createValidation();
		final List<Double> errorValuesByDimension = validation.validate(correctionModel, this::reader,
				singletonList(transformer), supplier(validationData), supplier(getTruthValuesByDatum(validationData)),
				ErrorFunction::rmse);
		
		return new TrainingResult(correctionModel, transformer, errorValuesByDimension);
	}
	
	public CorrectionModel<svm_node[], Double, Gaze> getCorrectionModel(final Serializable memento) {
		return LibsvmCorrectionModel.of(memento, this::reader, this::corrector, this::writer);
	}
	
	protected <T> Supplier<T> supplier(final List<T> data) {
		final Iterator<T> iterator = data.iterator();
		return () -> iterator.hasNext()?iterator.next():null; 
	}
	
	public static class Predictor {
		
		private final CorrectionModel<libsvm.svm_node[], Double, Gaze> correctionModel;
		private final LibsvmTransformer transformer;
		private final Function<Gaze,Gaze> predictor;
		
		private Predictor(final CorrectionModel<libsvm.svm_node[], Double, Gaze> correctionModel,
				final LibsvmTransformer transformer) {
			this.correctionModel = correctionModel;
			this.transformer = transformer;
			this.predictor = toCorrect -> correctionModel.apply(toCorrect, transformer);
		}
		
		public CorrectionModel<libsvm.svm_node[], Double, Gaze> getCorrectionModel() {
			return correctionModel;
		}

		public Function<Gaze, Gaze> getPredictor() {
			return predictor;
		}
		
		public Serializable getMemento() {
			return new Memento(correctionModel.getMemento(), transformer.getMemento());
		}
		
		public static Predictor fromMemento(final Serializable memento) {
			final GazeSvmSupport support = new GazeSvmSupport() {}; 
			final LibsvmCorrectionModel<Gaze> correctionModel = 
					LibsvmCorrectionModel.of(((Memento)memento).getCorrectionModelMemento(),
							support::reader, support::corrector, support::writer);
			final LibsvmTransformer transformer = 
					LibsvmTransformer.fromMemento(((Memento)memento).getTransformerMemento());
			return new Predictor(correctionModel, transformer);
		}
		
		private static class Memento implements Serializable {
			
			private static final long serialVersionUID = -3113141711859584562L;
			private final Serializable correctionModelMemento;
			private final Serializable transformerMemento;
			
			private Memento(final Serializable correctionModelMemento,
					final Serializable predictorMemento) {
				this.correctionModelMemento = correctionModelMemento;
				this.transformerMemento = predictorMemento;
			}

			private Serializable getCorrectionModelMemento() {
				return correctionModelMemento;
			}

			private Serializable getTransformerMemento() {
				return transformerMemento;
			}
			
		}
		
	}
	
	public static class TrainingResult extends Predictor {
		
		private final List<Double> errorValuesByDimension;
		
		public TrainingResult(final CorrectionModel<libsvm.svm_node[], Double, Gaze> correctionModel,
				final LibsvmTransformer transformer,
				final List<Double> errorValuesByDimension) {
			super(correctionModel, transformer);
			this.errorValuesByDimension = errorValuesByDimension;
		}

		public List<Double> getErrorValuesByDimension() {
			return errorValuesByDimension;
		}

	}
}
