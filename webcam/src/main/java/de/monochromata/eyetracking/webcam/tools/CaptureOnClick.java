package de.monochromata.eyetracking.webcam.tools;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.util.Optional.ofNullable;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.webcam.Webcam;
import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;

/**
 * Capture webcam images on mouse clicks and name the file by
 * the x and y coordinates of the mouse.
 * 
 * TODO: Move into a new package that signals that this class depends on Swing
 */
public class CaptureOnClick {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CaptureOnClick.class);
	private static final int MARGIN_PX = 30;

	private final List<Consumer<BufferedImage>> listenersForImagesWithoutManualContext = new ArrayList<>();
	private final List<BiConsumer<BufferedImage,Point>> listenersForImagesWithManualContext = new ArrayList<>();
	private final Webcam webcam;
	
	/**
	 * 
	 * @param manualContextConnector Is supplied a consumer that it sends manual contexts to.
	 * @param webcam
	 */
	public CaptureOnClick(final Consumer<Consumer<Point>> manualContextConnector, final Webcam webcam) {
		this.webcam = webcam;
		webcam.addImageListener(this::notifyListenersForImagesWithoutManualContext);
		manualContextConnector.accept(this::notifyListenersForImagesWithManualContext);
	}
	
	public void addListenerForImagesWithoutManualContext(final Consumer<BufferedImage> listener) {
		this.listenersForImagesWithoutManualContext.add(listener);
	}
	
	public void removeListenerForImagesWithoutManualContext(final Consumer<BufferedImage> listener) {
		this.listenersForImagesWithoutManualContext.remove(listener);
	}
	
	public void addListenerForImagesWithManualContext(final BiConsumer<BufferedImage,Point> listener) {
		this.listenersForImagesWithManualContext.add(listener);
	}
	
	public void removeListenerForImagesWithManualContext(final BiConsumer<BufferedImage,Point> listener) {
		this.listenersForImagesWithManualContext.remove(listener);
	}

	protected void notifyListenersForImagesWithoutManualContext(final BufferedImage image) {
		listenersForImagesWithoutManualContext.forEach(listener -> listener.accept(image));
	}
	
	protected void notifyListenersForImagesWithManualContext(final Point manualContext) {
		notifyListenersForImagesWithManualContext(webcam.getImage(), manualContext);
	}

	protected void notifyListenersForImagesWithManualContext(final BufferedImage image, final Point manualContext) {
		listenersForImagesWithManualContext.forEach(listener -> listener.accept(image, manualContext));
	}
	
	private static void saveImageWithoutTruth(final BufferedImage image) {
		final File file = createImageFileWithoutTruth();
		saveImage(image, file);
	}

	private static void saveImage(final BufferedImage image, final File file) {
		try {
			ImageIO.write(image, "png", file);
		} catch (final IOException e) {
			LOGGER.error("Failed to write webcam image to disk", e);
		}
	}
	
	private static File createImageFileWithoutTruth() {
		return new File(format("webcam_%d.png", currentTimeMillis()));
	}

	private static File createImageFileWithTruth(final Point point) {
		return new File(format("webcam_%d_%d_%d.png",
				currentTimeMillis(), point.getX(), point.getY()));
	}
	
	public static void main(final String[] args) {
		final JFrame jframe = new JFrame();
		final SarxosWebcam webcam = new SarxosWebcam();
		final AtomicReference<Consumer<Point>> manualContextConnector = new AtomicReference<>();
		addImageCapturingMouseListener(jframe, webcam, manualContextConnector);
		final CaptureOnClick instance = new CaptureOnClick(manualContextConnector::set, webcam);
		instance.addListenerForImagesWithoutManualContext(CaptureOnClick::saveImageWithoutTruth);
		instance.addListenerForImagesWithManualContext((image, point) -> {
			final File file = createImageFileWithTruth(point);
			saveImage(image, file);
		});
		setFrameBounds(jframe);
		setCloseOperation(jframe);
		jframe.setVisible(true);
	}
	
	private static void setCloseOperation(final JFrame jframe) {
		jframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private static void setFrameBounds(final JFrame jframe) {
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		final int width = (int)screenSize.getWidth()-(MARGIN_PX*2);
		final int height = (int)screenSize.getHeight()-(MARGIN_PX*2);
		jframe.setBounds(MARGIN_PX, MARGIN_PX, width, height);
	}

	public static void addImageCapturingMouseListener(final JFrame jframe, final Webcam webcam,
			final AtomicReference<Consumer<Point>> manualContextConnector) {
		jframe.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent event) {
				// The image obtained here might already have been obtained and
				// saved without ground truth, which is not deemed problematic.
				final PointImpl point = new PointImpl(event.getXOnScreen(), event.getYOnScreen());
				ofNullable(manualContextConnector.get())
					.ifPresent(consumer -> consumer.accept(point));
			}
		});
	}
	
}
