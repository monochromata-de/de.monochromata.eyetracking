package de.monochromata.eyetracking.webcam.features;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class HistogramEqualization {

	public static BufferedImage equalizeHistogram(final BufferedImage input) {
		final int[] histogram = getHistogram(input);
		final int[] lookupTable = equalizeHistogram(input, histogram);
		return equalizeImage(input, lookupTable);
	}
	
	private static int[] getHistogram(final BufferedImage input) {
		final WritableRaster raster = input.getRaster();
		final int[] intArray = new int[1];
		final int[] histogram = new int[255];
		for(int x=0;x<input.getWidth();x++) {
			for(int y=0;y<input.getHeight();y++) {
				final int value = raster.getPixel(x, y, intArray)[0]; 
				histogram[value]++;
			}
		}
		return histogram;
	}
	
	private static int[] equalizeHistogram(final BufferedImage image, final int[] histogram) {
		final int numberOfPixels = image.getWidth()*image.getHeight();
		final int[] equalizedHistogram = new int[histogram.length];
		int sum = 0;
		for(int i=0;i<histogram.length;i++) {
			sum += histogram[i];
			equalizedHistogram[i] = (int)Math.round(sum * 255 / (double)numberOfPixels);
		}
		return equalizedHistogram;
	}
	
	private static BufferedImage equalizeImage(final BufferedImage image, final int[] lookupTable) {
		final WritableRaster raster = image.getRaster();
		final int[] intArray = new int[1];
		for(int x=0;x<image.getWidth();x++) {
			for(int y=0;y<image.getHeight();y++) {
				final int oldValue = raster.getPixel(x, y, intArray)[0];
				final int newValue = lookupTable[oldValue];
				intArray[0] = newValue;
				raster.setPixel(x, y, intArray);
			}
		}
		return image;
	}
	
}
