package de.monochromata.eyetracking.webcam.calibration;

import de.monochromata.eyetracking.webcam.svm.GazeSvm;

public class EyeValidationResult {

	private final double errorX;
	private final double errorY;
	private final GazeSvm.Predictor predictor;
	
	public EyeValidationResult(final double errorX, final double errorY,
			final GazeSvm.Predictor predictor) {
		this.errorX = errorX;
		this.errorY = errorY;
		this.predictor = predictor;
	}

	public double getErrorX() {
		return errorX;
	}

	public double getErrorY() {
		return errorY;
	}

	public GazeSvm.Predictor getPredictor() {
		return predictor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(errorX);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(errorY);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EyeValidationResult other = (EyeValidationResult) obj;
		if (Double.doubleToLongBits(errorX) != Double.doubleToLongBits(other.errorX))
			return false;
		if (Double.doubleToLongBits(errorY) != Double.doubleToLongBits(other.errorY))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "x=" + getErrorX() + "px, y=" + getErrorY() + "px";
	}
	
}