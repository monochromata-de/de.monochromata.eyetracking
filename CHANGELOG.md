# Release 2.3.248

* Update dependency de.monochromata.event:event to v1.2.113

# Release 2.3.247

* Update de.monochromata.ml.version to v1.0.108

# Release 2.3.246

* Update dependency de.monochromata.event:aggregator to 1.2.112

# Release 2.3.245

* Update cucumber.version to v7

# Release 2.3.244

* Update dependency de.monochromata.event:aggregator to 1.2.111

# Release 2.3.243

* Update dependency de.monochromata.event:aggregator to 1.2.110
* Merge version numbers

# Release 2.3.242

* Update dependency de.monochromata.ml:aggregator to 1.0.107
* Merge version numbers

# Release 2.3.241

* Update dependency de.monochromata.plot:aggregator to 1.0.51
* Update dependency org.assertj:assertj-core to v3.21.0

# Release 2.3.240

* Update dependency de.monochromata.ml:aggregator to 1.0.106

# Release 2.3.239

* Update dependency de.monochromata.event:aggregator to 1.2.109
* Merge version numbers

# Release 2.3.238

* Update dependency de.monochromata.ml:aggregator to 1.0.105
* Merge version numbers

# Release 2.3.237

* Update dependency de.monochromata.plot:aggregator to 1.0.50
* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.1

# Release 2.3.236

* Update dependency de.monochromata.event:aggregator to 1.2.108

# Release 2.3.235

* Update dependency de.monochromata.event:aggregator to 1.2.107

# Release 2.3.234

* Update dependency de.monochromata.event:aggregator to 1.2.106

# Release 2.3.233

* Update de.monochromata.ml.version to v1.0.104

# Release 2.3.232

* Update dependency de.monochromata.event:aggregator to 1.2.105

# Release 2.3.231

* Update cucumber.version to v6.11.0

# Release 2.3.230

* Update dependency org.graalvm.sdk:graal-sdk to v21.2.0

# Release 2.3.229

* #36 Remove unused jmock dependency
* Merge version numbers

# Release 2.3.228

* Update dependency de.monochromata.ml:aggregator to 1.0.103
* Update dependency org.slf4j:slf4j-api to v1.7.32

# Release 2.3.227

* Update dependency de.monochromata.ml:aggregator to 1.0.102

# Release 2.3.226

* Update dependency de.monochromata.event:aggregator to 1.2.104

# Release 2.3.225

* Update dependency de.monochromata.event:aggregator to 1.2.103

# Release 2.3.224

* Update dependency de.monochromata.event:aggregator to 1.2.102
* Merge version numbers

# Release 2.3.223

* Update dependency de.monochromata.ml:aggregator to 1.0.101
* Update dependency de.monochromata.plot:aggregator to 1.0.49
* Merge version numbers

# Release 2.3.222

* Update dependency de.monochromata.event:aggregator to 1.2.101
* Update dependency org.assertj:assertj-core to v3.20.2

# Release 2.3.221

* Update dependency org.slf4j:slf4j-api to v1.7.31

# Release 2.3.220

* Update dependency de.monochromata.ml:aggregator to 1.0.99
* Merge version numbers

# Release 2.3.219

* Update dependency de.monochromata.event:aggregator to 1.2.100
* Update dependency de.monochromata.plot:aggregator to 1.0.48
* Update dependency org.assertj:assertj-core to v3.20.1

# Release 2.3.218

* Update dependency de.monochromata.event:aggregator to 1.2.99

# Release 2.3.217

* Update dependency de.monochromata.event:aggregator to 1.2.98
* Merge version numbers

# Release 2.3.216

* Update dependency de.monochromata.ml:aggregator to 1.0.97
* Update dependency org.reficio:p2-maven-plugin to v1.7.0
* Merge version numbers

# Release 2.3.215

* Update dependency de.monochromata.ml:aggregator to 1.0.96
* Update dependency de.monochromata.event:aggregator to 1.2.97
* Update dependency de.monochromata.plot:aggregator to 1.0.47
* Merge version numbers

# Release 2.3.214

* Update dependency de.monochromata.ml:aggregator to 1.0.95
* Update dependency de.monochromata.event:event to v1.2.96
* Merge version numbers

# Release 2.3.213

* Update dependency de.monochromata.ml:aggregator to 1.0.94
* Update dependency de.monochromata.plot:aggregator to 1.0.46

# Release 2.3.212

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.0

# Release 2.3.211

* Update dependency de.monochromata.event:aggregator to 1.2.95

# Release 2.3.210

* Update dependency de.monochromata.ml:aggregator to 1.0.93

# Release 2.3.209

* Update cucumber.version to v6.10.4

# Release 2.3.208

* Update dependency de.monochromata.event:aggregator to 1.2.94

# Release 2.3.207

* Update de.monochromata.ml.version to v1.0.92

# Release 2.3.206

* Update dependency de.monochromata.event:aggregator to 1.2.93

# Release 2.3.205

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.2
* Merge version numbers

# Release 2.3.204

* Update dependency de.monochromata.ml:aggregator to 1.0.91
* Update dependency de.monochromata.event:aggregator to 1.2.92
* Update dependency de.monochromata.plot:aggregator to 1.0.45

# Release 2.3.203

* Update dependency de.monochromata.ml:aggregator to 1.0.90

# Release 2.3.202

* Update dependency org.graalvm.sdk:graal-sdk to v21.1.0

# Release 2.3.201

* Update dependency de.monochromata.ml:aggregator to 1.0.89

# Release 2.3.200

* Update cucumber.version to v6.10.3

# Release 2.3.199

* Update dependency de.monochromata.event:aggregator to 1.2.91

# Release 2.3.198

* Update dependency org.reficio:p2-maven-plugin to v1.6.0
* Update dependency de.monochromata.ml:aggregator to 1.0.88
* Merge version numbers

# Release 2.3.197

* Update dependency de.monochromata.ml:aggregator to 1.0.87
* Update dependency de.monochromata.event:aggregator to 1.2.90
* Update dependency de.monochromata.plot:aggregator to 1.0.44

# Release 2.3.196

* Update dependency de.monochromata.event:aggregator to 1.2.89

# Release 2.3.195

* fix: bundling instructions for libsvm

# Release 2.3.194

* Update dependency de.monochromata.ml:aggregator to 1.0.86

# Release 2.3.193

* Update cucumber.version to v6.10.2

# Release 2.3.192

* Update cucumber.version to v6.10.1

# Release 2.3.191

* Update dependency org.apache.commons:commons-lang3 to v3.12.0

# Release 2.3.190

* Update dependency de.monochromata.event:aggregator to 1.2.88

# Release 2.3.189

* Update de.monochromata.ml.version to v1.0.85

# Release 2.3.188

* Update dependency de.monochromata.event:aggregator to 1.2.87

# Release 2.3.187

* Update dependency de.monochromata.event:aggregator to 1.2.86

# Release 2.3.186

* Update dependency junit:junit to v4.13.2

# Release 2.3.185

* Update cucumber.version to v6.10.0
* Merge version numbers

# Release 2.3.184

* Update dependency de.monochromata.ml:aggregator to 1.0.84
* Update dependency de.monochromata.event:aggregator to 1.2.85
* Update dependency de.monochromata.plot:aggregator to 1.0.43

# Release 2.3.183

* Update dependency org.graalvm.sdk:graal-sdk to v21.0.0.2

# Release 2.3.182

* Update dependency de.monochromata.ml:aggregator to 1.0.83
* Update dependency de.monochromata.event:aggregator to 1.2.84
* Merge version numbers

# Release 2.3.181

* Update dependency de.monochromata.ml:aggregator to 1.0.82
* Update dependency de.monochromata.event:aggregator to 1.2.83
* Update dependency de.monochromata.plot:aggregator to 1.0.42
* Update dependency org.assertj:assertj-core to v3.19.0

# Release 2.3.180

* Update dependency org.graalvm.sdk:graal-sdk to v21

# Release 2.3.179

* Update de.monochromata.ml.version to v1.0.81

# Release 2.3.178

* Update dependency de.monochromata.event:aggregator to 1.2.82

# Release 2.3.177

* Update dependency de.monochromata.event:aggregator to 1.2.81

# Release 2.3.176

* Update dependency de.monochromata.event:aggregator to 1.2.80
* Merge version numbers

# Release 2.3.175

* Update dependency de.monochromata.ml:aggregator to 1.0.80
* Update dependency de.monochromata.event:aggregator to 1.2.79
* Update dependency de.monochromata.plot:aggregator to 1.0.41
* Update dependency org.reficio:p2-maven-plugin to v1.5.0
* Merge version numbers

# Release 2.3.174

* Update dependency de.monochromata.ml:aggregator to 1.0.79
* Update dependency de.monochromata.event:aggregator to 1.2.78
* Update dependency de.monochromata.plot:aggregator to 1.0.40
* Update cucumber.version to v6.9.1

# Release 2.3.173

* Publish m2 and p2 repos to GitLab Pages
* Update link to downstream project
* Build with GraalVM CE
* Comment unimplemented steps
* Replace cucumber-java8 by cucumber-java
* Make sure all cucumber dependencies have same version
* Merge remote-tracking branch 'origin/master' into renovate/io.cucumber-cucumber-junit-6.x
* Update dependency de.monochromata.event:aggregator to 1.2.77
* Add current upstream repositories
* Merge remote-tracking branch 'origin/master' into renovate/io.cucumber-cucumber-junit-6.x
* Replace j2v8 with graal
* Update dependency de.monochromata.ml:aggregator to 1.0.78
* Update dependency de.monochromata.ml:aggregator to 1.0.77
* Update dependency de.monochromata.ml:aggregator to 1.0.76
* Update dependency de.monochromata.ml:aggregator to 1.0.75
* Update dependency de.monochromata.plot:aggregator to 1.0.39
* Update dependency de.monochromata.plot:aggregator to 1.0.38
* Update dependency de.monochromata.ml:aggregator to 1.0.74
* Update dependency de.monochromata.plot:aggregator to 1.0.37
* Update dependency de.monochromata.ml:aggregator to 1.0.73
* Update dependency de.monochromata.plot:aggregator to 1.0.36
* Update dependency de.monochromata.plot:aggregator to 1.0.35
* Update dependency de.monochromata.plot:aggregator to 1.0.34
* Update dependency de.monochromata.plot:aggregator to 1.0.33
* Update dependency de.monochromata.event:aggregator to 1.2.76
* Update dependency de.monochromata.event:aggregator to 1.2.75
* Update dependency de.monochromata.event:aggregator to 1.2.74
* Update dependency io.cucumber:cucumber-junit to v6

# Release 2.3.172

* Update dependency io.cucumber:cucumber-picocontainer to v6.9.0

# Release 2.3.171

* Update dependency org.assertj:assertj-core to v3.18.1

# Release 2.3.170

* Update dependency io.cucumber:cucumber-picocontainer to v6.8.2

# Release 2.3.169

* Update dependency de.monochromata.event:aggregator to 1.2.73

# Release 2.3.168

* Update dependency de.monochromata.event:aggregator to 1.2.72

# Release 2.3.167

* Update dependency org.assertj:assertj-core to v3.18.0

# Release 2.3.166

* Update dependency de.monochromata.event:aggregator to 1.2.71

# Release 2.3.165

* Update dependency de.monochromata.event:aggregator to 1.2.70
* Merge version numbers

# Release 2.3.164

* Update dependency de.monochromata.event:aggregator to 1.2.69
* Update dependency junit:junit to v4.13.1

# Release 2.3.163

* Update dependency io.cucumber:cucumber-picocontainer to v6.8.1

# Release 2.3.162

* Update dependency de.monochromata.event:aggregator to 1.2.68

# Release 2.3.161

* Update dependency io.cucumber:cucumber-picocontainer to v6.8.0

# Release 2.3.160

* Update dependency de.monochromata.event:aggregator to 1.2.67

# Release 2.3.159

* Update dependency de.monochromata.event:aggregator to 1.2.66

# Release 2.3.158

* Update dependency de.monochromata.event:aggregator to 1.2.65

# Release 2.3.157

* Update dependency de.monochromata.event:aggregator to 1.2.64

# Release 2.3.156

* Update dependency io.cucumber:cucumber-picocontainer to v6.7.0
* Update dependency io.cucumber:cucumber-picocontainer to v6.6.1

# Release 2.3.155

* Update dependency de.monochromata.event:aggregator to 1.2.63

# Release 2.3.154

* Update dependency org.assertj:assertj-core to v3.17.2

# Release 2.3.153

* Update dependency de.monochromata.event:aggregator to 1.2.62
* Merge version numbers

# Release 2.3.152

* Update dependency de.monochromata.event:aggregator to 1.2.61
* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.1

# Release 2.3.151

* Update dependency de.monochromata.event:aggregator to 1.2.60

# Release 2.3.150

* Update dependency org.assertj:assertj-core to v3.17.1

# Release 2.3.149

* Update dependency de.monochromata.event:aggregator to 1.2.59

# Release 2.3.148

* Update dependency io.cucumber:cucumber-picocontainer to v6.6.0

# Release 2.3.147

* Update dependency de.monochromata.event:aggregator to 1.2.58

# Release 2.3.146

* Update dependency de.monochromata.event:aggregator to 1.2.57

# Release 2.3.145

* Update dependency org.assertj:assertj-core to v3.17.0

# Release 2.3.144

* Update dependency de.monochromata.event:aggregator to 1.2.56

# Release 2.3.143

* Update dependency io.cucumber:cucumber-picocontainer to v6.5.1

# Release 2.3.142

* Update dependency de.monochromata.event:aggregator to 1.2.55

# Release 2.3.141

* Update dependency de.monochromata.event:aggregator to 1.2.54

# Release 2.3.140

* Update dependency io.cucumber:cucumber-picocontainer to v6.5.0

# Release 2.3.139

* Update dependency de.monochromata.event:aggregator to 1.2.53
* Merge version numbers

# Release 2.3.138

* Update dependency de.monochromata.event:aggregator to 1.2.52
* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.8.1

# Release 2.3.137

* Update dependency de.monochromata.event:aggregator to 1.2.51

# Release 2.3.136

* Update dependency io.cucumber:cucumber-picocontainer to v6.4.0

# Release 2.3.135

* Update dependency de.monochromata.event:aggregator to 1.2.50

# Release 2.3.134

* Update dependency io.cucumber:cucumber-picocontainer to v6.3.0

# Release 2.3.133

* Update dependency de.monochromata.event:aggregator to 1.2.49

# Release 2.3.132

* Update dependency de.monochromata.event:aggregator to 1.2.48

# Release 2.3.131

* Update dependency org.apache.commons:commons-lang3 to v3.11

# Release 2.3.130

* Update dependency de.monochromata.event:aggregator to 1.2.47

# Release 2.3.129

* Update dependency io.cucumber:cucumber-picocontainer to v6.2.2

# Release 2.3.128

* Update dependency io.cucumber:cucumber-picocontainer to v6.2.1

# Release 2.3.127

* Update dependency io.cucumber:cucumber-picocontainer to v6.2.0

# Release 2.3.126

* Update dependency io.cucumber:cucumber-picocontainer to v6.1.2

# Release 2.3.125

* Update dependency de.monochromata.event:aggregator to 1.2.46

# Release 2.3.124

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.1

# Release 2.3.123

* Update dependency de.monochromata.ml:aggregator to 1.0.72

# Release 2.3.122

* Update dependency de.monochromata.event:aggregator to 1.2.45

# Release 2.3.121

* Update dependency de.monochromata.plot:aggregator to 1.0.32

# Release 2.3.120

* Update dependency de.monochromata.event:aggregator to 1.2.44

# Release 2.3.119

* Update dependency de.monochromata.ml:aggregator to 1.0.70

# Release 2.3.118

* Update dependency de.monochromata.event:aggregator to 1.2.43

# Release 2.3.117

* Update dependency io.cucumber:cucumber-picocontainer to v6.1.1

# Release 2.3.116

* Update dependency io.cucumber:cucumber-picocontainer to v6.1.0

# Release 2.3.115

* Update dependency io.cucumber:cucumber-picocontainer to v6

# Release 2.3.114

* Update dependency de.monochromata.ml:aggregator to 1.0.69

# Release 2.3.113

* Update dependency de.monochromata.plot:aggregator to 1.0.31

# Release 2.3.112

* Update dependency de.monochromata.event:aggregator to 1.2.42

# Release 2.3.111

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.1

# Release 2.3.110

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.0

# Release 2.3.109

* Update dependency de.monochromata.event:aggregator to 1.2.41

# Release 2.3.108

* Update dependency de.monochromata.ml:aggregator to 1.0.67

# Release 2.3.107

* Update dependency de.monochromata.event:aggregator to 1.2.40

# Release 2.3.106

* Update dependency de.monochromata.ml:aggregator to 1.0.66

# Release 2.3.105

* Update dependency org.assertj:assertj-core to v3.16.1
* Merge version numbers

# Release 2.3.104

* Update dependency de.monochromata.ml:aggregator to 1.0.65
* Merge version numbers

# Release 2.3.103

* Update dependency de.monochromata.event:aggregator to 1.2.39
* Update dependency de.monochromata.plot:aggregator to 1.0.30

# Release 2.3.102

* Update dependency de.monochromata.ml:aggregator to 1.0.64

# Release 2.3.101

* Update dependency org.assertj:assertj-core to v3.16.0

# Release 2.3.100

* Update dependency de.monochromata.ml:aggregator to 1.0.63

# Release 2.3.99

* Update dependency de.monochromata.event:aggregator to 1.2.38
* Merge version numbers

# Release 2.3.98

* Update dependency de.monochromata.ml:aggregator to 1.0.62
* Update dependency de.monochromata.plot:aggregator to 1.0.29

# Release 2.3.97

* Update dependency io.cucumber:cucumber-picocontainer to v5.7.0

# Release 2.3.96

* Update dependency de.monochromata.event:aggregator to 1.2.37

# Release 2.3.95

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.0
* Merge version numbers

# Release 2.3.94

* Update dependency de.monochromata.ml:aggregator to 1.0.61
* Update dependency de.monochromata.event:aggregator to 1.2.36
* Merge version numbers

# Release 2.3.93

* Update dependency de.monochromata.ml:aggregator to 1.0.60
* Update dependency de.monochromata.plot:aggregator to 1.0.28

# Release 2.3.92

* Update dependency org.apache.commons:commons-lang3 to v3.10

# Release 2.3.91

* Update dependency io.cucumber:cucumber-picocontainer to v5.6.0

# Release 2.3.90

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.2.0
* Merge version numbers

# Release 2.3.89

* Update dependency de.monochromata.ml:aggregator to 1.0.59
* Merge version numbers

# Release 2.3.88

* Update dependency de.monochromata.ml:aggregator to 1.0.58
* Update dependency de.monochromata.event:aggregator to 1.2.35

# Release 2.3.87

* Update dependency de.monochromata.event:aggregator to 1.2.34

# Release 2.3.86

* Update dependency de.monochromata.ml:aggregator to 1.0.57

# Release 2.3.85

* Update dependency de.monochromata.event:aggregator to 1.2.33

# Release 2.3.84

* Update dependency io.cucumber:cucumber-picocontainer to v5.5.0

# Release 2.3.83

* Update de.monochromata.ml.version to v1.0.56

# Release 2.3.82

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.0

# Release 2.3.81

* Update dependency de.monochromata.plot:aggregator to 1.0.26

# Release 2.3.80

* Update dependency de.monochromata.ml:aggregator to 1.0.55

# Release 2.3.79

* Update dependency de.monochromata.event:aggregator to 1.2.32

# Release 2.3.78

* Update dependency io.cucumber:cucumber-picocontainer to v5.4.2

# Release 2.3.77

* Update dependency de.monochromata.event:aggregator to 1.2.31

# Release 2.3.76

* Update dependency io.cucumber:cucumber-picocontainer to v5.4.1

# Release 2.3.75

* Update dependency de.monochromata.event:aggregator to 1.2.30

# Release 2.3.74

* Update dependency io.cucumber:cucumber-picocontainer to v5.4.0

# Release 2.3.73

* Update dependency io.cucumber:cucumber-picocontainer to v5.3.0

# Release 2.3.72

* Change jcenter repo to HTTPS

# Release 2.3.71

* Update dependency io.cucumber:cucumber-picocontainer to v5.2.0

# Release 2.3.70

* Update dependency io.cucumber:cucumber-picocontainer to v5.1.3

# Release 2.3.69

* Update dependency org.assertj:assertj-core to v3.15.0

# Release 2.3.68

* Update de.monochromata.ml.version to v1.0.54

# Release 2.3.67

* Update dependency de.monochromata.plot:aggregator to 1.0.25
* Merge version numbers

# Release 2.3.66

* Update dependency de.monochromata.ml:aggregator to 1.0.53
* Update dependency de.monochromata.event:aggregator to 1.2.29

# Release 2.3.65

* Update dependency io.cucumber:cucumber-picocontainer to v5.1.2

# Release 2.3.64

* Update dependency de.monochromata.event:aggregator to 1.2.28

# Release 2.3.63

* Update dependency de.monochromata.ml:aggregator to 1.0.52

# Release 2.3.62

* Update dependency io.cucumber:cucumber-picocontainer to v5.1.1

# Release 2.3.61

* No changes

# Release 2.3.60

* No changes

# Release 2.3.59

* No changes

# Release 2.3.58

* No changes

# Release 2.3.57

* No changes

# Release 2.3.56

* No changes

# Release 2.3.55

* No changes

# Release 2.3.54

* No changes

# Release 2.3.53

* No changes

# Release 2.3.52

* No changes

# Release 2.3.51

* No changes

# Release 2.3.50

* No changes

# Release 2.3.49

* No changes

# Release 2.3.48

* No changes

# Release 2.3.47

* No changes

# Release 2.3.46

* No changes

# Release 2.3.45

* No changes

# Release 2.3.44

* No changes

# Release 2.3.43

* No changes

# Release 2.3.42

* No changes

# Release 2.3.41

* No changes

# Release 2.3.40

* No changes

# Release 2.3.39

* No changes

# Release 2.3.38

* No changes

# Release 2.3.37

* No changes

# Release 2.3.36

* No changes

# Release 2.3.35

* No changes

# Release 2.3.34

* No changes

# Release 2.3.33

* No changes

# Release 2.3.32

* No changes

# Release 2.3.31

* No changes

# Release 2.3.30

* No changes

# Release 2.3.29

* No changes

# Release 2.3.28

* No changes

