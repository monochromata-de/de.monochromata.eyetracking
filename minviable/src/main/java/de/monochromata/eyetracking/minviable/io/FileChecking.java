package de.monochromata.eyetracking.minviable.io;

import java.io.File;

public interface FileChecking {

	static void requireExistingDirectory(final File dir) {
		requireFileToExist(dir);
		requireFileToBeADirectory(dir);
	}
	
	static void requireFileToExist(final File file) {
		if(!file.exists()) {
			throw new IllegalArgumentException(file.getAbsolutePath()+" does not exist");
		}
	}
	
	static void requireFileToBeADirectory(final File dir) {
		if(!dir.isDirectory()) {
			throw new IllegalArgumentException(dir.getAbsolutePath()+" is not a directory");
		}
	}
	
}
