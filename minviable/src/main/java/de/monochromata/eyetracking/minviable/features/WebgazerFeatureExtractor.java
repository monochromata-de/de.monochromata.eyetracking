package de.monochromata.eyetracking.minviable.features;

import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.createEyePatches;
import static de.monochromata.eyetracking.minviable.features.FeatureExtraction.addEyeData;
import static de.monochromata.eyetracking.minviable.features.FeatureExtraction.eyeDataLength;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.Eye;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.webcam.features.CompositeFeatureExtractor;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;

/**
 * Combines the features of both eyes, i.e. provide identical features for both
 * eyes.
 * <p>
 * This should be roughly equivalent to how WebGazer creates features.
 *
 * @see #ofImages(Function, int)
 * @see #ofPositions(Function, int)
 */
public class WebgazerFeatureExtractor implements FeatureExtractor<EyePatches> {

	private static final int LEFT_EYE_DATA_OFFSET = 0;

	private final int paddingPx;
	private final Consumer<Object[]> visualizer;
	private final int eyeDataLength;
	private final int rightEyeDataOffset;

	protected WebgazerFeatureExtractor(final int paddingPx) {
		this(paddingPx, null);
	}

	/**
	 * @param visualizer
	 *            will be passed all images during image processing, i.e. the
	 *            original image, the scaled image and the image after histogram
	 *            equalization. The {@link Object} array passed to the
	 *            visualizer contain the {@link Eye} the image is from, a
	 *            {@link String} naming the kind of image and a copy of the
	 *            image.
	 */
	protected WebgazerFeatureExtractor(final int paddingPx, final Consumer<Object[]> visualizer) {
		this.paddingPx = paddingPx;
		this.visualizer = visualizer;
		eyeDataLength = eyeDataLength(paddingPx);
		rightEyeDataOffset = LEFT_EYE_DATA_OFFSET + eyeDataLength;
	}

	@Override
	public FeatureExtractor<EyePatches> withVisualizer(final Consumer<Object[]> visualizer) {
		return new WebgazerFeatureExtractor(paddingPx, visualizer);
	}

	@Override
	public EyeFeatures apply(final EyePatches eyePatches) {
		final double[] features = new double[getNumberOfFeatures()];
		addEyeFeatures(eyePatches, features, Eye.LEFT, EyePatches::getLeftPatch, LEFT_EYE_DATA_OFFSET);
		addEyeFeatures(eyePatches, features, Eye.RIGHT, EyePatches::getRightPatch, rightEyeDataOffset);
		return new EyeFeatures(features, features);
	}

	private double[] addEyeFeatures(final EyePatches eyePatches, final double[] features, final Eye eye,
			final Function<EyePatches, EyePatch> eyePatchAccess, final int eyeDataOffset) {
		addEyeData(features, eyeDataOffset, eye, eyePatchAccess.apply(eyePatches), visualizer);
		return features;
	}

	@Override
	public int getNumberOfFeatures() {
		return 2 * eyeDataLength;
	}

	/**
	 * Create a new instance that consumes webcam images.
	 */
	public static FeatureExtractor<BufferedImage> ofImages(final Function<BufferedImage, EyePatches> eyePatchesDetector,
			final int paddingPx) {
		return new CompositeFeatureExtractor<>(eyePatchesDetector, new WebgazerFeatureExtractor(paddingPx));
	}

	/**
	 * Create a new instance that consumes positions that are part of
	 * {@link ClmtrackrResult} instances.
	 *
	 * @param boundsFunction
	 *            used to determine the position of the eye patches
	 */
	public static FeatureExtractor<ClmtrackrResult> ofPositions(
			final Function<List<Point2D>, Rectangle[]> boundsFunction, final int paddingPx) {
		return new CompositeFeatureExtractor<>(result -> createEyePatches(result, boundsFunction),
				new WebgazerFeatureExtractor(paddingPx));
	}

	public static FeatureExtractor<EyePatches> ofEyePatches(final int paddingPx) {
		return new WebgazerFeatureExtractor(paddingPx);
	}

}
