package de.monochromata.eyetracking.minviable.io.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.stream.Collectors;

import de.monochromata.eyetracking.minviable.io.index.VersionedIndexFileReader.IndexEntry;

public interface IndexFileReading extends IndexFileIO {
	
	static List<IndexEntry> readIndex(final File indexFile) {
		try (final BufferedReader reader = new BufferedReader(new FileReader(indexFile))) {
			final VersionedIndexFileReader versionedReader = getVersionedReader(reader);
			return reader.lines()
					.map(IndexFileReading::getCells)
					.map(versionedReader::getPointAndEyePatches)
					.collect(Collectors.toList());
		} catch (final IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
	}

	static VersionedIndexFileReader getVersionedReader(final BufferedReader reader) throws IOException {
		final String header = reader.readLine();
		if(header.equals(Version3IndexFileReader.getHeader())) {
			return new Version3IndexFileReader();
		} else {
			throw new IllegalStateException("No (supported) schema version found for header "+header);
		}
	}
	
	static String[] getCells(final String line) {
		return line.split(" ");
	}
	
}
