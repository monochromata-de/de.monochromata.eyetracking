package de.monochromata.eyetracking.minviable.eyes;

import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.createEyePatches;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.function.Function;

import de.monochromata.clmtrackr4j.ClmtrackrResult;

public class EyePatchDetector implements Function<BufferedImage,EyePatches> {

	private final Function<BufferedImage, ClmtrackrResult> positionsDetector;
	private final Function<List<Point2D>,Rectangle[]> boundsFunction;
	
	public EyePatchDetector(final Function<BufferedImage, ClmtrackrResult> positionsDetector,
			final Function<List<Point2D>,Rectangle[]> boundsFunction) {
		this.positionsDetector = positionsDetector;
		this.boundsFunction = boundsFunction;
	}
	
	@Override
	public EyePatches apply(final BufferedImage image) {
		final ClmtrackrResult result = positionsDetector.apply(image);
		return createEyePatches(result, boundsFunction);
	}

}
