package de.monochromata.eyetracking.minviable.features;

import java.util.function.Consumer;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;

/**
 * Provides the parameters of the face model (rotation, scaling, translation) as
 * features for both eyes.
 * <p>
 * This feature extractor shall be combined with
 * {@link BinocularFeatureExtractor} or {@link WebgazerFeatureExtractor}.
 *
 * @see BinocularFeatureExtractor
 * @see WebgazerFeatureExtractor
 */
public class FaceModelParametersFeatureExtractor implements FeatureExtractor<ClmtrackrResult> {

	@Override
	public EyeFeatures apply(final ClmtrackrResult result) {
		final double[] modelParameters = getModelParameters(result);
		return new EyeFeatures(modelParameters, modelParameters);
	}

	protected double[] getModelParameters(final ClmtrackrResult result) {
		return new double[] { result.getRotationRadians(), result.getScalingFactor(), result.getTranslateXPx(),
				result.getTransplateYPx() };
	}

	@Override
	public int getNumberOfFeatures() {
		return 4;
	}

	@Override
	public FeatureExtractor<ClmtrackrResult> withVisualizer(final Consumer<Object[]> visualizer) {
		// TODO: How could these features be visualized?
		throw new UnsupportedOperationException();
	}

}
