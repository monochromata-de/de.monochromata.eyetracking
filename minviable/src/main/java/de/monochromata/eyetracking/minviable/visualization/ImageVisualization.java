package de.monochromata.eyetracking.minviable.visualization;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public interface ImageVisualization {

	static void showImageInFrame(final BufferedImage image, final long waitDurationMs) {
		final JFrame frame = new JFrame();
		frame.getContentPane().add(new JLabel(new ImageIcon(image)), null);
		frame.pack();
		frame.setVisible(true);
		if(waitDurationMs > 0) {
			try {
				synchronized(frame) {
					frame.wait(waitDurationMs);
				}
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
		frame.setVisible(false);
		frame.dispose();
	}
	
}
