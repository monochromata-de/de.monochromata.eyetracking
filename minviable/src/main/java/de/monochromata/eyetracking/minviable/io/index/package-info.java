/**
 * I/O for indices of images and manual contexts.
 */
package de.monochromata.eyetracking.minviable.io.index;