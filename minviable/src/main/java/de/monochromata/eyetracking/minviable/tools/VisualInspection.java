package de.monochromata.eyetracking.minviable.tools;

import static de.monochromata.eyetracking.minviable.io.index.IndexFileReading.readIndex;
import static java.lang.Integer.parseInt;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.List;

import javax.swing.JFrame;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.io.index.VersionedIndexFileReader;
import de.monochromata.eyetracking.minviable.io.index.VersionedIndexFileReader.IndexEntry;
import de.monochromata.eyetracking.minviable.visualization.DatumVisualization;
import de.monochromata.eyetracking.minviable.visualization.ScatterPlotVisualization;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;

/**
 * A command for visual inspection of a data set to gauge calibration results.
 */
public class VisualInspection {

	private final List<IndexEntry> allData;
	private final ScatterPlotVisualization scatterPlot;
	private final JFrame frame;
	private final DetailView detailView;

	public VisualInspection(final File imagesDirectory, final File indexFile, final FeatureExtractor featureExtractor) {
		final List<IndexEntry> allTrainingData = readIndex(indexFile);
		final List<Point> trainingPoints = createTrainingPoints(allTrainingData);
		final List<Point> testPointsTruth = createEmptyTestDataTruth();
		final List<Point> testPointsPredictions = createEmptyTestDataPredictions();
		allData = allTrainingData;
		scatterPlot = createScatterPlotVisualization(trainingPoints, testPointsTruth, testPointsPredictions);
		frame = createJFrame(scatterPlot);
		detailView = new DetailView(imagesDirectory, featureExtractor);
	}

	public void show() {
		frame.setVisible(true);
	}

	private static ScatterPlotVisualization createScatterPlotVisualization(final List<Point> trainingData,
			final List<Point> testDataTruth, final List<Point> testDataPredictions) {
		return new ScatterPlotVisualization(trainingData, testDataTruth, testDataPredictions);
	}

	private static List<Point> createTrainingPoints(final List<IndexEntry> allTrainingData) {
		return allTrainingData.stream().map(VersionedIndexFileReader.IndexEntry::getPoint).collect(toList());
	}

	private static List<Point> createEmptyTestDataTruth() {
		return emptyList();
	}

	private static List<Point> createEmptyTestDataPredictions() {
		return emptyList();
	}

	private JFrame createJFrame(final ScatterPlotVisualization scatterPlot) {
		final JFrame frame = new JFrame();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setBounds(0, 0, (int) screenSize.getWidth(), (int) screenSize.getHeight());
		frame.getContentPane().add(scatterPlot);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.addMouseMotionListener(createDetailListener());
		return frame;
	}

	private MouseMotionListener createDetailListener() {
		return new MouseAdapter() {

			private IndexEntry closestEntry;

			@Override
			public void mouseMoved(final MouseEvent e) {
				final IndexEntry newClosestEntry = getClosestEntry(e);
				if (newClosestEntry == null) {
					closestEntry = null;
				} else if (newClosestEntry != closestEntry) {
					closestEntry = newClosestEntry;
					detailView.showDetail(newClosestEntry);

				}
			}
		};
	}

	private IndexEntry getClosestEntry(final MouseEvent e) {
		IndexEntry closestEntry = null;
		double minDistance = Double.MAX_VALUE;
		for (final IndexEntry entry : allData) {
			final Point point = entry.getPoint();
			final double distance = e.getPoint().distance(point.getX(), point.getY());
			if (distance < minDistance) {
				minDistance = distance;
				closestEntry = entry;
			}
		}
		System.err.println("Closest point: " + closestEntry.getPoint() + " at " + minDistance + "px distance");
		return closestEntry;
	}

	public static void main(final String[] args) throws Exception {
		requireArguments(args);
		final File imagesDirectory = new File(args[0]);
		final File indexFile = new File(args[1]);
		final FeatureExtractor featureExtractor = createFeatureExtractor(args[2], parseInt(args[3]));
		final VisualInspection inspection = new VisualInspection(imagesDirectory, indexFile, featureExtractor);
		inspection.show();
	}

	private static void requireArguments(final String[] args) {
		if (args.length != 4) {
			System.err.println("Usage: <imagesDirectory> <indexFile> <featureExtractorClass> <paddingPx>");
			System.exit(1);
		}
	}

	@SuppressWarnings("unchecked")
	private static FeatureExtractor createFeatureExtractor(final String className, final int paddingPx)
			throws Exception {
		return ((Class<FeatureExtractor>) Class.forName(className)).getConstructor(new Class[] { int.class })
				.newInstance(paddingPx);
	}

	private static class DetailView {

		private final DatumVisualization datumVisualization;
		private final JFrame jframe;

		private DetailView(final File imagesDirectory, final FeatureExtractor featureExtractor) {
			datumVisualization = new DatumVisualization(imagesDirectory, featureExtractor);
			jframe = new JFrame();
			jframe.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			jframe.getContentPane().add(datumVisualization);
		}

		private void showDetail(final IndexEntry data) {
			datumVisualization.setDatumToShow(data.getImageFilename());
			jframe.pack();
			ensureFrameIsVisible();
		}

		private void ensureFrameIsVisible() {
			if (!jframe.isVisible()) {
				jframe.setVisible(true);
			}
		}

	}
}
