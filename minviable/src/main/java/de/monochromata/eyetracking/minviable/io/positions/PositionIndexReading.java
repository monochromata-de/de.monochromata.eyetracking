package de.monochromata.eyetracking.minviable.io.positions;

import static de.monochromata.eyetracking.minviable.io.index.IOExceptionConversion.convertIOExceptionToUnchecked;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import de.monochromata.eyetracking.minviable.io.ClmtrackrFileResult;

public interface PositionIndexReading extends PositionIndexIO {

	static Stream<ClmtrackrFileResult> readResults(final File imagesDirectory, final File positionsIndex) {
		return convertIOExceptionToUnchecked(() -> {
			return new BufferedReader(new FileReader(positionsIndex)).lines().skip(1)
					.map(line -> parseResult(imagesDirectory, line));
		});
	}

	static ClmtrackrFileResult parseResult(final File imagesDirectory, final String line) {
		return convertIOExceptionToUnchecked(() -> {
			final String[] cells = line.split(" ");
			switch (cells[0]) {
			case "1":
				return readVersion1Result(imagesDirectory, cells);
			case "2":
				return readVersion2Result(imagesDirectory, cells);
			default:
				throw new IllegalArgumentException("Unknown schema version: " + cells[0]);
			}

		});
	}

	static ClmtrackrFileResult readVersion1Result(final File imagesDirectory, final String[] cells)
			throws IOException {
		final File imageFile = new File(imagesDirectory, cells[1]);
		final int iterations = parseInt(cells[2]);
		final double score = parseDouble(cells[3]);
		final double convergence = parseDouble(cells[4]);
		final List<Point2D> positions = parsePositions(cells[5]);
		return new ClmtrackrFileResult(imageFile, positions, iterations, score, convergence, 
				1.0d, 0.0d, 0.0d, 0.0d);
	}

	static ClmtrackrFileResult readVersion2Result(final File imagesDirectory, final String[] cells)
			throws IOException {
		final File imageFile = new File(imagesDirectory, cells[1]);
		final int iterations = parseInt(cells[2]);
		final double score = parseDouble(cells[3]);
		final double convergence = parseDouble(cells[4]);
		final double scalingFactor = parseDouble(cells[5]);
		final double rotationRadians = parseDouble(cells[6]);
		final double translationXPx = parseDouble(cells[7]);
		final double translationYPx = parseDouble(cells[8]);
		final List<Point2D> positions = parsePositions(cells[9]);
		return new ClmtrackrFileResult(imageFile, positions, iterations, score, convergence, 
			scalingFactor, rotationRadians, translationXPx, translationYPx);
	}

	static List<Point2D> parsePositions(final String positionsString) {
		return stream(positionsString.split(";")).map(PositionIndexReading::parsePoint2D).collect(toList());
	}

	static Point2D parsePoint2D(final String data) {
		final String[] parts = data.split(",");
		return new Point2D.Double(parseDouble(parts[0]), parseDouble(parts[1]));
	}

}
