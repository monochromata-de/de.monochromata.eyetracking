/**
 * Representations of the eyes and eye images.
 */
package de.monochromata.eyetracking.minviable.eyes;