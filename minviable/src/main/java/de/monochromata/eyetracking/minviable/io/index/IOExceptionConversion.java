package de.monochromata.eyetracking.minviable.io.index;

import java.io.IOException;
import java.io.UncheckedIOException;

public interface IOExceptionConversion {

	static void convertIOExceptionToUnchecked(final IOExceptionRaiser raiser) {
		convertIOExceptionToUnchecked(() -> {
			raiser.run();
			return null;
		});
	}

	static <T> T convertIOExceptionToUnchecked(final IOExceptionRaisingSupplier<T> supplier) {
		try {
			return supplier.run();
		} catch(final IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
	}

	interface IOExceptionRaiser {
		public void run() throws IOException;
	}

	interface IOExceptionRaisingSupplier<T> {
		public T run() throws IOException;
	}

}
