package de.monochromata.eyetracking.minviable.visualization;

import static java.lang.Math.round;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class GraphicsPainter implements Painter {

	@Override
	public void drawFace(final BufferedImage image, final List<Point2D> positions) {
		drawWithColor(image, Color.GREEN, graphics -> {
			// Indices are based on http://www.auduno.com/clmtrackr/docs/reference.html
			drawPolyline(graphics, positions,  0, 15); // cheeks and chin
			drawPolyline(graphics, positions, 15, 19); // right eyebrow
			drawPolyline(graphics, positions, 19, 23); // left eyebrow
			drawPoint(graphics, positions.get(27)); // left pupil center
			drawPolyline(graphics, getPositions(positions, 23, 63, 24, 64, 25, 65, 26, 66, 23)); // left eye contour
			drawPoint(graphics, positions.get(32)); // right pupil center
			drawPolyline(graphics, getPositions(positions, 30, 68, 29, 67, 28, 70, 31, 69, 30)); // right eye contour
			drawPolyline(graphics, getPositions(positions, 33, 41, 62)); // nasal ridge
			drawPolyline(graphics, getPositions(positions, 34, 35, 36, 42, 37, 43, 38, 39, 40)); // nasal wings
			drawPolyline(graphics, getPositions(positions, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 44)); // lips outside
			drawPolyline(graphics, getPositions(positions, 44, 56, 57, 58, 50, 59, 60, 61, 44)); // lips inside
		});
	}

	@Override
	public void drawEyeBounds(final BufferedImage image,
			final Rectangle leftEyeBounds, final Rectangle rightEyeBounds) {
		drawWithColor(image, Color.BLUE, graphics -> {
			graphics.drawRect(leftEyeBounds.x, leftEyeBounds.y, 
					leftEyeBounds.width, leftEyeBounds.height);
			graphics.drawRect(rightEyeBounds.x, rightEyeBounds.y, 
					rightEyeBounds.width, rightEyeBounds.height);
			});
	}
	
	protected void drawPoint(final Graphics2D graphics, final Point2D point) {
		final int x = (int)Math.round(point.getX());
		final int y = (int)Math.round(point.getY());
		graphics.drawLine(x, y, x, y);
	}
	
	@Override
	public void drawRect(final Graphics2D graphics, final Rectangle rectangle) {
		graphics.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}

	protected void drawPolyline(final Graphics2D graphics, final List<Point2D> positions,
			final int fromIndexInclusive, final int toIndexExclusive) {
		final int numberOfPoints = toIndexExclusive - fromIndexInclusive;
		final int[] xCoordinates = new int[numberOfPoints];
		final int[] yCoordinates = new int[numberOfPoints];
		for(int i=fromIndexInclusive;i<toIndexExclusive;i++) {
			final Point2D point = positions.get(i);
			xCoordinates[i-fromIndexInclusive] = (int)round(point.getX());
			yCoordinates[i-fromIndexInclusive] = (int)round(point.getY());
		}
		graphics.drawPolyline(xCoordinates, yCoordinates, numberOfPoints);
	}
	
	protected void drawPolyline(final Graphics2D graphics, final List<Point2D> positions) {
		final int numberOfPoints = positions.size();
		final int[] xCoordinates = new int[numberOfPoints];
		final int[] yCoordinates = new int[numberOfPoints];
		addCoordinates(positions, numberOfPoints, xCoordinates, yCoordinates);
		graphics.drawPolyline(xCoordinates, yCoordinates, numberOfPoints);
	}

	private void addCoordinates(final List<Point2D> positions, final int numberOfPoints, final int[] xCoordinates,
			final int[] yCoordinates) {
		IntStream.range(0, numberOfPoints)
			.forEach(i -> 
				addCoordinate(positions, xCoordinates, yCoordinates, i));
	}

	private void addCoordinate(final List<Point2D> positions, final int[] xCoordinates, final int[] yCoordinates,
			final int i) {
		final Point2D position = positions.get(i);
		xCoordinates[i] = (int)round(position.getX());
		yCoordinates[i] = (int)round(position.getY());
	}
	
	protected List<Point2D> getPositions(final List<Point2D> positions,
			final Integer... indices) {
		return stream(indices).map(positions::get).collect(toList());
	}
	
	protected void drawWithColor(final BufferedImage image, final Color color, final Consumer<Graphics2D> painter) {
		final Graphics2D graphics = image.createGraphics();
		final Color oldColor = graphics.getColor();
		graphics.setColor(color);
		painter.accept(graphics);
		graphics.setColor(oldColor);
	}

}
