package de.monochromata.eyetracking.minviable.eyes;

import static java.lang.Double.NaN;

/**
 * A container for eye patches for a face.
 */
public class EyePatches {
	
	private final EyePatch leftPatch;
	private final EyePatch rightPatch;
	private final int iterations;
	private final double score;
	private final double convergence;
	
	public EyePatches(final EyePatch leftPatch, final EyePatch rightPatch) {
		this(leftPatch, rightPatch, -1, NaN, NaN);
	}
	
	public EyePatches(final EyePatch leftPatch, final EyePatch rightPatch, final int iterations, final double score, final double convergence) {
		this.leftPatch = leftPatch;
		this.rightPatch = rightPatch;
		this.iterations = iterations;
		this.score = score;
		this.convergence = convergence;
	}

	public EyePatch getLeftPatch() {
		return leftPatch;
	}

	public EyePatch getRightPatch() {
		return rightPatch;
	}

	public int getIterations() {
		return iterations;
	}

	public double getScore() {
		return score;
	}
	
	public double getConvergence() {
		return convergence;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(convergence);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + iterations;
		result = prime * result + ((leftPatch == null) ? 0 : leftPatch.hashCode());
		result = prime * result + ((rightPatch == null) ? 0 : rightPatch.hashCode());
		temp = Double.doubleToLongBits(score);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EyePatches other = (EyePatches) obj;
		if (Double.doubleToLongBits(convergence) != Double.doubleToLongBits(other.convergence))
			return false;
		if (iterations != other.iterations)
			return false;
		if (leftPatch == null) {
			if (other.leftPatch != null)
				return false;
		} else if (!leftPatch.equals(other.leftPatch))
			return false;
		if (rightPatch == null) {
			if (other.rightPatch != null)
				return false;
		} else if (!rightPatch.equals(other.rightPatch))
			return false;
		if (Double.doubleToLongBits(score) != Double.doubleToLongBits(other.score))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EyePatches [leftPatch=" + leftPatch + ", rightPatch=" + rightPatch + ", iterations=" + iterations
				+ ", score=" + score + ", convergence=" + convergence + "]";
	}

}
