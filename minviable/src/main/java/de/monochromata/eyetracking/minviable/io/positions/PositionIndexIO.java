package de.monochromata.eyetracking.minviable.io.positions;

public interface PositionIndexIO {
	
	public static final int CURRENT_SCHEMA_VERSION = 2;
	
	public static final String HEADER_FOR_SCHEMA_VERSION_1 = "schemaVersion imageFile iterations score convergence positions";

	public static final String HEADER_FOR_SCHEMA_VERSION_2 = "schemaVersion imageFile iterations score convergence scalingFactor rotationRadians translateXPx translateYPx positions";
	
}
