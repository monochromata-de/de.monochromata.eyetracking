package de.monochromata.eyetracking.minviable.io.index;

import static de.monochromata.eyetracking.Eye.LEFT;
import static de.monochromata.eyetracking.Eye.RIGHT;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import de.monochromata.eyetracking.Eye;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;

public interface VersionedIndexFileReader {

	default int indexOfSchemaVersion() {
		return 0;
	}

	default int indexOfImageFilename() {
		return 1;
	}

	default int indexOfTimestamp() {
		return 2;
	}

	default int indexOfManualContextX() {
		return 3;
	}

	default int indexOfManualContextY() {
		return 4;
	}

	default int indexOfIterations() {
		return 5;
	}

	default int indexOfScore() {
		return 6;
	}

	default int indexOfConvergence() {
		return 7;
	}

	default int indexOfLeftEyeX() {
		return 8;
	}

	default int indexOfLeftEyeY() {
		return 9;
	}

	default int indexOfLeftEyeWidth() {
		return 10;
	}

	default int indexOfLeftEyeHeight() {
		return 11;
	}

	default int indexOfLeftEyeData() {
		return 12;
	}

	default int indexOfRighEyeX() {
		return 13;
	}

	default int indexOfRightEyeY() {
		return 14;
	}

	default int indexOfRightEyeWidth() {
		return 15;
	}

	default int indexOfRightEyeHeight() {
		return 16;
	}

	default int indexOfRightEyeData() {
		return 17;
	}

	IndexEntry getPointAndEyePatches(final String[] cells);

	default Point getManualContextPoint(final String[] cells) {
		return new PointImpl(getManualContextX(cells), getManualContextY(cells));
	}

	default int getManualContextX(final String[] cells) {
		return Integer.parseInt(cells[indexOfManualContextX()]);
	}

	default int getManualContextY(final String[] cells) {
		return Integer.parseInt(cells[indexOfManualContextY()]);
	}

	default int getEyePatchX(final String[] cells, final Eye eye) {
		return Integer.parseInt(cells[eye == LEFT ? indexOfLeftEyeX() : indexOfRighEyeX()]);
	}

	default int getEyePatchY(final String[] cells, final Eye eye) {
		return Integer.parseInt(cells[eye == LEFT ? indexOfLeftEyeY() : indexOfRightEyeY()]);
	}

	default int getEyePatchWidth(final String[] cells, final Eye eye) {
		return Integer.parseInt(cells[eye == LEFT ? indexOfLeftEyeWidth() : indexOfRightEyeWidth()]);
	}

	default int getEyePatchHeight(final String[] cells, final Eye eye) {
		return Integer.parseInt(cells[eye == LEFT ? indexOfLeftEyeHeight() : indexOfRightEyeHeight()]);
	}

	default double[] getEyePatchPixels(final String[] cells, final Eye eye) {
		final String dataString = cells[eye == LEFT ? indexOfLeftEyeData() : indexOfRightEyeData()].replace("[", "")
				.replace("]", "");
		return Arrays.stream(dataString.split(",")).mapToDouble(Double::parseDouble).toArray();
	}

	default EyePatches createEyePatches(final String[] cells) {
		final EyePatch leftEyePatch = createEyePatch(cells, LEFT);
		final EyePatch rightEyePatch = createEyePatch(cells, RIGHT);
		return createEyePatches(cells, leftEyePatch, rightEyePatch);
	}

	default EyePatches createEyePatches(final String[] cells, final EyePatch leftEyePatch,
			final EyePatch rightEyePatch) {
		return new EyePatches(leftEyePatch, rightEyePatch);
	}

	default EyePatch createEyePatch(final String[] cells, final Eye eye) {
		final int x = getEyePatchX(cells, eye);
		final int y = getEyePatchY(cells, eye);
		final int width = getEyePatchWidth(cells, eye);
		final int height = getEyePatchHeight(cells, eye);
		final double[] data = getEyePatchPixels(cells, eye);
		return new EyePatch(x, y, width, height, data);
	}

	/**
	 * @throws UncheckedIOException
	 */
	default BufferedImage readImage(final String[] cells) {
		try {
			final String filename = cells[indexOfImageFilename()];
			return ImageIO.read(new File(filename));
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	class IndexEntry {

		private final Point point;
		private final EyePatches eyePatches;
		private final int schemaVersion;
		private final String imageFilename;
		private final long timestamp;
		private final int iterations;
		private final double score;
		private final double convergence;

		public IndexEntry(final Point point, final EyePatches eyePatches, final int schemaVersion,
				final String imageFilename, final long timestamp, final int iterations, final double score,
				final double convergence) {
			this.point = point;
			this.eyePatches = eyePatches;
			this.schemaVersion = schemaVersion;
			this.imageFilename = imageFilename;
			this.timestamp = timestamp;
			this.iterations = iterations;
			this.score = score;
			this.convergence = convergence;
		}

		public Point getPoint() {
			return point;
		}

		public EyePatches getEyePatches() {
			return eyePatches;
		}

		public int getSchemaVersion() {
			return schemaVersion;
		}

		public String getImageFilename() {
			return imageFilename;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public int getIterations() {
			return iterations;
		}

		public double getScore() {
			return score;
		}

		public double getConvergence() {
			return convergence;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(convergence);
			result = prime * result + (int) (temp ^ temp >>> 32);
			result = prime * result + (eyePatches == null ? 0 : eyePatches.hashCode());
			result = prime * result + (imageFilename == null ? 0 : imageFilename.hashCode());
			result = prime * result + iterations;
			result = prime * result + (point == null ? 0 : point.hashCode());
			result = prime * result + schemaVersion;
			temp = Double.doubleToLongBits(score);
			result = prime * result + (int) (temp ^ temp >>> 32);
			result = prime * result + (int) (timestamp ^ timestamp >>> 32);
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final IndexEntry other = (IndexEntry) obj;
			if (Double.doubleToLongBits(convergence) != Double.doubleToLongBits(other.convergence)) {
				return false;
			}
			if (eyePatches == null) {
				if (other.eyePatches != null) {
					return false;
				}
			} else if (!eyePatches.equals(other.eyePatches)) {
				return false;
			}
			if (imageFilename == null) {
				if (other.imageFilename != null) {
					return false;
				}
			} else if (!imageFilename.equals(other.imageFilename)) {
				return false;
			}
			if (iterations != other.iterations) {
				return false;
			}
			if (point == null) {
				if (other.point != null) {
					return false;
				}
			} else if (!point.equals(other.point)) {
				return false;
			}
			if (schemaVersion != other.schemaVersion) {
				return false;
			}
			if (Double.doubleToLongBits(score) != Double.doubleToLongBits(other.score)) {
				return false;
			}
			if (timestamp != other.timestamp) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return "IndexEntry [point=" + point + ", eyePatches=" + eyePatches + ", schemaVersion=" + schemaVersion
					+ ", imageFilename=" + imageFilename + ", timestamp=" + timestamp + ", iterations=" + iterations
					+ ", score=" + score + ", convergence=" + convergence + "]";
		}

	}

}
