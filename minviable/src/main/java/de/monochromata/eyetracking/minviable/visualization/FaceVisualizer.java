package de.monochromata.eyetracking.minviable.visualization;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class FaceVisualizer extends AbstractSwingVisualizer {

	public FaceVisualizer() {
	}

	public FaceVisualizer(final JFrame frame) {
		super(frame);
	}

	public FaceVisualizer(final JFrame frame, final Painter painter) {
		super(frame, painter);
	}

	@Override
	protected GridLayout getLayout() {
		return new GridLayout(1, 1);
	}
	
}
