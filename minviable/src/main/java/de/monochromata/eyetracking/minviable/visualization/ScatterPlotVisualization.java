package de.monochromata.eyetracking.minviable.visualization;

import static java.awt.Color.BLUE;
import static java.awt.Color.GREEN;
import static java.awt.Color.RED;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.JPanel;

import de.monochromata.eyetracking.Point;

public class ScatterPlotVisualization extends JPanel {

	private static final long serialVersionUID = 3279189185399796871L;
	private static final int POINT_RADIUS_PX = 3;
	private static final int POINT_DIAMETER_PX = 6;
	
	private final List<Point> trainingData;
	private final List<Point> testDataTruth;
	private final List<Point> testDataPredictions;
	
	public ScatterPlotVisualization(final List<Point> trainingData, final List<Point> testDataTruth,
			final List<Point> testDataPredictions) {
		this.trainingData = trainingData;
		this.testDataTruth = testDataTruth;
		this.testDataPredictions = testDataPredictions;
	}

	@Override
	protected void paintComponent(final Graphics g) {
		super.paintComponent(g);
		paintScatterPlot(g);
	}

	public void paintScatterPlot(final Graphics g) {
		paintScatterPlot(g, trainingData, BLUE);
		paintScatterPlot(g, testDataTruth, GREEN);
		paintScatterPlot(g, testDataPredictions, RED);
		connectPoints(g, testDataTruth, testDataPredictions, RED);
	}
	
	protected void paintScatterPlot(final Graphics g, final List<Point> points, final Color color) {
		points.forEach(point -> paintPoint(g, point, color));
	}
	
	protected void paintPoint(final Graphics g, final Point point, final Color color) {
		withColor(g, color, () -> fillOval(g, point));
	}

	protected void fillOval(final Graphics g, final Point point) {
		g.fillOval(point.getX()-POINT_RADIUS_PX,
				point.getY()-POINT_RADIUS_PX,
				POINT_DIAMETER_PX, POINT_DIAMETER_PX);
	}
	
	protected void connectPoints(final Graphics g, final List<Point> startingPoints,
			final List<Point> endPoints, final Color color) {
		IntStream.range(0, startingPoints.size()).forEach(i -> 
				connectPoints(g, startingPoints.get(i), endPoints.get(i), color));
	}
	
	protected void connectPoints(final Graphics g, final Point startingPoint,
			final Point endPoint, final Color color) {
		withColor(g, color, () -> drawLine(g, startingPoint, endPoint));
	}
	
	protected void drawLine(final Graphics g, final Point startingPoint, final Point endPoint) {
		g.drawLine(startingPoint.getX(), startingPoint.getY(),
				endPoint.getX(), endPoint.getY());
	}
	
	protected void withColor(final Graphics g, final Color color, final Runnable runnable) {
		final Color oldColor = g.getColor();
		g.setColor(color);
		runnable.run();
		g.setColor(oldColor);
	}
}
