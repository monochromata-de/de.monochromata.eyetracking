package de.monochromata.eyetracking.minviable.io.index;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;

public class Version3IndexFileReader implements VersionedIndexFileReader {

	public static String getHeader() {
		return "schemaVersion imageFilename timestamp manualContextX manualContextY iterations score convergence leftEyeX leftEyeY leftEyeWidth leftEyeHeight leftEyeData rightEyeX rightEyeY rightEyeWidth rightEyeHeight rightEyeData";
	}

	public int getSchemaVersion(final String[] cells) {
		return parseInt(cells[indexOfSchemaVersion()]);
	}

	public String getImageFilename(final String[] cells) {
		return cells[indexOfImageFilename()];
	}

	public long getTimestamp(final String[] cells) {
		return parseLong(cells[indexOfTimestamp()]);
	}

	public int getIterations(final String[] cells) {
		return parseInt(cells[indexOfIterations()]);
	}

	public double getScore(final String[] cells) {
		return parseDouble(cells[indexOfScore()]);
	}

	public double getConvergence(final String[] cells) {
		return parseDouble(cells[indexOfConvergence()]);
	}

	@Override
	public IndexEntry getPointAndEyePatches(final String[] cells) {
		final Point point = getManualContextPoint(cells);
		final EyePatches eyePatches = createEyePatches(cells);
		return new IndexEntry(point, eyePatches, getSchemaVersion(cells), getImageFilename(cells), getTimestamp(cells),
				getIterations(cells), getScore(cells), getConvergence(cells));
	}

}
