package de.monochromata.eyetracking.minviable.tools;

import static de.monochromata.clmtrackr4j.ClmtrackrResultTransformation.transformResult;
import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.createEyePatches;
import static de.monochromata.eyetracking.minviable.io.FileChecking.requireExistingDirectory;
import static de.monochromata.eyetracking.minviable.io.FileChecking.requireFileToExist;
import static de.monochromata.eyetracking.minviable.io.index.IOExceptionConversion.convertIOExceptionToUnchecked;
import static de.monochromata.eyetracking.minviable.io.index.IndexFileIO.getImageFiles;
import static de.monochromata.eyetracking.minviable.io.index.IndexFileWriting.addEyePatchesToIndexFile;
import static de.monochromata.eyetracking.minviable.io.index.IndexFileWriting.createIndexWriterAndWriteHeaderLine;
import static de.monochromata.eyetracking.minviable.io.positions.PositionIndexReading.readResults;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.io.ClmtrackrFileResult;

/**
 * Converts positions from a positions file created by
 * {@link HighQualityPositionsDetector} into eye patches for all files in a
 * given input directory and saves the eye patches files as well as the index
 * file in an output directory.
 */
public class PositionsToEyePatches {

	private final File imageDirectory;
	private final File positionsFile;
	private final Function<List<Point2D>, Rectangle[]> boundsFunction;
	private final Function<ClmtrackrResult, EyePatches> converter;

	public PositionsToEyePatches(final String imageDirectory, final String positionsFile) {
		this(new File(imageDirectory), new File(positionsFile));
	}

	public PositionsToEyePatches(final File imageDirectory, final File positionsFile) {
		requireExistingDirectory(imageDirectory);
		requireFileToExist(positionsFile);
		this.imageDirectory = imageDirectory;
		this.positionsFile = positionsFile;
		boundsFunction = BoundsFunctions::getEyeBoundsFromEyeCorners;
		// this.boundsFunction =
		// ClmtrackerEyePatchesExtraction::getEyeBoundsFromPupilPosition;
		/*
		 * this.boundsFunction = positions -> EyePatchesExtraction.applyPadding(
		 * ClmtrackerEyePatchesExtraction.getEyeBoundsFromEyeCorners(positions),
		 * 8);
		 */
		converter = result -> createEyePatches(transformResult(result, 2.0d, VALUE_INTERPOLATION_BILINEAR),
				boundsFunction);
	}

	public void runBatch() {
		convertIOExceptionToUnchecked(this::runBatchWithCheckedException);
	}

	private void runBatchWithCheckedException() throws IOException {
		final List<File> inputImageFiles = getImageFiles(imageDirectory);
		try (final BufferedWriter indexFileWriter = createIndexWriterAndWriteHeaderLine(positionsFile.getParentFile(),
				inputImageFiles.size())) {
			addEyePatchesToIndexFile(indexFileWriter, getImageFileAndEyePatchesStream());
		}
	}

	private Stream<Object[]> getImageFileAndEyePatchesStream() {
		return readResults(imageDirectory, positionsFile).map(this::getImageFileAndEyePatches);
	}

	private Object[] getImageFileAndEyePatches(final ClmtrackrFileResult result) {
		final File imageFile = result.getImageFile();
		final EyePatches eyePatches = converter.apply(result);
		return new Object[] { imageFile, eyePatches };
	}

	public static void main(final String[] args) {
		final String imageDirectory = args[0];
		final String positionsFile = args[1];
		new PositionsToEyePatches(imageDirectory, positionsFile).runBatch();
	}

}
