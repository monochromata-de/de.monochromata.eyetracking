package de.monochromata.eyetracking.minviable.io;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import javax.imageio.ImageIO;

import de.monochromata.clmtrackr4j.ClmtrackrResult;

public class ClmtrackrFileResult extends ClmtrackrResult {

	private BufferedImage cachedImage;
	private final File imageFile;

	public ClmtrackrFileResult(final File imageFile, final List<Point2D> positions, final int iterations,
			final double score, final double convergence, final double scalingFactor, final double rotationRadians,
			final double translateXPx, final double transplateYPx) throws IOException {
		super(null, positions, iterations, score, convergence, scalingFactor, rotationRadians, translateXPx,
				transplateYPx);
		this.imageFile = imageFile;
	}

	/**
	 * Lazily loads the image from the image file.
	 */
	@Override
	public BufferedImage getImage() {
		ensureCachedImageIsAvailable();
		return cachedImage;
	}

	protected void ensureCachedImageIsAvailable() {
		if (cachedImage == null) {
			try {
				cachedImage = ImageIO.read(imageFile);
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		}
	}

	public File getImageFile() {
		return imageFile;
	}

}
