package de.monochromata.eyetracking.minviable;

import static java.lang.Math.round;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

import de.monochromata.event.AbstractTracker;
import de.monochromata.eyetracking.ControllableEyeTracker;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.webcam.svm.Gaze;
import de.monochromata.eyetracking.webcam.svm.GazeTracker;

public class MinimumViableEyeTracker extends AbstractTracker<Consumer<Point>> implements ControllableEyeTracker {

	private final BiConsumer<Gaze, Gaze> gazeListener = this::notifyListeners;
	private final Supplier<GazeTracker> gazeTrackerSupplier;
	private final BiFunction<Gaze, Gaze, Point> gazeConverter;
	private boolean selected = false;
	private boolean tracking = false;
	private GazeTracker tracker;

	/**
	 * @param gazeTrackerSupplier
	 *            Used to obtain a {@link GazeTracker} when tracking is started
	 *            due to an invocation of {@link #setSelected(boolean)}
	 * @param gazeConverter
	 *            Use to obtain a point from left and right gaze estimation.
	 *            Either one of the two gaze estimations is converted to a point
	 *            using {@link #toPoint(Gaze)}, or the two estimations are
	 *            combined.
	 * @see #toPoint(Gaze)
	 */
	public MinimumViableEyeTracker(final Supplier<GazeTracker> gazeTrackerSupplier,
			final BiFunction<Gaze, Gaze, Point> gazeConverter) {
		this.gazeTrackerSupplier = gazeTrackerSupplier;
		this.gazeConverter = gazeConverter;
	}

	@Override
	public void setSelected(final boolean selected) {
		if (this.selected && tracking && !selected) {
			throw new IllegalStateException("Cannot de-select while tracking, invoke setTracking(false) first.");
		}
		this.selected = selected;
	}

	@Override
	public boolean getSelected() {
		return selected;
	}

	@Override
	public void setTracking(final boolean track) {
		if (!getSelected() && track) {
			throw new IllegalStateException(
					"Cannot start tracking while not selected, invoke setSelected(true) first.");
		}
		if (!tracking && track) {
			startTracking();
		} else if (tracking && !track) {
			stopTracking();
		}
		tracking = track;
	}

	protected void startTracking() {
		tracker = gazeTrackerSupplier.get();
		tracker.addListener(gazeListener);
	}

	protected void stopTracking() {
		tracker.removeListener(gazeListener);
		tracker.stop();
		tracker = null;
	}

	@Override
	public boolean getTracking() {
		return tracking;
	}

	protected void notifyListeners(final Gaze leftGaze, final Gaze rightGaze) {
		notifyListeners(gazeConverter.apply(leftGaze, rightGaze));
	}

	public static PointImpl toPoint(final Gaze gaze) {
		return new PointImpl((int) round(gaze.getXEstimate()), (int) Math.round(gaze.getYEstimate()));
	}

	protected void notifyListeners(final Point point) {
		notifyListeners(listener -> listener.accept(point));
	}

}
