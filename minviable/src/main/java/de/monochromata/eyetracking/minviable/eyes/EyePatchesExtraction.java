package de.monochromata.eyetracking.minviable.eyes;

import static de.monochromata.eyetracking.minviable.visualization.ImageVisualization.showImageInFrame;
import static de.monochromata.eyetracking.webcam.image.ImageManipulation.ensureGrayscale;
import static java.util.Arrays.stream;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.function.Function;

import de.monochromata.clmtrackr4j.ClmtrackrResult;

public interface EyePatchesExtraction {

	int TARGET_EYE_PATCH_WIDTH = 10;
	int TARGET_EYE_PATCH_HEIGHT = 6;

	/**
	 * Create eye patches without transforming the image in the given result.
	 */
	static EyePatches createEyePatches(final ClmtrackrResult result,
			final Function<List<Point2D>, Rectangle[]> boundsFunction) {
		if (result == null) {
			return null;
		}
		return createEyePatchesFromNonNullResult(result, boundsFunction);
	}

	static EyePatches createEyePatchesFromNonNullResult(final ClmtrackrResult result,
			final Function<List<Point2D>, Rectangle[]> boundsFunction) {
		final Rectangle[] leftAndRightEyeBounds = boundsFunction.apply(result.getPositions());
		return createEyePatches(result, leftAndRightEyeBounds[0], leftAndRightEyeBounds[1]);
	}

	static EyePatches createEyePatches(final ClmtrackrResult result, final Rectangle leftEyeBounds,
			final Rectangle rightEyeBounds) {
		final BufferedImage grayscaleImage = ensureGrayscale(result.getImage());
		final EyePatch leftPatch = createEyePatch(grayscaleImage, leftEyeBounds);
		final EyePatch rightPatch = createEyePatch(grayscaleImage, rightEyeBounds);
		return new EyePatches(leftPatch, rightPatch, result.getIterations(), result.getScore(),
				result.getConvergence());
	}

	static EyePatch createEyePatch(final BufferedImage image, final Rectangle bounds) {
		final double[] data = new double[bounds.width * bounds.height];
		try {
			image.getRaster().getPixels(bounds.x, bounds.y, bounds.width, bounds.height, data);
		} catch (final ArrayIndexOutOfBoundsException e) {
			System.err.println(
					"image.width=" + image.getWidth() + " image.height=" + image.getHeight() + " bounds=" + bounds);
			showImageInFrame(image, 3000);
			e.printStackTrace();
		}
		return new EyePatch(bounds.x, bounds.y, bounds.width, bounds.height, data);
	}

	/**
	 * Equivalent to
	 * {@code getCenteredStanardBounds(bounds, TARGET_EYE_PATCH_WIDTH, TARGET_EYE_PATCH_HEIGHT)}
	 * .
	 *
	 * @see #getCenteredStandardBounds(Rectangle, int, int)
	 * @see #TARGET_EYE_PATCH_WIDTH
	 * @see #TARGET_EYE_PATCH_HEIGHT
	 */
	static Rectangle getCenteredStandardBounds(final Rectangle bounds) {
		return getCenteredStandardBounds(bounds, TARGET_EYE_PATCH_WIDTH, TARGET_EYE_PATCH_HEIGHT);
	}

	/**
	 * Returns new bounds that align the given width and height at the center of
	 * the given bounds.
	 *
	 * @see #TARGET_EYE_PATCH_WIDTH
	 * @see #TARGET_EYE_PATCH_HEIGHT
	 */
	static Rectangle getCenteredStandardBounds(final Rectangle bounds, final int width, final int height) {
		final int centerX = bounds.x + bounds.width / 2;
		final int centerY = bounds.y + bounds.height / 2;
		return new Rectangle(centerX - width / 2, centerY - height / 2, width, height);
	}

	static Rectangle[] applyPadding(final Rectangle[] originalBounds, final int padding) {
		return stream(originalBounds).map(bounds -> applyPadding(bounds, padding)).toArray(Rectangle[]::new);
	}

	/**
	 * Pad the given rectangle with the given padding.
	 */
	static Rectangle applyPadding(final Rectangle originalBounds, final int padding) {
		return new Rectangle(originalBounds.x - padding, originalBounds.y - padding, originalBounds.width + padding * 2,
				originalBounds.height + padding * 2);
	}

}
