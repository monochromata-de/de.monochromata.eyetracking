package de.monochromata.eyetracking.minviable.visualization;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import de.monochromata.clmtrackr4j.js.Context2D;

public abstract class AbstractSwingVisualizer implements Visualizer {

	private final JFrame frame;
	private final Painter painter;
	private final ImageIcon imageIcon = new ImageIcon();
	protected final Map<BufferedImage,BufferedImage> imageCopies = new WeakHashMap<>();
	private Rectangle lastFaceBounds;

	public AbstractSwingVisualizer() {
		this(new JFrame(), new GraphicsPainter());
	}
	
	public AbstractSwingVisualizer(final JFrame frame) {
		this(frame, new GraphicsPainter());
	}
	
	public AbstractSwingVisualizer(final JFrame frame, final Painter painter) {
		this.frame = frame;
		this.painter = painter;
	}

	public JFrame getFrame() {
		return frame;
	}

	public Painter getPainter() {
		return painter;
	}

	@Override
	public void initialize(final BufferedImage initialImage) {
		initializeImageIcon(copy(initialImage));
		initializeChangeListener();
	}

	private void initializeImageIcon(final BufferedImage writableImage) {
		frame.setLayout(getLayout());
		imageIcon.setImage(writableImage);
		frame.add(new JLabel(imageIcon));
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private void initializeChangeListener() {
		Context2D.addChangeListener(context2D -> {
			copyIfDisplayed(context2D);
			frame.repaint();
		});
	}
	
	protected abstract GridLayout getLayout();

	@Override
	public void faceFound(final BufferedImage image, final Rectangle faceBounds) {
		lastFaceBounds = faceBounds;
		final BufferedImage target = copy(image);
		imageIcon.setImage(target);
		drawFaceBounds(target, faceBounds);
		frame.repaint();
	}

	@Override
	public void eyesFound(final BufferedImage image, final List<Point2D> facePoints) {
		final BufferedImage target = copy(image);
		imageIcon.setImage(target);
		drawFaceBounds(target, lastFaceBounds);
		drawOnImage(target, facePoints);
		frame.repaint();
	}

	private void drawFaceBounds(final BufferedImage target, final Rectangle faceBounds) {
		if(faceBounds != null) {
			painter.drawRect(target.createGraphics(), faceBounds);
		}
	}
	
	protected void drawOnImage(final BufferedImage image, final List<Point2D> facePoints) {
		painter.drawFace(image, facePoints);
	}

	@Override
	public void noEyesFound(final BufferedImage image) {
		imageIcon.setImage(copy(image));
		frame.repaint();
	}
	
	protected BufferedImage copy(final BufferedImage original) {
		final ColorModel colorModel = original.getColorModel();
		final BufferedImage copy = new BufferedImage(colorModel, original.copyData(null),
				colorModel.isAlphaPremultiplied(), null);
		imageCopies.put(original, copy);
		return copy;
	}
	
	protected void copyIfDisplayed(final Context2D context2D) {
		final BufferedImage original = context2D.getBufferedImage();
		final BufferedImage copy = imageCopies.get(original);
		if(copy != null) {
			copy(original, copy);
		}
	}
	
	protected void copy(final BufferedImage from, final BufferedImage to) {
		from.copyData(to.getRaster());
	}
	
}
