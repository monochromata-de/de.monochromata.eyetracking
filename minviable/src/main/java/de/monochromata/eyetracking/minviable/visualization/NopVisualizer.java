package de.monochromata.eyetracking.minviable.visualization;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

public class NopVisualizer implements Visualizer {

	@Override
	public void initialize(final BufferedImage initialImage) {
	}

	@Override
	public void faceFound(final BufferedImage image, final Rectangle faceBounds) {		
	}

	@Override
	public void eyesFound(final BufferedImage image, final List<Point2D> facePoints) {
	}

	@Override
	public void noEyesFound(final BufferedImage image) {
	}
	
}
