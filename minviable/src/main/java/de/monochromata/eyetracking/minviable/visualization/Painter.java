package de.monochromata.eyetracking.minviable.visualization;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 * An interface for drawing onto
 */
public interface Painter {
	
	/**
	 * Draws the given positions onto the given image.
	 */
	void drawFace(final BufferedImage image, final List<Point2D> positions);
	
	/**
	 * Draws the given eye bounds onto the given image.
	 */
	void drawEyeBounds(final BufferedImage image, final Rectangle leftEyeBounds,
			final Rectangle rightEyeBounds);
	
	/**
	 * Draws the given rectangle onto the given graphics context that must
	 * originate from a {@link BufferedImage}. 
	 */
	void drawRect(final Graphics2D graphics, final Rectangle rectangle);
}
