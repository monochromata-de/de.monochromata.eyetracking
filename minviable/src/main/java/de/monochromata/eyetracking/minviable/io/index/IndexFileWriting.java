package de.monochromata.eyetracking.minviable.io.index;

import static de.monochromata.eyetracking.minviable.io.index.IOExceptionConversion.convertIOExceptionToUnchecked;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;

public interface IndexFileWriting extends IndexFileIO {
	
	static BufferedWriter createIndexWriterAndWriteHeaderLine(final File outputDirectory,
			final int numberOfInputImages) throws IOException {
		final BufferedWriter writer = createIndexWriter(outputDirectory, numberOfInputImages);
		writeHeaderLine(writer);
		return writer;
	}

	static BufferedWriter createIndexWriter(final File outputDirectory,
			final int numberOfInputImages) throws IOException {
		final File indexFile = createIndexFile(outputDirectory, numberOfInputImages);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(indexFile));
		return writer;
	}

	static void writeHeaderLine(final BufferedWriter writer) throws IOException {
		writer.write(Version3IndexFileReader.getHeader());
		writer.newLine();
	}
	
	static File createIndexFile(final File outputDirectory, final int numberOfInputImages)
			throws IOException {
		outputDirectory.mkdirs();
		final String indexFileName = numberOfInputImages+"setsOfEyePatchesAndManualContexts.csv";
		final File indexFile = new File(outputDirectory, indexFileName);
		indexFile.createNewFile();
		return indexFile;
	}

	static void addEyePatchesToIndexFile(final BufferedWriter indexFileWriter,
			final Stream<Object[]> imageFileAndEyePatchesStream) {
		imageFileAndEyePatchesStream.forEach(data ->  
			addEyePatchesToIndexFile(indexFileWriter, (File)data[0], (EyePatches)data[1]));
	}

	static void addEyePatchesToIndexFile(final BufferedWriter indexFileWriter,
			final File inputImageFile,
			final EyePatches eyePatches) {
		convertIOExceptionToUnchecked(() -> {
			indexFileWriter.write(createEyePatchesLine(inputImageFile, eyePatches));
			indexFileWriter.newLine();
		});
	}
	
	static String createEyePatchesLine(final File inputImageFile,
			final EyePatches eyePatches) {
		final Matcher matcher = createMatcherAndRejectNonMatches(inputImageFile);
		return CURRENT_SCHEMA_VERSION
				+" "+inputImageFile.getName()
				+" "+getTimestamp(matcher)
				+" "+getManualContextInfo(matcher)
				+" "+eyePatches.getIterations()
				+" "+format(eyePatches.getScore())
				+" "+format(eyePatches.getConvergence())
				+" "+getEyePatchInfo(inputImageFile, "left", eyePatches.getLeftPatch())
				+" "+getEyePatchInfo(inputImageFile, "right", eyePatches.getRightPatch());
	}

	static String format(double value) {
		return String.format("%1$.2f", value);
	}
	
	static String getTimestamp(final Matcher matcher) {
		return matcher.group("timestamp");
	}
	
	static String getManualContextInfo(final Matcher matcher) {
		return matcher.group("manualContextX")+" "+matcher.group("manualContextY");
	}

	static Matcher createMatcherAndRejectNonMatches(final File inputImageFile) {
		final Matcher matcher = INPUT_IMAGE_FILE_NAME_PATTERN.matcher(inputImageFile.getName());
		if(!matcher.matches()) {
			throw new IllegalArgumentException("File name "+inputImageFile.getName()
				+" does not match pattern "+INPUT_IMAGE_FILE_NAME_PATTERN.pattern());
		}
		return matcher;
	}
	
	static String getEyePatchInfo(final File inputImageFile, final String side, final EyePatch eyePatch) {
		return eyePatch.getX()+" "+eyePatch.getY()
			+" "+eyePatch.getWidth()+" "+eyePatch.getHeight()
			+" "+createDataString(eyePatch);
	}
	
	static String createDataString(final EyePatch eyePatch) {
		return "["+serializeArray(eyePatch.getData())+"]";
	}
	
	static String serializeArray(final double[] data) {
		return Arrays.stream(data)
				.mapToObj(Double::toString)
				.collect(Collectors.joining(","));
	}

	static EyePatches createEyePatches(final File inputImageFile,
			final Function<BufferedImage,EyePatches> eyePatchDetector) {
		return convertIOExceptionToUnchecked(() -> {
			return detectEyePatches(inputImageFile, eyePatchDetector);
		});
	}

	static EyePatches detectEyePatches(final File inputImageFile, 
			final Function<BufferedImage,EyePatches> eyePatchDetector) throws IOException {
		final BufferedImage image = ImageIO.read(inputImageFile);
		return eyePatchDetector.apply(image);
	}
	
}
