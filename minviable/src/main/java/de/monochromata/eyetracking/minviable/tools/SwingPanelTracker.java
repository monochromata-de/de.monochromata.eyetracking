package de.monochromata.eyetracking.minviable.tools;

import static de.monochromata.clmtrackr4j.ClmtrackrBackend.createBackend;
import static java.lang.Math.round;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.monochromata.clmtrackr4j.ClmtrackrBackend;
import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatchDetector;
import de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction;
import de.monochromata.eyetracking.minviable.features.BinocularFeatureExtractor;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;
import de.monochromata.eyetracking.webcam.svm.Gaze;
import de.monochromata.eyetracking.webcam.svm.GazeSvm;
import de.monochromata.eyetracking.webcam.svm.GazeSvm.Predictor;
import de.monochromata.eyetracking.webcam.svm.GazeTracker;

/**
 * Paints detected gaze locations onto an AWT component.
 */
public class SwingPanelTracker {

    private static final boolean HIGH_QUALITY = false;
    private static final Color RED = new Color(180, 0, 0);
    private static final Color GREEN = new Color(0, 180, 0);
    private static final int RADIUS_PX = 5;
    private static final int DIAMETER_PX = 2 * RADIUS_PX;

    private final GazeTracker tracker;
    private final JFrame jframe;

    private Gaze leftGaze;
    private Gaze rightGaze;

    public SwingPanelTracker(final GazeTracker tracker, final JFrame jframe) {
        this.tracker = tracker;
        this.jframe = jframe;
        this.jframe.setContentPane(new GazePanel());
        this.tracker.addListener(this::paintGaze);
    }

    private void paintGaze(final Gaze leftGaze, final Gaze rightGaze) {
        System.err.println("Paint gaze " + leftGaze.getXEstimate() + "," + leftGaze.getYEstimate() + " "
                + rightGaze.getXEstimate() + "," + rightGaze.getYEstimate());
        this.leftGaze = leftGaze;
        this.rightGaze = rightGaze;
        jframe.repaint();
    }

    /**
     * Used to prevent the tracker instance from being garbage-collected.
     */
    @SuppressWarnings("unused")
    private static SwingPanelTracker Instance;

    public static void main(final String[] args) throws Exception {
        if (args.length != 3) {
            System.err.println("Usage: <backend:graal> <leftPredictorDataFile> <rightPredictorDataFile>");
            return;
        }
        final SarxosWebcam webcam = new SarxosWebcam();
        final ClmtrackrBackend backend = createBackend(args[0]);
        final EyePatchDetector eyePatchDetector = new EyePatchDetector(
                new ClmtrackrPositionsDetector(backend, HIGH_QUALITY), positions -> EyePatchesExtraction
                        .applyPadding(BoundsFunctions.getEyeBoundsFromEyeCorners(positions), 2));
        final FeatureExtractor<BufferedImage> featureExtractor = BinocularFeatureExtractor.ofImages(eyePatchDetector,
                0);
        final GazeTracker tracker = new GazeTracker(webcam, featureExtractor);
        tracker.setPredictors(loadPredictor(args[1]), loadPredictor(args[2]));
        final JFrame panel = createWindow();
        Instance = new SwingPanelTracker(tracker, panel);
        tracker.start();
    }

    private static Predictor loadPredictor(final String filename) throws IOException, ClassNotFoundException {
        return loadPredictor(new File(filename));
    }

    private static Predictor loadPredictor(final File file) throws IOException, ClassNotFoundException {
        try (final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            final Serializable memento = (Serializable) ois.readObject();
            return GazeSvm.Predictor.fromMemento(memento);
        }
    }

    protected static JFrame createWindow() {
        final JFrame window = new JFrame();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        window.setBounds(10, 30, (int) screenSize.getWidth() - 20, (int) screenSize.getHeight() - 80);
        window.setDefaultCloseOperation(EXIT_ON_CLOSE);
        window.setVisible(true);
        return window;
    }

    private class GazePanel extends JPanel {

        private static final long serialVersionUID = 3068578713886965760L;

        @Override
        public void paint(final Graphics graphics) {
            paintGazePoint(graphics, leftGaze, RED);
            paintGazePoint(graphics, rightGaze, GREEN);
        }

        private void paintGazePoint(final Graphics graphics, final Gaze gaze, final Color color) {
            if (gaze != null) {
                paintNonNullGaze(graphics, gaze, color);
            }
        }

        private void paintNonNullGaze(final Graphics graphics, final Gaze gaze, final Color color) {
            final int x = (int) round(gaze.getXEstimate()) - RADIUS_PX;
            final int y = (int) round(gaze.getYEstimate()) - RADIUS_PX;
            graphics.setColor(color);
            graphics.fillOval(x, y, DIAMETER_PX, DIAMETER_PX);
        }

    }

}
