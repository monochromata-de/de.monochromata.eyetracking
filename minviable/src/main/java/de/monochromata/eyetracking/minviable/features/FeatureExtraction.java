package de.monochromata.eyetracking.minviable.features;

import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.TARGET_EYE_PATCH_HEIGHT;
import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.TARGET_EYE_PATCH_WIDTH;
import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;
import static java.lang.System.arraycopy;

import java.awt.image.BufferedImage;
import java.util.function.Consumer;

import de.monochromata.eyetracking.Eye;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.webcam.features.HistogramEqualization;

public interface FeatureExtraction {

	final int EYE_DATA_LENGTH = TARGET_EYE_PATCH_WIDTH * TARGET_EYE_PATCH_HEIGHT;
	final int POINT_LENGTH = 2;

	static int eyeDataLength(final int paddingPx) {
		return (2 * paddingPx + TARGET_EYE_PATCH_WIDTH) * (2 * paddingPx + TARGET_EYE_PATCH_HEIGHT);
	}

	static void addEyeData(final double[] features, final int offset, final Eye eye, final EyePatch eyePatch,
			final Consumer<Object[]> visualizer) {
		final double[] copyArray = new double[EYE_DATA_LENGTH];
		final BufferedImage image = createImageFrom(eye, eyePatch, visualizer);
		final BufferedImage imageWithEqualizedHistogram = equalizeHistogram(eye, image, visualizer);
		imageWithEqualizedHistogram.getRaster().getPixels(0, 0, TARGET_EYE_PATCH_WIDTH, TARGET_EYE_PATCH_HEIGHT,
				copyArray);
		arraycopy(copyArray, 0, features, offset, copyArray.length);
	}

	static BufferedImage createImageFrom(final Eye eye, final EyePatch eyePatch, final Consumer<Object[]> visualizer) {
		final int width = eyePatch.getWidth();
		final int height = eyePatch.getHeight();
		final BufferedImage bufferedImage = new BufferedImage(width, height, TYPE_BYTE_GRAY);
		bufferedImage.getRaster().setPixels(0, 0, width, height, eyePatch.getData());
		visualize(eye, "eye patch", bufferedImage, visualizer);
		return bufferedImage;
	}

	static BufferedImage equalizeHistogram(final Eye eye, final BufferedImage bufferedImage,
			final Consumer<Object[]> visualizer) {
		// TODO: Implement the HistogramEqualizationStepdefs, first
		final BufferedImage imageWithEqualizedHistogram = HistogramEqualization.equalizeHistogram(bufferedImage);
		visualize(eye, "eye patch (eq. hist.)", imageWithEqualizedHistogram, visualizer);
		return imageWithEqualizedHistogram;
	}

	static void visualize(final Eye eye, final String kindOfImage, final BufferedImage image,
			final Consumer<Object[]> visualizer) {
		if (visualizer != null) {
			final BufferedImage copy = copy(image);
			visualizer.accept(new Object[] { eye, kindOfImage, copy });
		}
	}

	static BufferedImage copy(final BufferedImage image) {
		final BufferedImage copy = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		copy.setData(image.getData());
		return copy;
	}

}
