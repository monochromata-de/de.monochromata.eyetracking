package de.monochromata.eyetracking.minviable.features;

import static de.monochromata.eyetracking.minviable.features.FeatureExtraction.addEyeData;
import static de.monochromata.eyetracking.minviable.features.FeatureExtraction.eyeDataLength;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.Eye;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction;
import de.monochromata.eyetracking.webcam.features.CompositeFeatureExtractor;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;

/**
 * Extracts two sets of features from a given set of eye patches - one set of
 * features for each eye.
 * <p>
 * This is roughly equivalent to how WebGazer creates features, but creates a
 * separate feature vector for each eye.
 *
 * @see #ofImages(Function, int)
 * @see #ofPositions(Function, int)
 */
public class BinocularFeatureExtractor implements FeatureExtractor<EyePatches> {

	private static final int EYE_DATA_OFFSET = 0;

	private final int paddingPx;
	private final Consumer<Object[]> visualizer;
	private final int eyeDataLength;

	protected BinocularFeatureExtractor(final int paddingPx) {
		this(paddingPx, null);
	}

	/**
	 * @param visualizer
	 *            will be passed all images during image processing, i.e. the
	 *            original image, the scaled image and the image after histogram
	 *            equalization. The {@link Object} array passed to the
	 *            visualizer contain the {@link Eye} the image is from, a
	 *            {@link String} naming the kind of image and a copy of the
	 *            image.
	 */
	protected BinocularFeatureExtractor(final int paddingPx, final Consumer<Object[]> visualizer) {
		this.paddingPx = paddingPx;
		this.visualizer = visualizer;
		// TODO: It is not plausible why paddingPx is used here
		eyeDataLength = eyeDataLength(paddingPx);
	}

	@Override
	public FeatureExtractor<EyePatches> withVisualizer(final Consumer<Object[]> visualizer) {
		return new BinocularFeatureExtractor(paddingPx, visualizer);
	}

	@Override
	public int getNumberOfFeatures() {
		return eyeDataLength;
	}

	/**
	 * Creates a feature vector for a set of eye patches.
	 *
	 * @param eyePatches
	 *            a set of eye patches
	 * @return the feature vector
	 */
	@Override
	public EyeFeatures apply(final EyePatches eyePatches) {
		final double[] leftEyeFeatures = createEyeFeatures(eyePatches, Eye.LEFT, EyePatches::getLeftPatch);
		final double[] rightEyeFeatures = createEyeFeatures(eyePatches, Eye.RIGHT, EyePatches::getRightPatch);
		return new EyeFeatures(leftEyeFeatures, rightEyeFeatures);
	}

	private double[] createEyeFeatures(final EyePatches eyePatches, final Eye eye,
			final Function<EyePatches, EyePatch> eyePatchAccess) {
		final double[] features = new double[getNumberOfFeatures()];
		addEyeData(features, EYE_DATA_OFFSET, eye, eyePatchAccess.apply(eyePatches), visualizer);
		return features;
	}

	/**
	 * Extracts the data for the left eye from a feature vector.
	 *
	 * @param extractedFeatures
	 *            as obtained from {@link #apply(EyePatches)}
	 * @return the data of the left eye patch
	 */
	public double[] getEyeData(final double[] extractedFeatures) {
		return getData(extractedFeatures, EYE_DATA_OFFSET, eyeDataLength);
	}

	private static double[] getData(final double[] extractedFeatures, final int offset, final int length) {
		final double[] data = new double[length];
		System.arraycopy(extractedFeatures, offset, data, 0, data.length);
		return data;
	}

	/**
	 * Create a new instance that consumes webcam images.
	 */
	public static FeatureExtractor<BufferedImage> ofImages(final Function<BufferedImage, EyePatches> eyePatchesDetector,
			final int paddingPx) {
		return new CompositeFeatureExtractor<>(eyePatchesDetector, new BinocularFeatureExtractor(paddingPx));
	}

	/**
	 * Create a new instance that consumes positions that are part of
	 * {@link ClmtrackrResult} instances.
	 *
	 * @param boundsFunction
	 *            used to determine the position of the eye patches
	 */
	public static FeatureExtractor<ClmtrackrResult> ofPositions(
			final Function<List<Point2D>, Rectangle[]> boundsFunction, final int paddingPx) {
		return ofPositions(EyePatchesExtraction::createEyePatches, boundsFunction, paddingPx);
	}

	/**
	 * Provided for testing. Use {@link #ofPositions(Function, int)} for
	 * production code.
	 *
	 * @see #ofPositions(Function, int)
	 */
	public static FeatureExtractor<ClmtrackrResult> ofPositions(
			final BiFunction<ClmtrackrResult, Function<List<Point2D>, Rectangle[]>, EyePatches> eyePatchesExtractor,
			final Function<List<Point2D>, Rectangle[]> boundsFunction, final int paddingPx) {
		return new CompositeFeatureExtractor<>(result -> eyePatchesExtractor.apply(result, boundsFunction),
				new BinocularFeatureExtractor(paddingPx));
	}

	public static FeatureExtractor<EyePatches> ofEyePatches(final int paddingPx) {
		return new BinocularFeatureExtractor(paddingPx);
	}

}
