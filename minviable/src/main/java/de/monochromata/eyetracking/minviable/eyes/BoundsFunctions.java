package de.monochromata.eyetracking.minviable.eyes;

import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.TARGET_EYE_PATCH_HEIGHT;
import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.TARGET_EYE_PATCH_WIDTH;
import static java.lang.Math.round;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;

public interface BoundsFunctions {

	static Rectangle[] getEyeBoundsFromPupilPosition(final List<Point2D> positions) {
		return new Rectangle[] {
				BoundsFunctions.getEyeBoundsFromPupilPosition(positions.get(27)),
				BoundsFunctions.getEyeBoundsFromPupilPosition(positions.get(32))
		};
	}

	static Rectangle getEyeBoundsFromPupilPosition(final Point2D pupilPosition) {
		final double centerX = pupilPosition.getX();
		final double centerY = pupilPosition.getY();
		return BoundsFunctions.getEyeBoundsAroundCenter(centerX, centerY);
	}

	static Rectangle[] getEyeBoundsFromEyeCorners(final List<Point2D> positions) {
		return new Rectangle[] {
			BoundsFunctions.getEyeBoundsFromEyeCorners(positions.get(23), positions.get(25)),
			BoundsFunctions.getEyeBoundsFromEyeCorners(positions.get(30), positions.get(28))
		};
	}

	static Rectangle getEyeBoundsFromEyeCorners(final Point2D leftCorner, final Point2D rightCorner) {
		final int centerX = BoundsFunctions.mean(leftCorner.getX(), rightCorner.getX());
		final int centerY = BoundsFunctions.mean(leftCorner.getY(), rightCorner.getY());
		return BoundsFunctions.getEyeBoundsAroundCenter(centerX, centerY);
	}

	static Rectangle getEyeBoundsAroundCenter(final double centerX, final double centerY) {
		final int x = (int)round(centerX-(TARGET_EYE_PATCH_WIDTH/2));
		final int y = (int)round(centerY-(TARGET_EYE_PATCH_HEIGHT/2)); 
		return new Rectangle(x, y, TARGET_EYE_PATCH_WIDTH, TARGET_EYE_PATCH_HEIGHT);
	}

	static int mean(final double value1, final double value2) {
		return (int)Math.round((value1+value2)/2);
	}

}
