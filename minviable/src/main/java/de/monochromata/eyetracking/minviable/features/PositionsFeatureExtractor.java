package de.monochromata.eyetracking.minviable.features;

import java.util.function.Consumer;
import java.util.stream.DoubleStream;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;

/**
 * Provides all points that Clmtrackr detected as part of the face for both
 * eyes.
 * <p>
 * This feature extractor shall be combined with
 * {@link BinocularFeatureExtractor} or {@link WebgazerFeatureExtractor}.
 *
 * @see BinocularFeatureExtractor
 * @see WebgazerFeatureExtractor
 */
public class PositionsFeatureExtractor implements FeatureExtractor<ClmtrackrResult> {

	@Override
	public EyeFeatures apply(final ClmtrackrResult result) {
		final double[] faceFeatures = createEyeFeatures(result);
		return new EyeFeatures(faceFeatures, faceFeatures);
	}

	protected double[] createEyeFeatures(final ClmtrackrResult result) {
		return result.getPositions().stream().flatMapToDouble(point -> DoubleStream.of(point.getX(), point.getY()))
				.toArray();
	}

	@Override
	public int getNumberOfFeatures() {
		return ClmtrackrResult.NUMBER_OF_POSITIONS * 2;
	}

	@Override
	public FeatureExtractor<ClmtrackrResult> withVisualizer(final Consumer<Object[]> visualizer) {
		// TODO: How could these features be visualized?
		throw new UnsupportedOperationException();
	}

}
