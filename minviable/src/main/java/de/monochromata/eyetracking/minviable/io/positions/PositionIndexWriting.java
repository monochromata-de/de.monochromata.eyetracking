package de.monochromata.eyetracking.minviable.io.positions;

import static de.monochromata.eyetracking.minviable.io.index.IndexFileIO.INPUT_IMAGE_FILE_NAME_PATTERN;
import static java.util.stream.Collectors.joining;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

import javax.imageio.ImageIO;

import org.slf4j.Logger;

import de.monochromata.clmtrackr4j.ClmtrackrResult;

public interface PositionIndexWriting extends PositionIndexIO {

	static BufferedWriter createIndexWriterAndWriteHeaderLine(final File outputDirectory,
			final int numberOfInputImages) throws IOException {
		final BufferedWriter writer = createIndexWriter(outputDirectory, numberOfInputImages);
		PositionIndexWriting.writeHeaderLine(writer);
		return writer;
	}

	static BufferedWriter createIndexWriter(final File outputDirectory,
			final int numberOfInputImages) throws IOException {
		final File indexFile = createIndexFile(outputDirectory, numberOfInputImages);
		final BufferedWriter writer = new BufferedWriter(new FileWriter(indexFile));
		return writer;
	}

	static void writeHeaderLine(final BufferedWriter writer) throws IOException {
		writer.write(HEADER_FOR_SCHEMA_VERSION_2);
		writer.newLine();
	}

	static File createIndexFile(final File outputDirectory, final int numberOfInputImages)
			throws IOException {
		outputDirectory.mkdirs();
		final String indexFileName = numberOfInputImages+"positions.csv";
		final File indexFile = new File(outputDirectory, indexFileName);
		indexFile.createNewFile();
		return indexFile;
	}

	static void addResultToIndexFile(final BufferedWriter indexFileWriter, final List<File> inputImageFiles,
			final Function<BufferedImage,ClmtrackrResult> positionsDetector, final Logger logger) {
		inputImageFiles.forEach(inputImageFile -> 
			addResultToIndexFile(indexFileWriter, inputImageFile, positionsDetector, logger));
	}

	static void addResultToIndexFile(final BufferedWriter indexFileWriter, final File inputImageFile,
			final Function<BufferedImage,ClmtrackrResult> eyePatchDetector, final Logger logger) {
		try {
			if(imageNameHasManualContext(inputImageFile)) {
				// Only add positions for images that come with a manual context truth
				indexFileWriter.write(createResultLine(inputImageFile, eyePatchDetector));
				indexFileWriter.newLine();
			} else {
				// Feed the new image into the face tracker so it can adjust the face model 
				getDetectionResult(inputImageFile, eyePatchDetector);
			}
		} catch (final Exception ioe) {
			logger.warn("Failed to create result line from image file "+inputImageFile.getName(), ioe);
		}
	}

	static boolean imageNameHasManualContext(final File inputImageFile) {
		return INPUT_IMAGE_FILE_NAME_PATTERN.matcher(inputImageFile.getName()).matches();
	}

	static String createResultLine(final File inputImageFile,
			final Function<BufferedImage,ClmtrackrResult> eyePatchDetector) throws IOException {
		final ClmtrackrResult result = getDetectionResult(inputImageFile, eyePatchDetector);
		return PositionIndexIO.CURRENT_SCHEMA_VERSION
				+" "+inputImageFile.getName()
				+" "+result.getIterations()
				+" "+result.getScore()
				+" "+result.getConvergence()
				+" "+result.getScalingFactor()
				+" "+result.getRotationRadians()
				+" "+result.getTranslateXPx()
				+" "+result.getTransplateYPx()
				+" "+listPositions(result.getPositions());
	}

	static String listPositions(final List<Point2D> positions) {
		return positions.stream()
				.map(position -> position.getX()+","+position.getY())
				.collect(joining(";"));
	}

	static ClmtrackrResult getDetectionResult(final File inputImageFile, 
			final Function<BufferedImage,ClmtrackrResult> positionsDetector) throws IOException {
		final BufferedImage image = ImageIO.read(inputImageFile);
		if(image == null) {
			throw new IOException("Failed to read image from "+inputImageFile.getAbsolutePath());
		}
		return positionsDetector.apply(image);
	}

}
