package de.monochromata.eyetracking.minviable.tools;

import static de.monochromata.clmtrackr4j.ClmtrackrBackend.createBackend;

import java.io.File;

import de.monochromata.clmtrackr4j.ClmtrackrBackend;
import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;

public class HighQualityPositionsDetector extends PositionsDetector {
	
	public HighQualityPositionsDetector(final ClmtrackrBackend backend,
			final File inputDirectory, final File outputDirectory,
			final Visualizer visualizer) {
		super(backend, inputDirectory, outputDirectory, visualizer);
	}

	public HighQualityPositionsDetector(final ClmtrackrBackend backend,
			final File inputDirectory, final File outputDirectory) {
		super(backend, inputDirectory, outputDirectory);
	}

	public HighQualityPositionsDetector(final ClmtrackrBackend backend,
			final String inputPath, final String outputPath,
			final Visualizer visualizer) {
		super(backend, inputPath, outputPath, visualizer);
	}

	public HighQualityPositionsDetector(final ClmtrackrBackend backend,
			final String inputPath, final String outputPath) {
		super(backend, inputPath, outputPath);
	}

	@Override
	protected ClmtrackrPositionsDetector createPositionsDetector(ClmtrackrBackend backend, Visualizer visualizer) {
		return new ClmtrackrPositionsDetector(backend, visualizer, true);
	}
	
	public static void main(final String[] args) throws Exception {
		requireArgumentsOrShowUsage(args);
		final ClmtrackrBackend backend = createBackend(args[0]);
		final String imageDirectory = args[1];
		final String positionsFileDirectory = args[2];
		final Visualizer visualizer = createVisualizer(args[3]);
		new HighQualityPositionsDetector(backend, imageDirectory, positionsFileDirectory, visualizer).runBatch();
	}
	
}
