package de.monochromata.eyetracking.minviable.tools;

import static de.monochromata.clmtrackr4j.ClmtrackrBackend.createBackend;
import static de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector.NO_DETECTION_DURATION_AVAILABLE;

import java.awt.image.BufferedImage;

import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.eyetracking.minviable.visualization.FaceVisualizer;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;
import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;

public class FaceTracker {

    public static void main(final String[] args) {
        requireArguments(args);
        final SarxosWebcam webcam = new SarxosWebcam(1);
        final Visualizer visualizer = /* new NopVisualizer(); */new FaceVisualizer();// new ImageProcessingVisualizer();
        final ClmtrackrPositionsDetector detector = new ClmtrackrPositionsDetector(createBackend(args[0]), visualizer);
        BufferedImage image;
        long count = 0;
        while ((image = webcam.getImage()) != null) {
            count = printStatistics(webcam, detector, count);
            detector.apply(image);
        }
    }

    protected static long printStatistics(final SarxosWebcam webcam, final ClmtrackrPositionsDetector detector,
            long count) {
        if (count++ % 30 == 0) {
            printFramesPerSecond(webcam);
            printDetectionDuration(detector);
        }
        return count;
    }

    protected static void printFramesPerSecond(final SarxosWebcam webcam) {
        System.err.println("FPS: " + webcam.getFramesPerSecond());
    }

    protected static void printDetectionDuration(final ClmtrackrPositionsDetector detector) {
        final long lastDetectionDuration = detector.getLastDetectionDurationMs();
        if (lastDetectionDuration != NO_DETECTION_DURATION_AVAILABLE) {
            System.err.println("last detection took " + lastDetectionDuration + " ms");
        }
    }

    private static void requireArguments(final String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: <backend:graal>");
            System.exit(1);
        }
    }

}
