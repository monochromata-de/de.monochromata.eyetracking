package de.monochromata.eyetracking.minviable.visualization;

import static de.monochromata.eyetracking.webcam.image.ImageManipulation.scale;
import static java.awt.Color.GREEN;
import static java.awt.FlowLayout.LEADING;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR;
import static javax.swing.BoxLayout.Y_AXIS;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.monochromata.eyetracking.Eye;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.image.ImageManipulation;

public class DatumVisualization extends JPanel {

	private static final int SCALING_FACTOR = 5;

	private static final long serialVersionUID = -2462957993950342329L;
	private final File imagesDirectory;
	private final FeatureExtractor<BufferedImage> featureExtractor;
	private JPanel featuresPanel;

	/**
	 * @param featureExtractor
	 *            a copy of the extractor is used with an internal visualizer
	 */
	public DatumVisualization(final File imagesDirectory, final FeatureExtractor<BufferedImage> featureExtractor) {
		this.imagesDirectory = imagesDirectory;
		this.featureExtractor = featureExtractor.withVisualizer(this::addEyeImage);
		setLayout(new BoxLayout(this, Y_AXIS));
	}

	/**
	 * Show the given datum
	 *
	 * @param imageFileName
	 *            the simple name of the webcam image, relative to
	 *            {@link #imagesDirectory}
	 * @throws UncheckedIOException
	 *             When failing to read the webcam image
	 */
	public void setDatumToShow(final String imageFileName) {
		removeAll();
		final BufferedImage image = loadImage(imageFileName);
		addWebcamShot(imageFileName, image);
		extractFeaturesAndTherebyVisualizeEyePatch(image);
	}

	private void extractFeaturesAndTherebyVisualizeEyePatch(final BufferedImage image) {
		featuresPanel = new JPanel();
		featuresPanel.setLayout(new FlowLayout(LEADING, 0, 0));
		add(featuresPanel);
		featureExtractor.apply(image);
	}

	protected void addWebcamShot(final String imageFileName, final BufferedImage image) {
		addImage(imageFileName, image);
		// addGrayscaleImage(imageFileName, image, eyePatches);
	}

	protected BufferedImage loadImage(final String imageFileName) {
		try {
			final File imageFile = new File(imagesDirectory, imageFileName);
			return ImageIO.read(imageFile);
		} catch (final IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
	}

	private void addImage(final String imageFileName, final BufferedImage image) {
		add(new JLabel(imageFileName + " (" + image.getWidth() + "x" + image.getHeight() + "px)"));
		add(new JLabel(new ImageIcon(image)));
	}

	private void addGrayscaleImage(final String imageFileName, final BufferedImage image, final EyePatches eyePatches) {
		final BufferedImage grayscaleImage = ImageManipulation.ensureGrayscale(image);
		addEyePatches(grayscaleImage, eyePatches);
		final JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, Y_AXIS));
		panel.add(new JLabel(imageFileName + " gray (" + image.getWidth() + "x" + image.getHeight() + "px)"));
		panel.add(new JLabel(new ImageIcon(grayscaleImage)));
		add(panel);
	}

	private void addEyePatches(final Image grayscaleImage, final EyePatches eyePatches) {
		final Graphics graphics = grayscaleImage.getGraphics();
		drawRectAroundEyePatch(graphics, eyePatches.getLeftPatch());
		drawRectAroundEyePatch(graphics, eyePatches.getRightPatch());
	}

	private void drawRectAroundEyePatch(final Graphics graphics, final EyePatch eyePatch) {
		// TODO: Make this drawing testable
		// TODO: Re-use color-setting from other parts of the code
		final Color oldColor = graphics.getColor();
		graphics.setColor(GREEN);
		graphics.drawRect(eyePatch.getX() - 1, eyePatch.getY(), eyePatch.getWidth() + 2, eyePatch.getHeight() + 2);
		graphics.setColor(oldColor);
	}

	protected void addEyeImage(final Object[] data) {
		addEyeImage((Eye) data[0], (String) data[1], (BufferedImage) data[2]);
	}

	protected void addEyeImage(final Eye eye, final String kindOfImage, final BufferedImage image) {
		final int width = SCALING_FACTOR * image.getWidth();
		final int height = SCALING_FACTOR * image.getHeight();
		final BufferedImage scaled = scale(image, width, height, VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		final JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, Y_AXIS));
		panel.add(new JLabel(
				ucfirst(eye.name())/*
									 * +" "+kindOfImage+" ("
									 * +image.getWidth()+"x"+image.getHeight()+
									 * "px)*"+SCALING_FACTOR
									 */));
		panel.add(new JLabel(new ImageIcon(scaled)));
		featuresPanel.add(panel);
	}

	protected String ucfirst(final String input) {
		return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
	}
}
