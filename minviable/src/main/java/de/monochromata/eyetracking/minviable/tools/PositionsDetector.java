package de.monochromata.eyetracking.minviable.tools;

import static de.monochromata.clmtrackr4j.ClmtrackrBackend.createBackend;
import static de.monochromata.eyetracking.minviable.io.FileChecking.requireExistingDirectory;
import static de.monochromata.eyetracking.minviable.io.index.IOExceptionConversion.convertIOExceptionToUnchecked;
import static de.monochromata.eyetracking.minviable.io.positions.PositionIndexWriting.addResultToIndexFile;
import static de.monochromata.eyetracking.minviable.io.positions.PositionIndexWriting.createIndexWriterAndWriteHeaderLine;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.monochromata.clmtrackr4j.ClmtrackrBackend;
import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.eyetracking.minviable.io.index.IndexFileIO;
import de.monochromata.eyetracking.minviable.visualization.FaceVisualizer;
import de.monochromata.eyetracking.minviable.visualization.ImageProcessingVisualizer;
import de.monochromata.eyetracking.minviable.visualization.NopVisualizer;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;

/**
 * Detects positions for all image files with manual contexts in a given input
 * directory and saves the positions file in an output directory.
 */
public class PositionsDetector {

    private static final Logger LOGGER = LoggerFactory.getLogger(HighQualityPositionsDetector.class);

    private final File inputDirectory;
    private final File outputDirectory;
    private final ClmtrackrPositionsDetector positionsDetector;

    public PositionsDetector(final ClmtrackrBackend backend, final String inputPath, final String outputPath) {
        this(backend, new File(inputPath), new File(outputPath), new NopVisualizer());
    }

    public PositionsDetector(final ClmtrackrBackend backend, final String inputPath, final String outputPath,
            final Visualizer visualizer) {
        this(backend, new File(inputPath), new File(outputPath), visualizer);
    }

    public PositionsDetector(final ClmtrackrBackend backend, final File inputDirectory, final File outputDirectory) {
        this(backend, inputDirectory, outputDirectory, new NopVisualizer());
    }

    public PositionsDetector(final ClmtrackrBackend backend, final File inputDirectory, final File outputDirectory,
            final Visualizer visualizer) {
        requireExistingDirectory(inputDirectory);
        requireExistingDirectory(outputDirectory);
        this.inputDirectory = inputDirectory;
        this.outputDirectory = outputDirectory;
        positionsDetector = createPositionsDetector(backend, visualizer);
    }

    protected ClmtrackrPositionsDetector createPositionsDetector(final ClmtrackrBackend backend,
            final Visualizer visualizer) {
        return new ClmtrackrPositionsDetector(backend, visualizer, false);
    }

    public void runBatch() {
        convertIOExceptionToUnchecked(this::runBatchWithCheckedException);
    }

    private void runBatchWithCheckedException() throws IOException {
        final List<File> inputImageFiles = IndexFileIO.getImageFiles(inputDirectory);
        try (final BufferedWriter indexFileWriter = createIndexWriterAndWriteHeaderLine(outputDirectory,
                inputImageFiles.size())) {
            addResultToIndexFile(indexFileWriter, inputImageFiles, positionsDetector, LOGGER);
        }
    }

    public static void main(final String[] args) throws Exception {
        requireArgumentsOrShowUsage(args);
        final ClmtrackrBackend backend = createBackend(args[0]);
        final String imageDirectory = args[1];
        final String positionsFileDirectory = args[2];
        final Visualizer visualizer = createVisualizer(args[3]);
        new PositionsDetector(backend, imageDirectory, positionsFileDirectory, visualizer).runBatch();
    }

    protected static void requireArgumentsOrShowUsage(final String[] args) {
        if (args.length != 4) {
            final String lineSeparator = System.getProperty("line.separator");
            System.err.println("Usage: <backend:graal> <imageDirectory> <outputDirectory> <visualizerClass>"
                    + lineSeparator + "   visualizerClass may be one of:" + lineSeparator + "   "
                    + NopVisualizer.class.getName() + lineSeparator + "   " + FaceVisualizer.class.getName()
                    + lineSeparator + "   " + ImageProcessingVisualizer.class.getName() + lineSeparator);
            System.exit(1);
        }
    }

    protected static Visualizer createVisualizer(final String className)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return (Visualizer) Class.forName(className).newInstance();
    }

}
