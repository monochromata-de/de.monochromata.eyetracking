package de.monochromata.eyetracking.minviable.visualization;

import static java.util.Objects.requireNonNull;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import de.monochromata.clmtrackr4j.js.Canvas;
import de.monochromata.clmtrackr4j.js.Context2D;

public class ImageProcessingVisualizer extends AbstractSwingVisualizer {
	
	private static final LineBorder RED_BORDER = new LineBorder(Color.RED);
	private final List<Runnable> drawOperations = new LinkedList<>();
	
	public ImageProcessingVisualizer() {
	}

	public ImageProcessingVisualizer(final JFrame frame) {
		super(frame);
	}
	
	public ImageProcessingVisualizer(final JFrame frame, final Painter painter) {
		super(frame, painter);
	}

	@Override
	public void initialize(final BufferedImage initialImage) {
		super.initialize(initialImage);
		initializeListenerForNewCanvases();
		initializeDrawListener();
	}
	
	private void initializeListenerForNewCanvases() {
		Canvas.addCreationListener(canvas -> {
			final ImageIcon canvasIcon = new ImageIcon();
			final BufferedImage copyOfImage = copy(canvas.getBufferedImage());
			final Context2D context2D = (Context2D)canvas.getContext("2d");
			requireNonNull(context2D);
			canvasIcon.setImage(copyOfImage);
			final JLabel canvasLabel = new JLabel(canvasIcon);
			canvasLabel.setBorder(RED_BORDER);
			getFrame().add(canvasLabel);
			getFrame().pack();
		});
	}

	private void initializeDrawListener() {
		Context2D.addReadListener((context2D,rectangle) -> {
			drawOperations.add(() -> {
				final Graphics2D graphicsForContext = imageCopies.get(context2D.getBufferedImage()).createGraphics();
				getPainter().drawRect(graphicsForContext, rectangle); });
		});
	}
	
	@Override
	protected GridLayout getLayout() {
		return new GridLayout(3, 3);
	}

	@Override
	protected void drawOnImage(final BufferedImage image, final List<Point2D> facePoints) {
		super.drawOnImage(image, facePoints);
		runDrawOperations();
	}	
	
	private void runDrawOperations() {
		drawOperations.forEach(Runnable::run);
		drawOperations.clear();
	}
	
}
