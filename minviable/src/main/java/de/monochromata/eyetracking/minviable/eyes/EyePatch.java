package de.monochromata.eyetracking.minviable.eyes;

import java.awt.image.BufferedImage;
import java.util.Arrays;

/**
 * Represents an eye image as a {@code 6 x 10} feature vector (6 rows, 10 columns
 * for the same number of pixels).
 * <p>
 * The data in the eye patch has been obtained from a grayscaled {@link BufferedImage} via
 * <pre>{@code bufferedImage.getRaster().getPixels(x, y, width, height, (double[])null);}</pre>
 * and can be set in a new {@link BufferedImage} using
 * <pre>{@code newBufferedImage.setPixels(x, y, width, height, data);}</pre>
 */
public class EyePatch {

	private final int x, y, width, height;
	private final double[] data;
	
	public EyePatch(int x, int y, int width, int height, double[] data) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.data = data;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public double[] getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + height;
		result = prime * result + width;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EyePatch other = (EyePatch) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		if (height != other.height)
			return false;
		if (width != other.width)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EyePatch [x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + ", data="
				+ Arrays.toString(data) + "]";
	}
	
}
