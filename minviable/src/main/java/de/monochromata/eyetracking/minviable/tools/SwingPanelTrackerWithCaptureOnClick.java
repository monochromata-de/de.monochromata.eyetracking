package de.monochromata.eyetracking.minviable.tools;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.JFrame;

import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.clmtrackr4j.js.graal.GraalBackend;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatchDetector;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction;
import de.monochromata.eyetracking.minviable.features.BinocularFeatureExtractor;
import de.monochromata.eyetracking.minviable.visualization.FaceVisualizer;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;
import de.monochromata.eyetracking.webcam.calibration.Calibration;
import de.monochromata.eyetracking.webcam.calibration.ContinuousCalibration;
import de.monochromata.eyetracking.webcam.calibration.EyeValidationResult;
import de.monochromata.eyetracking.webcam.calibration.ValidationResult;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.sarxos.SarxosWebcam;
import de.monochromata.eyetracking.webcam.svm.GazeTracker;
import de.monochromata.eyetracking.webcam.tools.CaptureOnClick;
import libsvm.svm_parameter;

public class SwingPanelTrackerWithCaptureOnClick {

    /**
     * Used to store the tracker instance to keep it from being garbage-collected.
     */
    @SuppressWarnings("unused")
    private static SwingPanelTracker swingPanelTracker;

    public static void main(final String[] args) {
        final ForkJoinPool executor = new ForkJoinPool(1);
        final SarxosWebcam webcam = new SarxosWebcam(1);

        final svm_parameter hyperParameters = createHyperParameters();
        final GraalBackend backend = new GraalBackend();
        final Visualizer visualizer = new FaceVisualizer();
        /*
         * final Function<List<Point2D>, Rectangle[]> boundsFunction =
         * ClmtrackerEyePatchesExtraction::getEyeBoundsFromEyeCorners;
         */
        final Function<List<Point2D>, Rectangle[]> boundsFunction = positions -> EyePatchesExtraction
                .applyPadding(BoundsFunctions.getEyeBoundsFromEyeCorners(positions), 2);
        final Function<BufferedImage, EyePatches> eyePatchDetector = new EyePatchDetector(
                new ClmtrackrPositionsDetector(backend, visualizer), boundsFunction);
        final FeatureExtractor<BufferedImage> featureExtractor = BinocularFeatureExtractor.ofImages(eyePatchDetector,
                0);
        final Calibration calibration = new Calibration(featureExtractor, hyperParameters);
        final int trainingThreshold = 15;
        final int validationThreshold = 5;
        final ContinuousCalibration<BufferedImage> continuousCalibration = new ContinuousCalibration<>(executor,
                calibration, featureExtractor, trainingThreshold, validationThreshold);

        final JFrame jframe = SwingPanelTracker.createWindow();
        final AtomicReference<Consumer<Point>> manualContextConnection = new AtomicReference<>();
        final CaptureOnClick captureOnClick = new CaptureOnClick(manualContextConnection::set, webcam);
        captureOnClick.addListenerForImagesWithManualContext(
                (image, point) -> continuousCalibration.addDatum(image, point.getX(), point.getY()));
        CaptureOnClick.addImageCapturingMouseListener(jframe, webcam, manualContextConnection);

        final GazeTracker tracker = new GazeTracker(webcam, featureExtractor);
        continuousCalibration.addValidationListener(tracker::setPredictors);
        continuousCalibration.addValidationListener(SwingPanelTrackerWithCaptureOnClick::logRevalidation);

        swingPanelTracker = new SwingPanelTracker(tracker, jframe);
        tracker.start();
    }

    protected static void logRevalidation(final ValidationResult validationResult) {
        logRevalidation("left", validationResult.getLeftEyeResult());
        logRevalidation("right", validationResult.getRightEyeResult());
        System.err.println();
    }

    private static void logRevalidation(final String side, final EyeValidationResult result) {
        System.err.print(side + " eye error x=" + result.getErrorX() + " y=" + result.getErrorY() + " ");
    }

    protected static svm_parameter createHyperParameters() {
        return createSamplePolynomialHyperParameters();
    }

    protected static svm_parameter createSamplePolynomialHyperParameters() {
        final svm_parameter hyperParameters = new svm_parameter();
        hyperParameters.svm_type = svm_parameter.NU_SVR;
        hyperParameters.kernel_type = svm_parameter.POLY;
        // for polynomial
        hyperParameters.degree = 3;
        hyperParameters.gamma = 0.01;
        hyperParameters.coef0 = 1;
        // for Nu
        hyperParameters.C = 1;
        hyperParameters.nu = 1;
        // technical
        hyperParameters.cache_size = 100;
        hyperParameters.eps = 0.001;
        hyperParameters.shrinking = 1;
        hyperParameters.probability = 0;
        return hyperParameters;
    }

}
