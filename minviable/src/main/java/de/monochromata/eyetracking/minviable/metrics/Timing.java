package de.monochromata.eyetracking.minviable.metrics;

import java.util.function.Supplier;

public interface Timing {

	boolean SHOW_MILLISECONDS = true;
	
	static void logDuration(final String taskDescription, final Runnable runnable) {
		logDuration(taskDescription, () -> { runnable.run(); return null; });
	}
	
	static <T> T logDuration(final String taskDescription, final Supplier<T> supplier) {
		final long startMs = SHOW_MILLISECONDS?System.currentTimeMillis():System.nanoTime();
		final T result = supplier.get();
		final long endMs = SHOW_MILLISECONDS?System.currentTimeMillis():System.nanoTime();
		System.err.println(taskDescription+" took "+(endMs-startMs)+(SHOW_MILLISECONDS?"ms":"ns"));
		return result;
	}
	
}
