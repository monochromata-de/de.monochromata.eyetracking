/**
 * I/O for positions obtained from clmtrackr4j.
 */
package de.monochromata.eyetracking.minviable.io.positions;