package de.monochromata.eyetracking.minviable.io.index;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

public interface IndexFileIO {

	Pattern INPUT_IMAGE_FILE_NAME_PATTERN = 
			Pattern.compile("(?<prefix>[^_]+)_(?<timestamp>\\d+)_(?<manualContextX>\\d+)_(?<manualContextY>\\d+).png");
	
	int CURRENT_SCHEMA_VERSION = 3;

	static List<File> getImageFiles(final File inputImageDirectory) {
		return stream(inputImageDirectory.listFiles(file -> file.getName().endsWith(".png")))
				.sorted((file1, file2) -> file1.getName().compareTo(file2.getName()))
				.collect(toList());
	}

}
