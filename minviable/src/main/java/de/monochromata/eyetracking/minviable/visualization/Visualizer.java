package de.monochromata.eyetracking.minviable.visualization;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

public interface Visualizer {
	
	void initialize(final BufferedImage initialImage);
	
	void faceFound(final BufferedImage image, final Rectangle faceBounds);
	
	void eyesFound(final BufferedImage image, final List<Point2D> facePoints);
	
	void noEyesFound(final BufferedImage image);

}
