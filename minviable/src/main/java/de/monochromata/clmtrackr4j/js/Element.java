package de.monochromata.clmtrackr4j.js;

public class Element {

	public final String tagName;

	public Element(final String tagName) {
		this.tagName = tagName.toUpperCase();
	}

	public String getTagName() {
		return tagName;
	}

}