package de.monochromata.clmtrackr4j.js;

import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.lang.Integer.parseInt;

import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Canvas extends Element {

	private static final List<Consumer<Canvas>> LISTENERS = new ArrayList<>();
	private static final boolean GRAYSCALE = true;

	public Object x, y, width, height;
	BufferedImage bufferedImage;
	private final Context2D context2D;

	public Canvas(final int width, final int height) {
		this(new BufferedImage(width, height, TYPE_INT_ARGB));
		LISTENERS.forEach(listener -> listener.accept(Canvas.this));
	}

	public Canvas(final BufferedImage bufferedImage) {
		super("canvas");
		this.bufferedImage = ensureImageType(bufferedImage);
		context2D = new Context2D(this.bufferedImage);
		width = this.bufferedImage.getWidth();
		height = this.bufferedImage.getHeight();
	}

	public static void addCreationListener(final Consumer<Canvas> listener) {
		LISTENERS.add(listener);
	}

	public static void removeCreationListener(final Consumer<Canvas> listener) {
		LISTENERS.remove(listener);
	}

	protected static BufferedImage ensureImageType(final BufferedImage bufferedImage) {
		final BufferedImage grayscaledImage = GRAYSCALE ? convert(bufferedImage, TYPE_BYTE_GRAY) : bufferedImage;
		return convert(grayscaledImage, TYPE_INT_ARGB);
	}

	private static BufferedImage convert(final BufferedImage input, final int targetType) {
		final BufferedImage converted = new BufferedImage(input.getWidth(), input.getHeight(), targetType);
		new ColorConvertOp(null).filter(input, converted);
		return converted;
	}

	public BufferedImage getBufferedImage() {
		return bufferedImage;
	}

	public Context2D getContext(final String contextType) {
		switch (contextType) {
		case "2d":
			return context2D;
		default:
			throw new IllegalArgumentException("Unknown context type: " + contextType);
		}
	}

	public void setAttribute(final String name, final int value) {
		setAttribute(name, "" + value);
	}

	public void setAttribute(final String name, final float value) {
		setAttribute(name, "" + value);
	}

	public void setAttribute(final String name, final String value) {
		switch (name) {
		case "x":
			x = value;
			break;
		case "y":
			y = value;
			break;
		case "width":
			width = value;
			bufferedImage = new BufferedImage(parseInt(value), bufferedImage.getHeight(), TYPE_INT_ARGB);
			break;
		case "height":
			height = value;
			bufferedImage = new BufferedImage(bufferedImage.getWidth(), parseInt(value), TYPE_INT_ARGB);
			break;
		default:
			throw new IllegalArgumentException("Unknown attribute: " + name);
		}
	}

}