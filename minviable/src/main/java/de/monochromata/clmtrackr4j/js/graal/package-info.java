/**
 * A Clmtrackr backend using the Graal.JS JavaScript engine.
 *
 * @see <a href="https://github.com/graalvm/graaljs">Graal.JS</a>
 * @see <a href="https://github.com/oracle/graal/blob/master/sdk">Graal SDK</a>
 */
package de.monochromata.clmtrackr4j.js.graal;