package de.monochromata.clmtrackr4j.js;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Context2D {

    private static final List<Consumer<Context2D>> CHANGE_LISTENERS = new ArrayList<>();
    private static final List<BiConsumer<Context2D, Rectangle>> READ_LISTENERS = new ArrayList<>();

    private final BufferedImage bufferedImage;
    private final Stack<Graphics2D> states = new Stack<>();
    private Graphics2D currentGraphics;
    private int[] buffer;

    public Context2D(final BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
        currentGraphics = bufferedImage.createGraphics();
    }

    public static void addChangeListener(final Consumer<Context2D> listener) {
        CHANGE_LISTENERS.add(listener);
    }

    public static void removeChangeListener(final Consumer<Context2D> listener) {
        CHANGE_LISTENERS.remove(listener);
    }

    protected void notifyChangeListeners() {
        CHANGE_LISTENERS.forEach(listener -> listener.accept(Context2D.this));
    }

    public static void addReadListener(final BiConsumer<Context2D, Rectangle> listener) {
        READ_LISTENERS.add(listener);
    }

    public static void removeReadListener(final BiConsumer<Context2D, Rectangle> listener) {
        READ_LISTENERS.remove(listener);
    }

    protected void notifyReadListeners(final Rectangle readArea) {
        READ_LISTENERS.forEach(listener -> listener.accept(Context2D.this, readArea));
    }

    public void drawImage(final Canvas imageCanvas, final int x, final int y) {
        final boolean imageComplete = currentGraphics.drawImage(imageCanvas.getBufferedImage(),
                x, y, null);
        if (!imageComplete) {
            throw new IllegalStateException("Drawing is incomplete, you need to use an ImageObserver");
        }
        notifyChangeListeners();
    }

    public void drawImage(final Canvas imageCanvas, final int x, final int y, final int width, final int height) {
        final boolean imageComplete = currentGraphics.drawImage(imageCanvas.getBufferedImage(),
                x, y, width, height, null);
        if (!imageComplete) {
            throw new IllegalStateException("Drawing is incomplete, you need to use an ImageObserver");
        }
        notifyChangeListeners();
    }

    public void drawImage(final Canvas imageCanvas, final int sx, final int sy, final double swidth,
            final double sheight, final int x, final int y, final int width, final int height) {
        drawImage(imageCanvas, sx, sy, (int) swidth, (int) sheight, x, y, width, height);
    }

    public void drawImage(final Canvas imageCanvas, final int sx, final int sy, final int swidth, final int sheight,
            final int x, final int y, final int width, final int height) {
        final boolean imageComplete = currentGraphics.drawImage(imageCanvas.getBufferedImage(),
                x, y, x + width, y + height, sx, sy, sx + swidth, sy + sheight, null);
        if (!imageComplete) {
            throw new IllegalStateException("Drawing is incomplete, you need to use an ImageObserver");
        }
        notifyChangeListeners();
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public ImageData getImageData(final int x, final int y, final int width, final int height) {
        if (bufferedImage == null) {
            return new ImageData(new int[0]);
        }
        final int bufferSize = 4 * width * height;
        if (buffer == null || buffer.length != bufferSize) {
            buffer = new int[bufferSize];
        }
        /* Timing.logDuration("getPixels", () -> */bufferedImage.getRaster().getPixels(x, y, width, height, buffer)/*
                                                                                                                    * )
                                                                                                                    */;
        // System.err.println("width="+width+" height="+height+"
        // width*height="+(width*height)+" pixels="+buffer.length);
        final ImageData newData = new ImageData(buffer);
        notifyReadListeners(new Rectangle(x, y, width, height));
        return newData;
    }

    public void clearRect(final int x, final int y, final int width, final int height) {
        final Color oldColor = currentGraphics.getColor();
        currentGraphics.clearRect(x, y, width, height);
        currentGraphics.setColor(oldColor);
        notifyChangeListeners();
    }

    public void scale(final double scaleWidth, final double scaleHeight) {
        currentGraphics.scale(scaleWidth, scaleHeight);
    }

    public void rotate(final double radians) {
        currentGraphics.rotate(radians);
    }

    public void translate(final double x, final double y) {
        currentGraphics.translate(x, y);
    }

    public void save() {
        states.push(currentGraphics);
        currentGraphics = (Graphics2D) currentGraphics.create();
    }

    public void restore() {
        if (!states.isEmpty()) {
            currentGraphics = states.pop();
        }
    }

    public static class ImageData {

        public int[] data;

        public ImageData(final int[] data) {
            this.data = data;
        }

    }

}