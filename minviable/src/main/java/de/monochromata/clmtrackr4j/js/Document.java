package de.monochromata.clmtrackr4j.js;

import java.util.ArrayList;
import java.util.List;

public class Document {

	private static final boolean LOG_TO_STDOUT = false;
	private static List<Runnable> TrackingLostListeners = new ArrayList<>();
	
	private final int defaultCanvasWidth;
	private final int defaultCanvasHeight;

	public Document(final int defaultCanvasWidth, final int defaultCanvasHeight) {
		this.defaultCanvasWidth = defaultCanvasWidth;
		this.defaultCanvasHeight = defaultCanvasHeight;
	}
	
	public static void addTrackingLostListener(final Runnable listener) {
		TrackingLostListeners.add(listener);
	}

	public static void removeTrackingLostListener(final Runnable listener) {
		TrackingLostListeners.remove(listener);
	}

	public Element createElement(final String elementType) {
		switch (elementType) {
		case "canvas":
			return new Canvas(defaultCanvasWidth, defaultCanvasHeight);
		default:
			return new Element(elementType);
		}
	}

	public Event createEvent(final String eventType) {
		return new Event(eventType);
	}

	public void dispatchEvent(final Event event) {
		if (LOG_TO_STDOUT) {
			System.out.println("Document.dispatchEvent(" + event + ")");
		}
		switch (event.message) {
		case "clmtrackrLost":
			TrackingLostListeners.forEach(Runnable::run);
			break;
		}
	}

}
