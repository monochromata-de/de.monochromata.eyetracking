package de.monochromata.clmtrackr4j.js;

public class Event {

	public final String eventType;
	public String message;
	public boolean bubbles;
	public boolean cancelable;

	public Event(final String eventType) {
		this.eventType = eventType;
	}

	public Event(final String eventType, final String message,
			final boolean bubbles, final boolean cancelable) {
		this(eventType);
		this.message = message;
		this.bubbles = bubbles;
		this.cancelable = cancelable;
	}

	public void initEvent(final String message, final boolean bubbles,
			final boolean cancelable) {
		this.message = message;
		this.bubbles = bubbles;
		this.cancelable = cancelable;
	}

	public String getEventType() {
		return eventType;
	}

	public String getMessage() {
		return message;
	}

	public boolean isBubbles() {
		return bubbles;
	}
	
	public boolean isCancelable() {
		return cancelable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bubbles ? 1231 : 1237);
		result = prime * result + (cancelable ? 1231 : 1237);
		result = prime * result + ((eventType == null) ? 0 : eventType.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (bubbles != other.bubbles)
			return false;
		if (cancelable != other.cancelable)
			return false;
		if (eventType == null) {
			if (other.eventType != null)
				return false;
		} else if (!eventType.equals(other.eventType))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Event [eventType=" + eventType + ", message=" + message + ", a=" + bubbles + ", b=" + cancelable + "]";
	}

}
