/**
 * Glue code to run the original JavaScript clmtrackr using Nashorn.
 */
package de.monochromata.clmtrackr4j.js;