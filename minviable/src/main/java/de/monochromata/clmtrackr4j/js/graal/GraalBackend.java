package de.monochromata.clmtrackr4j.js.graal;

import static java.lang.Double.NaN;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import de.monochromata.clmtrackr4j.ClmtrackrBackend;
import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.clmtrackr4j.js.Canvas;
import de.monochromata.clmtrackr4j.js.Document;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;

/**
 * A backend using <a href="https://github.com/graalvm/graaljs">Graal.JS</a> and
 * <a href="https://github.com/oracle/graal/blob/master/sdk">Graal SDK</a>.
 *
 * @see <a href=
 *      "https://amarszalek.net/blog/2018/06/08/evaluating-javascript-in-java-graalvm/">Evaluating
 *      JavaScript in Java with GraalVM</a>
 * @see <a href=
 *      "https://github.com/adikm/java-javascript-graalvm">github.com/adikm/java-javascript-graalvm</a>
 */
public class GraalBackend implements ClmtrackrBackend {

	protected boolean initialized = false;
	protected Context javaScriptContext;
	private Value javaScriptBindings;

	private List<Point2D> lastPositions;
	private Rectangle lastHeadboxRectangle;
	private double lastScore = NaN;
	private double lastConvergence = NaN;
	private double[] lastParameters;

	protected void ensureInitialized(final BufferedImage initialImage, final Visualizer visualizer) throws IOException {
		if (!initialized) {
			visualizer.initialize(initialImage);
			initialize(initialImage);
			initialized = true;
		}
	}

	protected void initialize(final BufferedImage initialImage) throws IOException {
		try {
			javaScriptContext = Context.newBuilder("js").allowAllAccess(true).build();
			javaScriptBindings = javaScriptContext.getBindings("js");

			javaScriptContext.eval("js", "window = {}; self = {};");
			javaScriptBindings.putMember("document", new Document(initialImage.getWidth(), initialImage.getHeight()));
			evalResource("/model_pca_20_svm.js");
			evalResource("/clmtrackr.js");
			javaScriptContext.eval("js", "var ctracker = new clm.tracker();" + "ctracker.init(pModel);");
			javaScriptContext.eval("js",
					"var positions = undefined;" + "var score = undefined;" + "var convergence = undefined;"
							+ "var headBox = undefined;" + "var headBoxRectangle = undefined;"
							+ "var parametersToReturn = undefined;");
		} catch (final Throwable t) {
			t.printStackTrace();
			throw t;
		}
	}

	@Override
	public boolean detectEyes(final BufferedImage image, final Visualizer visualizer) throws Exception {
		ensureInitialized(image, visualizer);
		javaScriptBindings.putMember("imageCanvas", new Canvas(image));
		javaScriptBindings.putMember("detected", false);
		final boolean detected = javaScriptContext
				.eval("js",
						"positions = ctracker.track(imageCanvas);\n" + "detected=(positions !== false);"
								+ "score=ctracker.getScore()+0.0;\n" + "convergence=ctracker.getConvergence();\n"
								+ "parametersToReturn = ctracker.getCurrentParameters();\n" + "detected;\n")
				.asBoolean();
		if (detected) {
			lastPositions = getPositions();
			lastHeadboxRectangle = getHeadboxRectangle();
			lastParameters = getParameters();
		}
		lastScore = javaScriptBindings.getMember("score").asDouble();
		lastConvergence = javaScriptBindings.getMember("convergence").asDouble();
		return detected;
	}

	protected List<Point2D> getPositions() {
		final Value positions = javaScriptBindings.getMember("positions");
		return range(0, (int) positions.getArraySize()).mapToObj(i -> positions.getArrayElement(i))
				.map(this::createPosition).collect(toList());
	}

	protected Double createPosition(final Value jsArray) {
		final double x = jsArray.getArrayElement(0).asDouble();
		final double y = jsArray.getArrayElement(1).asDouble();
		return new Point2D.Double(x, y);
	}

	protected Rectangle getHeadboxRectangle() {
		final Value headBox = javaScriptBindings.getMember("headBox");
		final int x = (int) headBox.getMember("x").asDouble();
		final int y = (int) headBox.getMember("y").asDouble();
		final int width = (int) headBox.getMember("width").asDouble();
		final int height = (int) headBox.getMember("height").asDouble();
		return new Rectangle(x, y, width, height);
	}

	protected double[] getParameters() {
		final Value array = javaScriptBindings.getMember("parametersToReturn");
		return range(0, (int) array.getArraySize()).mapToDouble(i -> array.getArrayElement(i).asDouble()).toArray();
	}

	@Override
	public ClmtrackrResult createResult(final BufferedImage image, final ClmtrackrResult previousResult,
			final Visualizer visualizer) {
		final ClmtrackrResult result = createResult(image, previousResult);
		visualizeHeadbox(image, visualizer);
		visualizer.eyesFound(image, lastPositions);
		return result;
	}

	protected ClmtrackrResult createResult(final BufferedImage image, final ClmtrackrResult previousResult) {
		final int iterations = previousResult == null ? 1 : previousResult.getIterations() + 1;
		return new ClmtrackrResult(image, lastPositions, iterations, lastScore, lastConvergence, lastParameters[0],
				lastParameters[1], lastParameters[2], lastParameters[3]);
	}

	private void visualizeHeadbox(final BufferedImage image, final Visualizer visualizer) {
		visualizer.faceFound(image, lastHeadboxRectangle);
	}

	protected void evalResource(final String classpathLocation) throws IOException {
		final URL resourceUrl = this.getClass().getResource(classpathLocation);
		final Source source = Source.newBuilder("js", resourceUrl).build();
		javaScriptContext.eval(source);
	}

}
