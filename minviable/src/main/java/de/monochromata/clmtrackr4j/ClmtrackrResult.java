package de.monochromata.clmtrackr4j;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

public class ClmtrackrResult {

	public static final int NUMBER_OF_POSITIONS = 71;

	private final BufferedImage image;
	private final List<Point2D> positions;
	private final int iterations;
	private final double score;
	private final double convergence;
	private final double scalingFactor;
	private final double rotationRadians;
	private final double translateXPx;
	private final double transplateYPx;

	public ClmtrackrResult(final BufferedImage image, final List<Point2D> positions, final int iterations,
			final double score, final double convergence, final double scalingFactor, final double rotationRadians,
			final double translateXPx, final double transplateYPx) {
		this.image = image;
		this.positions = positions;
		this.iterations = iterations;
		this.score = score;
		this.convergence = convergence;
		this.scalingFactor = scalingFactor;
		this.rotationRadians = rotationRadians;
		this.translateXPx = translateXPx;
		this.transplateYPx = transplateYPx;
	}

	public BufferedImage getImage() {
		return image;
	}

	public List<Point2D> getPositions() {
		return positions;
	}

	public int getIterations() {
		return iterations;
	}

	public double getScore() {
		return score;
	}

	public double getConvergence() {
		return convergence;
	}

	public double getScalingFactor() {
		return scalingFactor;
	}

	public double getRotationRadians() {
		return rotationRadians;
	}

	public double getTranslateXPx() {
		return translateXPx;
	}

	public double getTransplateYPx() {
		return transplateYPx;
	}

	@Override
	public String toString() {
		return "ClmtrackerResult [positions=" + positions + ", iterations=" + iterations + ", score=" + score
				+ ", convergence=" + convergence + ", scalingFactor=" + scalingFactor + ", rotationRadians="
				+ rotationRadians + ", translateXPx=" + translateXPx + ", transplateYPx=" + transplateYPx + "]";
	}

}
