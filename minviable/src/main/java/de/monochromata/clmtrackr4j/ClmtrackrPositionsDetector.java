package de.monochromata.clmtrackr4j;

import static java.lang.System.currentTimeMillis;
import static org.slf4j.LoggerFactory.getLogger;

import java.awt.image.BufferedImage;
import java.util.function.Function;

import org.slf4j.Logger;

import de.monochromata.eyetracking.minviable.eyes.EyePatchDetector;
import de.monochromata.eyetracking.minviable.visualization.NopVisualizer;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;

public class ClmtrackrPositionsDetector implements Function<BufferedImage, ClmtrackrResult> {

    private static final Logger LOGGER = getLogger(EyePatchDetector.class);
    public static final long NO_DETECTION_DURATION_AVAILABLE = -1;

    private final ClmtrackrBackend backend;
    private final Visualizer visualizer;
    private final boolean highQuality;
    private final int maxNumberOfIterations = 30;
    private long lastDetectionDurationMs = NO_DETECTION_DURATION_AVAILABLE;

    public ClmtrackrPositionsDetector(final ClmtrackrBackend backend) {
        this(backend, false);
    }

    public ClmtrackrPositionsDetector(final ClmtrackrBackend backend, final boolean highQuality) {
        this(backend, new NopVisualizer(), highQuality);
    }

    public ClmtrackrPositionsDetector(final ClmtrackrBackend backend, final Visualizer visualizer) {
        this(backend, visualizer, false);
    }

    public ClmtrackrPositionsDetector(final ClmtrackrBackend backend, final Visualizer visualizer,
            final boolean highQuality) {
        this.backend = backend;
        this.visualizer = visualizer;
        this.highQuality = highQuality;
    }

    @Override
    public ClmtrackrResult apply(final BufferedImage image) {
        try {
            ClmtrackrResult previousResult = null;
            ClmtrackrResult result = null;
            int iteration = 1;
            do {
                previousResult = result;

                final long startMs = currentTimeMillis();
                final boolean detected = backend.detectEyes(image, visualizer);
                if (detected) {
                    result = backend.createResult(image, result, visualizer);
                    updateLastDetectionDuration(startMs);
                } else if (!highQuality) {
                    visualizer.noEyesFound(image);
                    break;
                }
            } while (highQuality && repeatDetection(previousResult, result) && iteration++ < maxNumberOfIterations);
            return highQuality ? previousResult : result;
        } catch (final Throwable t) {
            LOGGER.error("Tracking failed", t);
        }
        return null;
    }

    protected void updateLastDetectionDuration(final long startMs) {
        final long endMs = currentTimeMillis();
        lastDetectionDurationMs = endMs - startMs;
    }

    /**
     * @return the last detection duration, or
     *         {@link #NO_DETECTION_DURATION_AVAILABLE}
     */
    public long getLastDetectionDurationMs() {
        return lastDetectionDurationMs;
    }

    protected boolean repeatDetection(final ClmtrackrResult previousResult, final ClmtrackrResult result) {
        final double currentConvergence = result == null ? 999999.0 : result.getConvergence();
        System.err.println(
                (previousResult == null ? "-" : previousResult.getConvergence()) + " -> " + currentConvergence);
        // TODO: Extract to an interface and add tests for these complex
        // conditions
        return currentConvergence == 999999.0
                || previousResult == null /*
                                           * happens when a new image is supplied and convergence from Clmtrackr is
                                           * still based on old image
                                           */
                || currentConvergence > 20.0 // TODO: Make the threshold
                                             // configurable
                || currentConvergence < previousResult.getConvergence() && previousResult.getConvergence() > 1.0; // TODO:
                                                                                                                  // Also
                                                                                                                  // make
                                                                                                                  // this
                                                                                                                  // bound
                                                                                                                  // (1.0)
                                                                                                                  // configurable
    }

}
