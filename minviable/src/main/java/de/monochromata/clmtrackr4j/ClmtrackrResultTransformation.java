package de.monochromata.clmtrackr4j;

import static java.awt.RenderingHints.KEY_INTERPOLATION;
import static java.lang.Math.ceil;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public interface ClmtrackrResultTransformation {
	
	int SKETCH_CANVAS_WIDTH = 134;
	int SKETCH_CANVAS_HEIGHT = 122;
	
	double RELATIVE_LEFT_EDGE = 0.25;
	double RELATIVE_RIGHT_EDGE = 0.65;
	double RELATIVE_TOP_EDGE = 0.20;
	double RELATIVE_BOTTOM_EDGE = 0.35;
	
	/**
	 * Applies the transformation in the given result, returning a transformed
	 * result without transformation parameters.
	 * @param scalingFactor how the image of the given result shall be scaled - relative
	 * 		to the sketch canvas used by Clmtrackr.
	 * @param interpolation One of {@link RenderingHints#VALUE_INTERPOLATION_BICUBIC},
	 * 	{@link RenderingHints#VALUE_INTERPOLATION_BILINEAR},
	 *  {@link RenderingHints#VALUE_INTERPOLATION_NEAREST_NEIGHBOR}
	 * 
	 * @see #SKETCH_CANVAS_WIDTH
	 * @see #SKETCH_CANVAS_HEIGHT
	 */
	static ClmtrackrResult transformResult(final ClmtrackrResult result, double scalingFactor,
			Object interpolation) {
		// Calculation of transformation parameters is based on the else branch of
		// the Clmtrackr track method (see clmtrackr.js).
		final double halfPI = Math.PI/2;
		double rotation = halfPI - Math.atan((result.getScalingFactor()+1)/-result.getRotationRadians());
		if (rotation > halfPI) {
			rotation -= Math.PI;
		}
		final double clmtrackrScaling = 1/(-result.getRotationRadians() / Math.sin(rotation));
		final Pair<BufferedImage,AffineTransform> transformationResult = 
				transform(result.getImage(), clmtrackrScaling, scalingFactor,
						rotation, -result.getTranslateXPx(),
						-result.getTransplateYPx(), interpolation);
		final List<Point2D> transformedPositions =
				transform(result.getPositions(), transformationResult.getRight());
		return new ClmtrackrResult(transformationResult.getLeft(), transformedPositions,
				result.getIterations(), result.getScore(), result.getConvergence(),
				1.0d, 0.0d, 0.0d, 0.0d);
	}
	
	/**
	 * Transform an image.
	 * 
	 * @param image the image of which to return a transformed copy
	 * @param scalingFactor the scaling applied by Clmtrackr
	 * @param additionalScalingFactor extra scaling applied (after the one applied by Clmtrackr)
	 * @param rotationRadians the rotation angle in radians
	 * @param translationXPx translation along the x-axis in pixels
	 * @param translationYPx translation along the y-axis in pixels
	 * @param interpolation One of {@link RenderingHints#VALUE_INTERPOLATION_BICUBIC},
	 * 	{@link RenderingHints#VALUE_INTERPOLATION_BILINEAR},
	 *  {@link RenderingHints#VALUE_INTERPOLATION_NEAREST_NEIGHBOR}
	 * @return both the transformed copy of the image and the affine transform applied, in case e.g.
	 * 	points in the image need to be transformed, too.
	 */
	static Pair<BufferedImage,AffineTransform> transform(final BufferedImage image,
			final double scalingFactor, final double additionalScalingFactor,
			final double rotationRadians,
			final double translationXPx, final double translationYPx,
			final Object interpolation) {
		final int targetWidth = (int)ceil(SKETCH_CANVAS_WIDTH*additionalScalingFactor*(RELATIVE_RIGHT_EDGE-RELATIVE_LEFT_EDGE));
		final int targetHeight = (int)ceil(SKETCH_CANVAS_HEIGHT*additionalScalingFactor*(RELATIVE_BOTTOM_EDGE-RELATIVE_TOP_EDGE));
		final BufferedImage targetImage = new BufferedImage(targetWidth, targetHeight, image.getType());
		final Graphics2D targetGraphics = targetImage.createGraphics();
		targetGraphics.scale(scalingFactor*additionalScalingFactor, scalingFactor*additionalScalingFactor);
		targetGraphics.rotate(rotationRadians);
		final double newTranslationXPx = translationXPx-(SKETCH_CANVAS_WIDTH*RELATIVE_LEFT_EDGE*(1/scalingFactor));
		final double newTranslationYPx = translationYPx-(SKETCH_CANVAS_HEIGHT*RELATIVE_TOP_EDGE*(1/scalingFactor));
		targetGraphics.translate(newTranslationXPx, newTranslationYPx);
		targetGraphics.setRenderingHint(KEY_INTERPOLATION, interpolation);
		final boolean completed = targetGraphics.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
		if(!completed) {
			throw new RuntimeException("Transformation did not complete immediately. An ImageObserver is required.");
		}
		return new ImmutablePair<>(targetImage, targetGraphics.getTransform());
	}

	static List<Point2D> transform(final List<Point2D> positions,
			final AffineTransform transform) {
		return positions.stream()
			.map(position -> transform.transform(position, null))
			.collect(Collectors.toList());
	}

}
