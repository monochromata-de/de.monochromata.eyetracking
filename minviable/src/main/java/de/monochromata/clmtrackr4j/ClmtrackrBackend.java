package de.monochromata.clmtrackr4j;

import java.awt.image.BufferedImage;

import de.monochromata.clmtrackr4j.js.graal.GraalBackend;
import de.monochromata.eyetracking.minviable.visualization.Visualizer;

public interface ClmtrackrBackend {

    boolean detectEyes(final BufferedImage image, final Visualizer visualizer) throws Exception;

    /**
     * Creates a result for the last {@link #detectEyes(BufferedImage, Visualizer)}
     * invocation that returned {@literal true}.
     */
    public ClmtrackrResult createResult(final BufferedImage image, final ClmtrackrResult previousResult,
            final Visualizer visualizer);

    static ClmtrackrBackend createBackend(final String backendType) {
        switch (backendType) {
        case "graal":
            return new GraalBackend();
        default:
            throw new IllegalArgumentException("Unknown backend type: " + backendType);
        }
    }

}
