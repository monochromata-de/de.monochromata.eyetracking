/**
 * An adaptation of the JavaScript-based face-tracking library
 * <a href="https://github.com/auduno/clmtrackr">clmtrackr</a> for Java.
 *
 * References
 * <ul>
 * <li>Jason M. Saragih, Simon Lucey, and Jeffrey F. Cohn. 2011. Deformable
 * Model Fitting by Regularized Landmark Mean-Shift. Int. J. Comput. Vision 91,
 * 2 (January 2011), 200-215.</li>
 * <li><a href=
 * "http://www.milbo.users.sonic.net/stasm/index.html">STASM</a></li>
 * <li><a href="http://www.ri.cmu.edu/pub_files/2009/9/CameraReady-6.pdf"> JM
 * Saragih, S Lucey, JF Cohn. 2009. Face alignment through subspace constrained
 * mean-shifts. IEEE 12th International Conference on Computer Vision,
 * 1034-1041.</a></li>
 * </ul>
 */
package de.monochromata.clmtrackr4j;