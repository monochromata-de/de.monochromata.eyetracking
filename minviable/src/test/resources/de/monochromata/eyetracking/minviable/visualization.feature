Feature: Visualization
  The work of Clmtrackr can be inspected via visualization features.

  Scenario: Do not render anything by default

  Scenario Outline: Render detected face
    It should be possible to inspect for each image where clmtrackr
    detected the face.

    Given a buffered image from file /lowQualityFaceImage.png
    And a <backend> backend
    And a FaceVisualizer
    When eye patches are requested
    # The image assertions do not work anymore because we're not paiting on the original bufferedImage anymore.
    #Then the buffered image is drawn onto the given frame
    # And the bounds of the detected eyes are drawn on the buffered image in the frame
    # And the detected face is drawn on the buffered image in the frame
    # And the frame is closed
    Then the frame is closed

    Examples: 
      | backend |
      | graal   |

  Scenario Outline: Render image processing
    It should be possible to visualize how clmtrackr processes the image.

    Given a buffered image from file /lowQualityFaceImage.png
    And a <backend> backend
    And an ImageProcessingVisualizer
    When eye patches are requested
    # The image assertions do not work anymore because we're not paiting on the original bufferedImage anymore.
    #Then the buffered image is drawn onto the given frame
    # And the bounds of the detected eyes are drawn on the buffered image in the frame
    # And the detected face is drawn on the buffered image in the frame
    # And the bounds of the clmtrackr patches are drawn on a canvas in the frame
    # And all canvases are shown in the frame
    # And the frame is closed
    Then all canvases are shown in the frame
    And the frame is closed

    Examples: 
      | backend |
      | graal   |

Scenario: Compare training and test data
 Given three training data
   And three test data truth
   And three predictions from the test data
  When the data is visualized
  Then training data is painted in blue
   And test data truth is painted in green
   And test data predictions are painted in red with
   And test data truth and predictions are connected by lines to visualize prediction error

#Scenario: Visualize all intermediate processing steps of an image
#  TODO: Also write step definitions/tests for HistogramEqualization
#    TODO: Both qualitatively and for given sample data
# Given directory src/test/resources/168images/ with image files
#   And index file src/test/resources/168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv
#  When visualization is requested for line number 2 and image webcam_1486759052071_368_677.png
#  Then the webcam shot is added to the panel at 320x260px
#   And the grayscale version of the webcam shot is added to the panel at 320x260px
#   And the eye patch is added to the panel, scaled to 140x50px
#   And the scaled eye patch is added to the panel, scaled to 100x60px
#   And the eye patch with equalized histogram is added to the panel, scaled to 100x60px
#   And 60 lines are added to the panel, as wide as the pixel value, 1px high

Scenario: Show datum details on mouse-over
  TODO: The actual point is to collect/create the data to be shown
 Given three training data
   And three test data truth
   And three predictions from the test data
  When the data is visualized
   #And the mouse is over / close to a datum
  #Then the details of the datum are presented