Feature: Eye patches extraction
  
  How eye bounds are calculated from Clmtrackr positions and additional arguments.

  Background: 
    Given a buffered image from file /webcam_1491140516649_434_433.png
    And positions
      """
      23=(323.77080525109295,296.19644279340025)
      27=(339.6071636934687,292.28068739136154)
      25=(356.56604958011565,291.7992951421605)
      30=(406.00484340340705,285.4335432968128)
      32=(422.86748911169906,281.31790990583164)
      28=(439.65031511237777,280.9356880923432)
      """
    And scalingFactor=1.8482316703031711
    And rotationRadians=-0.29694056339338
    And translationX=209.22475865079323px
    And translationY=205.80616934191303px

  Scenario: Extract eye patches with bounds from pupil position
    When eye patches are extracted based on pupil position
    Then the left eye has bounds x=335 y=289 width=10 height=6 and data
      """
       7.0  6.0  7.0  6.0  6.0  4.0  3.0  5.0 15.0 15.0
       7.0  7.0  5.0  4.0  2.0  3.0  3.0  3.0 12.0 11.0
       8.0 14.0 14.0 11.0  4.0  3.0  2.0  2.0 11.0 18.0
       9.0 11.0 14.0 14.0 10.0  9.0  8.0 12.0 20.0 25.0
      10.0 14.0 15.0 12.0 14.0 18.0 22.0 26.0 21.0 26.0
      11.0 12.0 16.0 17.0 19.0 20.0 20.0 24.0 24.0 23.0
      """
    And the right eye has bounds x=418 y=278 width=10 height=6 and data
      """
      12.0 12.0 10.0  8.0  6.0  4.0  3.0 10.0 20.0 14.0
      12.0 12.0 10.0  8.0  6.0  4.0  4.0  4.0 10.0 11.0
      14.0 13.0 10.0  7.0  3.0  3.0  4.0  3.0  6.0 12.0
      13.0 16.0 18.0 14.0  4.0  2.0  2.0  4.0 22.0 28.0
      15.0 18.0 24.0 23.0 17.0 16.0 15.0 27.0 40.0 31.0
      18.0 19.0 23.0 25.0 26.0 29.0 21.0 34.0 28.0 26.0
      """

  Scenario: Extract eye patches with bounds from eye corners
    When eye patches are extracted based on eye corners
    Then the left eye has bounds x=335 y=291 width=10 height=6 and data
      """
       8.0 14.0 14.0 11.0  4.0  3.0  2.0  2.0 11.0 18.0
       9.0 11.0 14.0 14.0 10.0  9.0  8.0 12.0 20.0 25.0
      10.0 14.0 15.0 12.0 14.0 18.0 22.0 26.0 21.0 26.0
      11.0 12.0 16.0 17.0 19.0 20.0 20.0 24.0 24.0 23.0
      12.0 12.0 17.0 19.0 21.0 24.0 22.0 17.0 19.0 26.0
      16.0 17.0 12.0 16.0 17.0 17.0 21.0 24.0 47.0 86.0
      """
    And the right eye has bounds x=418 y=280 width=10 height=6 and data
      """
      14.0 13.0 10.0  7.0  3.0  3.0  4.0  3.0  6.0 12.0
      13.0 16.0 18.0 14.0  4.0  2.0  2.0  4.0 22.0 28.0
      15.0 18.0 24.0 23.0 17.0 16.0 15.0 27.0 40.0 31.0
      18.0 19.0 23.0 25.0 26.0 29.0 21.0 34.0 28.0 26.0
      18.0 16.0 19.0 23.0 25.0 25.0 27.0 31.0 32.0 31.0
      24.0 18.0 17.0 19.0 22.0 20.0 21.0 20.0 30.0 38.0
      """

  Scenario: Extract eye patches from transformed image with bounds from pupil position
    When eye patches are extracted from a translated image based on pupil position
    Then the left eye has bounds x=29 y=18 width=10 height=6 and data
      """
       7.0  7.0  6.0  6.0  5.0  4.0  4.0  3.0  6.0 15.0
       8.0  9.0 10.0  8.0  5.0  3.0  3.0  3.0  4.0 13.0
       8.0 11.0 13.0 13.0  8.0  4.0  3.0  2.0  3.0 12.0
       9.0 12.0 14.0 14.0 12.0 10.0  9.0 10.0 13.0 20.0
      10.0 12.0 15.0 14.0 15.0 17.0 20.0 22.0 22.0 23.0
      12.0 12.0 16.0 17.0 19.0 20.0 20.0 23.0 24.0 23.0
      """
    And the right eye has bounds x=117 y=16 width=10 height=6 and data
      """
      12.0 12.0 10.0  8.0  6.0  5.0  4.0  5.0 10.0 15.0
      14.0 13.0 11.0  8.0  4.0  4.0  4.0  4.0  6.0 10.0
      14.0 16.0 17.0 13.0  5.0  3.0  3.0  3.0  7.0 12.0
      16.0 19.0 23.0 21.0 12.0  9.0  6.0  8.0 20.0 27.0
      18.0 20.0 23.0 25.0 22.0 22.0 18.0 27.0 36.0 32.0
      17.0 17.0 20.0 24.0 25.0 27.0 24.0 33.0 29.0 27.0
      """

  Scenario: Extract eye patches from transformed image with bounds from eye corners
    When eye patches are extracted from a translated image based on eye corners
    Then the left eye has bounds x=29 y=20 width=10 height=6 and data
      """
       8.0 11.0 13.0 13.0  8.0  4.0  3.0  2.0  3.0 12.0
       9.0 12.0 14.0 14.0 12.0 10.0  9.0 10.0 13.0 20.0
      10.0 12.0 15.0 14.0 15.0 17.0 20.0 22.0 22.0 23.0
      12.0 12.0 16.0 17.0 19.0 20.0 20.0 23.0 24.0 23.0
      14.0 15.0 15.0 18.0 20.0 23.0 22.0 19.0 20.0 23.0
      19.0 19.0 16.0 19.0 18.0 17.0 20.0 22.0 33.0 49.0
      """
    And the right eye has bounds x=117 y=18 width=10 height=6 and data
      """
      14.0 16.0 17.0 13.0  5.0  3.0  3.0  3.0  7.0 12.0
      16.0 19.0 23.0 21.0 12.0  9.0  6.0  8.0 20.0 27.0
      18.0 20.0 23.0 25.0 22.0 22.0 18.0 27.0 36.0 32.0
      17.0 17.0 20.0 24.0 25.0 27.0 24.0 33.0 29.0 27.0
      21.0 17.0 18.0 21.0 23.0 23.0 25.0 29.0 32.0 31.0
      49.0 39.0 32.0 30.0 29.0 28.0 28.0 29.0 34.0 39.0
      """
