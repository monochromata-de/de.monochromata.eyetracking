Feature: Calibration
  Singular calibration and validation. See also: continuousCalibration.feature.

Scenario Outline: Calibration with 20 images with a scaling feature extractor
  Multiple data are used for calibration and validation.
  // TODO: Use a LibsvmCenterer as preprocessor? i.e. center the data
  // TODO: How to create the images and manual contexts
    // TODO: Use a ClmtrackrEyePatchDetector with highQuality=true?
  // TODO: Also assert validation results
  // TODO: Eigentlich ist Feature-Suche / CV notwendig, um bestmögliche Hyper-Parameter zu bestimmen 
#Given a noop webcam
#  And a scaling feature extractor
#  And <trainingSize> images are used for training
#  And <testSize> images are used for validation
#  And eye patches and manual contexts from <dataFile>
#  And height interval <interval>
#  And padding <padding>
#  And sample <kernelType> hyper-parameters
# When the images and manual contexts are used for calibration
# Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
# 
#   Examples:
#   | trainingSize | testSize | interval | padding | kernelType | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
#   | 10           | 10       | 0-10     | 0px     | RBF        | 429.71     | 269.31     | 429.71      | 269.31      | /20setsOfEyePatches2/20setsOfEyePatchesAndManualContexts.csv  |
#   | 15           | 5        | 0-10     | 0px     | RBF        | 346.25     | 172.54     | 346.83      | 172.69      | /20setsOfEyePatches2/20setsOfEyePatchesAndManualContexts.csv  |
#   | 10           | 10       | 0-10     | 0px     | linear     | 175.97     | 320.20     | 220.05      | 244.79      | /20setsOfEyePatches2/20setsOfEyePatchesAndManualContexts.csv  |
#   | 15           | 5        | 0-10     | 0px     | linear     |  68.55     | 114.72     | 162.98      |  71.85      | /20setsOfEyePatches2/20setsOfEyePatchesAndManualContexts.csv  |
#   | 10           | 10       | 0-10     | 0px     | polynomial | 193.26     | 296.36     | 214.56      | 252.84      | /20setsOfEyePatches2/20setsOfEyePatchesAndManualContexts.csv  |
#   | 15           | 5        | 0-10     | 0px     | polynomial |  44.97     | 133.41     | 111.98      |  67.46      | /20setsOfEyePatches2/20setsOfEyePatchesAndManualContexts.csv  |

Scenario Outline: Calibration with 168 images with a scaling feature extractor
  Multiple data are used for calibration and validation.
  // TODO: Use a LibsvmCenterer as preprocessor? i.e. center the data
  // TODO: How to create the images and manual contexts
    // TODO: Use a ClmtrackrEyePatchDetector with highQuality=true?
  // TODO: Also assert validation results
  // TODO: Eigentlich ist Feature-Suche / CV notwendig, um bestmögliche Hyper-Parameter zu bestimmen 
#Given a noop webcam
#  And a scaling feature extractor
#  And <trainingSize> images are used for training
#  And <testSize> images are used for validation
#  And eye patches and manual contexts from <dataFile>
#  And height interval <interval>
#  And padding <padding>
#  And sample <kernelType> hyper-parameters
# When the images and manual contexts are used for calibration
# Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
# 
#   # TODO: Padding could be split for top/right/bottom/left (per eye) - and combinations
#   Examples:
#   | trainingSize | testSize | interval | padding | kernelType | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
#   | 140          | 20       | 0-10     | 0px     | RBF        | 319.84     | 161.41     | 335.42      | 162.10      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv |
#   | 140          | 20       | 5-6      | 0px     | RBF        | 279.54     | 172.14     | 295.73      | 171.26      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv |
#   | 140          | 20       | 0-10     | 1px     | RBF        | 340.36     | 161.27     | 347.96      | 158.80      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 140          | 20       | 6-7      | 1px     | RBF        | 301.17     | 170.78     | 317.67      | 167.06      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 140          | 20       | 0-10     | 2px     | RBF        | 341.20     | 160.67     | 345.50      | 157.14      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 140          | 20       | 7-8      | 2px     | RBF        | 303.38     | 170.85     | 312.24      | 165.30      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 120          | 20       | 7-8      | 2px     | RBF        | 394.13     | 170.47     | 398.90      | 170.05      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 140          | 20       | 0-10     | 0px     | linear     |  89.24     | 104.80     |  95.16      |  83.40      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv |
#   | 140          | 20       | 5-6      | 0px     | linear     |  88.19     | 110.32     |  86.23      | 108.16      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv |
#   | 140          | 20       | 0-10     | 1px     | linear     | 120.52     | 100.28     |  77.38      | 100.52      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 140          | 20       | 6-7      | 1px     | linear     | 112.18     | 104.82     | 102.16      | 106.86      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 140          | 20       | 0-10     | 2px     | linear     | 120.25     |  68.95     |  91.48      |  87.19      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 140          | 20       | 7-8      | 2px     | linear     | 111.05     |  70.37     |  87.97      |  98.53      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 120          | 20       | 7-8      | 2px     | linear     | 103.09     |  89.16     |  93.57      | 117.04      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 140          | 20       | 0-10     | 0px     | polynomial |  80.32     | 104.88     |  86.58      |  73.80      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv |
#   | 140          | 20       | 5-6      | 0px     | polynomial |  82.51     | 114.61     |  85.70      |  91.26      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContexts.csv |
#   | 140          | 20       | 0-10     | 1px     | polynomial |  99.82     | 104.28     |  77.37      |  77.83      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 140          | 20       | 6-7      | 1px     | polynomial |  95.00     | 110.57     |  91.21      |  82.22      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 140          | 20       | 0-10     | 2px     | polynomial | 106.06     |  75.62     |  65.00      |  78.61      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 140          | 20       | 7-8      | 2px     | polynomial | 101.82     |  76.39     |  72.63      |  66.92      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 120          | 20       | 7-8      | 2px     | polynomial |  94.03     |  93.20     |  96.63      |  99.45      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |


Scenario Outline: Calibration with 478 images with scaling feature extractor
  Multiple data are used for calibration and validation.
  // TODO: Use a LibsvmCenterer as preprocessor? i.e. center the data
  // TODO: How to create the images and manual contexts
    // TODO: Use a ClmtrackrEyePatchDetector with highQuality=true?
  // TODO: Also assert validation results
  // TODO: Eigentlich ist Feature-Suche / CV notwendig, um bestmögliche Hyper-Parameter zu bestimmen 
#Given a noop webcam
#  And a scaling feature extractor
#  And <trainingSize> images are used for training
#  And <testSize> images are used for validation
#  And eye patches and manual contexts from <dataFile>
#  And height interval <interval>
#  And padding <padding>
#  And sample <kernelType> hyper-parameters
# When the images and manual contexts are used for calibration
# Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
# 
#   # TODO: Padding could be split for top/right/bottom/left (per eye) - and combinations
#   Examples:
#   | trainingSize | testSize | interval | padding | kernelType | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
#   | 200          | 20       | 0-10     | 2px     | RBF        | 445.53     | 292.72     | 446.78      | 287.99      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 300          | 20       | 0-10     | 2px     | RBF        | 286.99     | 177.93     | 287.18      | 175.15      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 400          | 20       | 0-10     | 2px     | RBF        | 403.80     | 170.37     | 414.45      | 174.68      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 400          | 20       | 0-10     | 1px     | linear     |  66.07     | 115.05     | 104.58      | 166.15      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 200          | 20       | 0-10     | 2px     | linear     | 129.52     | 100.94     | 140.71      |  97.57      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 300          | 20       | 0-10     | 2px     | linear     |  94.34     |  95.34     | 155.33      | 108.41      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 400          | 20       | 0-10     | 2px     | linear     |  75.78     |  64.76     |  92.24      | 145.80      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 200          | 20       | 0-10     | 0px     | polynomial | 123.72     | 175.79     | 190.33      | 150.42      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith0Padding.csv |
#   | 300          | 20       | 0-10     | 0px     | polynomial | 139.99     | 106.63     | 113.87      | 126.13      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith0Padding.csv |
#   | 400          | 20       | 0-10     | 0px     | polynomial |  50.32     | 178.72     |  97.80      | 121.02      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith0Padding.csv |
#   | 200          | 20       | 0-10     | 1px     | polynomial | 146.00     | 155.86     | 148.84      | 151.09      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 300          | 20       | 0-10     | 1px     | polynomial |  97.34     | 102.06     | 106.23      |  93.96      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 400          | 20       | 0-10     | 1px     | polynomial |  54.14     | 110.13     |  76.66      | 140.32      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 300          | 20       | 6-6      | 1px     | polynomial |  97.96     |  88.87     | 158.22      |  97.61      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 200          | 20       | 0-10     | 2px     | polynomial | 121.14     | 111.82     | 113.62      | 119.26      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 300          | 20       | 0-10     | 2px     | polynomial |  89.08     |  86.11     | 117.49      | 103.24      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 400          | 20       | 0-10     | 2px     | polynomial |  52.99     |  65.18     |  78.72      | 135.67      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |


Scenario Outline: Calibration with 478 images with a default feature extractor
  Multiple data are used for calibration and validation.
  // TODO: Use a LibsvmCenterer as preprocessor? i.e. center the data
  // TODO: How to create the images and manual contexts
    // TODO: Use a ClmtrackrEyePatchDetector with highQuality=true?
  // TODO: Also assert validation results
  // TODO: Eigentlich ist Feature-Suche / CV notwendig, um bestmögliche Hyper-Parameter zu bestimmen 
Given a noop webcam
  And a default feature extractor from eye patches
  And <trainingSize> images are used for training
  And <testSize> images are used for validation
  And eye patches and manual contexts from <dataFile>
  And height interval <interval>
  And padding <padding>
  And sample <kernelType> hyper-parameters
 When the images and manual contexts are used for calibration
 Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
 
   # TODO: Padding could be split for top/right/bottom/left (per eye) - and combinations
   Examples:
   | trainingSize | testSize | interval | padding | kernelType | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
   # From Pupil Position
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding0.csv
   | 400          | 20       | 0-10     | 0px     | polynomial |  57.66     | 126.96     | 119.62      | 113.62      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding0.csv |
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding1.csv
   | 400          | 20       | 0-10     | 1px     | polynomial |  85.41     | 112.86     | 115.64      | 132.40      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding1.csv |
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding2.csv
   | 400          | 20       | 0-10     | 2px     | polynomial |  93.15     | 118.14     | 143.97      | 137.31      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding2.csv |
   # From Eye Corners
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding0.csv
   | 400          | 20       | 0-10     | 0px     | polynomial |  63.58     | 114.69     | 135.45      | 125.48      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding0.csv |
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv
   |  10          | 20       | 0-10     | 1px     | polynomial | 202.26     | 229.52     | 281.20      | 282.66      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   |  20          | 20       | 0-10     | 1px     | polynomial | 158.45     | 199.73     | 211.18      | 215.03      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   |  30          | 20       | 0-10     | 1px     | polynomial | 133.26     | 132.66     | 178.72      | 112.51      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   |  50          | 20       | 0-10     | 1px     | polynomial | 140.65     | 173.58     | 174.97      | 127.13      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   | 100          | 20       | 0-10     | 1px     | polynomial | 109.23     | 119.08     | 127.73      | 107.87      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   | 200          | 20       | 0-10     | 1px     | polynomial |  96.90     | 162.74     | 151.31      | 139.13      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   | 400          | 20       | 0-10     | 1px     | polynomial |  55.13     |  81.29     | 128.83      | 122.71      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding1.csv |
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding2.csv
   | 400          | 20       | 0-10     | 2px     | polynomial |  89.35     |  83.46     | 132.47      | 117.64      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding2.csv |
   # With Transformation, from eye corners
   # /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsTransformedFromEyeCornersWithPadding0.csv
   #| 400          | 20       | 0-10     | 0px     | polynomial |  66.87 !   | 115.78     | 106.71      | 144.56      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsTransformedFromEyeCornersWithPadding0.csv |


Scenario Outline: Calibration with 478 images with various feature extractors
  Multiple data are used for calibration and validation. 
Given a noop webcam
  And a <featureExtractor> feature extractor
  And <trainingSize> images are used for training
  And <testSize> images are used for validation
  And clmtrackr results and manual contexts from <dataFile>
  And height interval 0-10
  And padding 0px
  And sample polynomial hyper-parameters
 When the clmtrackr results and manual contexts are used for calibration
 Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
 
   Examples:
   | trainingSize | testSize | featureExtractor                    | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
   | 400          | 20       | positions                           | 362.74     | 264.47     | 362.74      | 264.47      | src/test/resources/478setsOfEyePatches/478positions.csv |
   | 400          | 20       | face model parameters               | 460.93     | 190.65     | 460.93      | 190.65      | src/test/resources/478setsOfEyePatches/478positions.csv |
   | 400          | 20       | positions and face model parameters | 359.83     | 261.76     | 359.83      | 261.76      | src/test/resources/478setsOfEyePatches/478positions.csv |


Scenario Outline: Calibration with 478 images with various clmtrackr result and eye patches feature extractors
  Multiple data are used for calibration and validation. 
Given a noop webcam
  And clmtrackr results and manual contexts from <positionsDataFile>
  And eye patches and manual contexts from <eyePatchesDataFile>
  And a <featureExtractor> feature extractor
  And <trainingSize> images are used for training
  And <testSize> images are used for validation
  And height interval 0-10
  And padding 0px
  And sample polynomial hyper-parameters
 When the clmtrackr results and manual contexts are used for calibration
 Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
 
   Examples:
   | trainingSize | testSize | featureExtractor                              | errorLeftX | errorLeftY | errorRightX | errorRightY | positionsDataFile                                       | eyePatchesDataFile                                                                         |
   | 400          | 20       | default and face model parameters             | 57.57      | 126.32     | 118.80      | 113.12      | src/test/resources/478setsOfEyePatches/478positions.csv | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding0.csv |
   | 400          | 20       | default and positions                         | 79.57      | 163.26     | 109.49      | 113.88      | src/test/resources/478setsOfEyePatches/478positions.csv | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding0.csv |
   | 400          | 20       | default, positions, and face model parameters | 78.73      | 162.88     | 108.96      | 113.02      | src/test/resources/478setsOfEyePatches/478positions.csv | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding0.csv |


Scenario Outline: Saves correction model mementos with scaling feature extractor
  Multiple data are used for calibration and validation.
#Given a noop webcam
#  And a scaling feature extractor
#  And <trainingSize> images are used for training
#  And <testSize> images are used for validation
#  And eye patches and manual contexts from <dataFile>
#  And height interval <interval>
#  And padding <padding>
#  And sample <kernelType> hyper-parameters
# When the images and manual contexts are used for calibration
#  And the mementos of the correction models are saved to disk
# Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
#  And a file for the memento exists
#  
#   Examples:
#   | trainingSize | testSize | interval | padding | kernelType | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
#   | 140          | 20       | 7-8      | 2px     | polynomial | 101.82     |  76.39     |  72.63      |  66.92      | /168setsOfEyePatches/168setsOfEyePatchesAndManualContextsWith2Padding.csv |
#   | 150          | 20       | 7-8      | 1px     | polynomial | 104.34     | 139.42     | 125.48      |  88.57      | /214setsOfEyePatches/214setsOfEyePatchesAndManualContextsWith1Padding.csv |
#   | 400          | 20       | 0-10     | 2px     | polynomial |  66.29     |  64.46     |  60.88      |  81.94      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsWith2Padding.csv |


Scenario Outline: Saves correction model mementos with a default feature extractor
  Multiple data are used for calibration and validation. 
Given a noop webcam
  And a default feature extractor from eye patches
  And <trainingSize> images are used for training
  And <testSize> images are used for validation
  And eye patches and manual contexts from <dataFile>
  And height interval <interval>
  And padding <padding>
  And sample <kernelType> hyper-parameters
 When the images and manual contexts are used for calibration
  And the mementos of the correction models are saved to disk
 Then validation yields an error of x=<errorLeftX>px, y=<errorLeftY>px (left eye) and x=<errorRightX>px, y=<errorRightY>px (right eye)
  And a file for the memento exists
  
   Examples:
   | trainingSize | testSize | interval | padding | kernelType | errorLeftX | errorLeftY | errorRightX | errorRightY | dataFile                                                      |
   | 400          | 20       | 0-10     | 0px     | polynomial |  57.66     | 126.96     | 119.62      | 113.62      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromPupilPositionWithPadding0.csv |
   | 390          | 20       | 0-10     | 2px     | polynomial |  93.97     | 104.60     | 138.61      | 145.85      | /478setsOfEyePatches/478setsOfEyePatchesAndManualContextsFromEyeCornersWithPadding2.csv |