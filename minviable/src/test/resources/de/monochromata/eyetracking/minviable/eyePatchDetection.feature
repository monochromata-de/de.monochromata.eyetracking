Feature: Eye patch detection

  The eye patch detector shall detect eye patches in


  Scenario Outline: Detect eye patch
    TODO: The image is not scaled very well initially
    TODO: Add an example for a larger file

    Given a buffered image from file <imageFile>
    And a <backend> backend
    When eye patches are requested
    Then provide non-null eye patches
    And iterations=<iterations>
    And score=<score>
    And convergence=<convergence>
    And left eye is at <leftEyeBounds>
    And left eye has data <leftEyeData>
    And right eye is at <rightEyeBounds>
    And right eye has data <rightEyeData>

    Examples: 
      | imageFile                | backend | iterations | score | convergence | leftEyeBounds                    | leftEyeData                         | rightEyeBounds                   | rightEyeData                         |
      | /lowQualityFaceImage.png | graal   |          1 |   0.0 |    999999.0 | x=170, y=105, width=10, height=6 | /lowQualityFaceImageLeftEyeData.csv | x=209, y=106, width=10, height=6 | /lowQualityFaceImageRightEyeData.csv |


  Scenario Outline: Cannot detect eye patches of partial face
    TODO: This one actually yields an exception in JavaScript

    Given a buffered image from file /faceWithoutChin.png
    And a <backend> backend
    When eye patches are requested
    Then provide null eye patches

    Examples: 
      | backend |
      | graal   |


  Scenario Outline: Re-detect eye patches until convergence does not decrease anymore
    TODO: Alternatively set a minimum threshold to be reached

    Given a buffered image from file /lowQualityFaceImage.png
    And a <backend> backend
    And requesting high-quality eye patches
    When eye patches are requested
    Then provide non-null eye patches
    And iterations=17
    And score=0.0
    And convergence=<convergence>
    And left eye is at x=172, y=108, width=10, height=6
    And left eye has data /lowQualityFaceImageLeftEyeHighQualityData.csv
    And right eye is at x=208, y=106, width=10, height=6
    And right eye has data /lowQualityFaceImageRightEyeHighQualityData.csv

    Examples: 
      | backend | convergence        |
      | graal   | 19.74727035156286 |
# TODO: Tracking is not lost immediately, so this scenario is not applicable
# Scenario: Re-detect the face when clmtrackr lost track of the eyes
#  TODO: Document.TrackingLostListener = () -> detectFace(webcam, webcam.getImage(), nashorn);
#Given a buffered image from file /lowQualityFaceImage.png
#  And a buffered image from file /faceWithoutChin.png
# When eye patches are requested twice
# Then provide non-null eye patches
#  And tracking is lost
#  And an attempt is made to find a new face box
#  And provide null eye patches
