Feature: Continuous calibration
  
  With required and probable fixation locations (RFLs, PFLs) that are collected
  during eye tracking, eye tracking can be validated continuously and be
  re-calibrated if validation results are too bad.
  
  TODO: Currently, re-calibration is always performed when new data is available,
  not only when validation is too bad.
  TODO: When re-calibration runs for a longer time, a subset of the collected
  data might be used. 

  # TODO: Enable again when huge resources can be obtained from other places
  # than the repository.
  #Scenario Outline: Continuous calibration
  #  
  #  Calibration is not started / repeated before a set threshold of the number of
  #  training / validation data has been obtained (again).
  #  TODO: Noch einen TestableEyePatchesDetector im Background konfigurieren
  #
  #  Given images and manual contexts from directory /<numberOfImages>largeImages
  #  And a default feature extractor from images
  #  And sample polynomial hyper-parameters
  #  When continuous calibration is performed from <numberOfImages> images with a threshold of <trainingThreshold> training and <validationThreshold> validation data
  #  Then calibration has been performed <calibrationRunCount> times
  # 
  #  Examples: 
  #    | numberOfImages | trainingThreshold | validationThreshold | calibrationRunCount |
  #    |             15 |                25 |                   2 |                   0 |
  #    |             15 |                10 |                   5 |                   1 |
  #    |             15 |                 5 |                   2 |                   2 |
