package de.monochromata.eyetracking.minviable;

import static org.assertj.core.api.Assertions.assertThat;

import de.monochromata.eyetracking.minviable.state.EyeContextSourceState;
import de.monochromata.eyetracking.minviable.state.FeatureExtractorState;
import de.monochromata.eyetracking.minviable.state.InputState;
import de.monochromata.eyetracking.webcam.stubs.NoopWebcam;
import io.cucumber.java.en.Given;

public class EyeContextSourceStepdefs {

    private final InputState eyePatchesState;
    private final EyeContextSourceState eyeContextSourceState;
    private final FeatureExtractorState featureExtractorState;

    public EyeContextSourceStepdefs(final InputState eyePatchesState, final EyeContextSourceState eyeContextSourceState,
            final FeatureExtractorState featureExtractorState) {
        this.eyePatchesState = eyePatchesState;
        this.eyeContextSourceState = eyeContextSourceState;
        this.featureExtractorState = featureExtractorState;
    }

    @Given("^a noop webcam$")
    public void noopWebcam() {
        eyeContextSourceState.setWebcam(new NoopWebcam());
    }

    @Given("{int} images are used for training")
    public void trainingSize(final Integer trainingSize) {
        eyeContextSourceState.setTrainingSize(trainingSize);
    }

    @Given("{int} images are used for validation")
    public void testSize(final Integer testSize) {
        eyeContextSourceState.setTestSize(testSize);
    }

    @Given("height interval {word}")
    public void heightInterval(final String heightInterval) {
        final String[] bounds = heightInterval.split("-");
        eyeContextSourceState.setEyePatchMinimumHeight(Integer.parseInt(bounds[0]));
        eyeContextSourceState.setEyePatchMaximumHeight(Integer.parseInt(bounds[1]));
    }

    @Given("padding {int}px")
    public void padding(final Integer paddingPx) {
        eyeContextSourceState.setPaddingPx(paddingPx);
    }

    @Given("a training threshold of {int}, i.e. {int} image is used for training and {int} image is used for validation")
    @Given("a training threshold of {int}, {int} image is used for training and {int} images are used for validation")
    @Given("a training threshold of {int}, {int} images are used for training and {int} image is used for validation")
    @Given("a training threshold of {int}, {int} images are used for training and {int} images are used for validation")
    public void configureSetSizes(final Integer trainingThreshold, final Integer numberOfTrainingImages,
            final Integer numberOfValidationImages) {
        assertThat(numberOfTrainingImages).isEqualTo(trainingThreshold);
        assertThat(numberOfValidationImages).isEqualTo(trainingThreshold);
        eyeContextSourceState.setTrainingSize(trainingThreshold);
        eyeContextSourceState.setTestSize(trainingThreshold);
    }

}
