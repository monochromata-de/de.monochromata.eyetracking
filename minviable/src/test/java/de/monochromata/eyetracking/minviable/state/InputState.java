package de.monochromata.eyetracking.minviable.state;

import java.util.List;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;

public class InputState {

	private String inputFilename;
	private List<EyePatches> eyePatches;
	private List<ClmtrackrResult> clmtrackrResults;
	private List<Point> manualPoints;

	public String getInputFilename() {
		return inputFilename;
	}

	public void setInputFilename(final String inputFilename) {
		this.inputFilename = inputFilename;
	}

	public List<EyePatches> getEyePatches() {
		return eyePatches;
	}

	public void setEyePatches(final List<EyePatches> eyePatches) {
		this.eyePatches = eyePatches;
	}

	public List<ClmtrackrResult> getClmtrackrResults() {
		return clmtrackrResults;
	}

	public void setClmtrackrResults(final List<ClmtrackrResult> clmtrackrResults) {
		this.clmtrackrResults = clmtrackrResults;
	}

	public List<Point> getManualPoints() {
		return manualPoints;
	}

	public void setManualPoints(final List<Point> manualPoints) {
		this.manualPoints = manualPoints;
	}

}
