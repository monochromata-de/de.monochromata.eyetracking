package de.monochromata.eyetracking.minviable.state;

import java.io.File;

public class FileState {

	private File imagesDirectory;
	private File indexFile;

	public File getImagesDirectory() {
		return imagesDirectory;
	}

	public void setImagesDirectory(final File imagesDirectory) {
		this.imagesDirectory = imagesDirectory;
	}

	public File getIndexFile() {
		return indexFile;
	}

	public void setIndexFile(File indexFile) {
		this.indexFile = indexFile;
	}

}
