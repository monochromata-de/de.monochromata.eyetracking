package de.monochromata.eyetracking.minviable;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;
import static java.util.Collections.nCopies;
import static java.util.regex.Pattern.compile;

import java.awt.geom.Point2D;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.monochromata.eyetracking.minviable.state.PositionsState;
import io.cucumber.java.en.Given;

public class PositionsStepdefs {

    private static final Pattern POSITION_PATTERN = compile(
            "^(?<index>\\d+)=\\((?<x>[\\.\\d]+),\\s?(?<y>[\\.\\d]+)\\)$");

    private final PositionsState positionsState;

    public PositionsStepdefs(final PositionsState positionsState) {
        this.positionsState = positionsState;
    }

    @Given("^positions$")
    public void positions(final String positionsString) {
        positionsState.setPositions(parsePositions(positionsString));
    }

    private List<Point2D> parsePositions(final String positionsString) {
        final List<Point2D> positions = new ArrayList<>(nCopies(71, new Point2D.Double(0.0d, 0.0d)));
        stream(positionsString.split("\n")).map(this::parsePosition)
                .forEach(entry -> positions.set(entry.getKey(), entry.getValue()));
        return positions;
    }

    private Entry<Integer, Point2D> parsePosition(final String positionString) {
        final Matcher matcher = POSITION_PATTERN.matcher(positionString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(
                    "Invalid string " + positionString + " does not match " + POSITION_PATTERN);
        }
        final int index = parseInt(matcher.group("index"));
        final double x = parseDouble(matcher.group("x"));
        final double y = parseDouble(matcher.group("y"));
        final Point2D point = new Point2D.Double(x, y);
        return new SimpleImmutableEntry<>(index, point);
    }
}
