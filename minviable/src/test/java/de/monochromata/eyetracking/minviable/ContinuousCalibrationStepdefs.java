package de.monochromata.eyetracking.minviable;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.minviable.state.FeatureExtractorState;
import de.monochromata.eyetracking.minviable.state.SvmState;
import de.monochromata.eyetracking.webcam.calibration.Calibration;
import de.monochromata.eyetracking.webcam.calibration.ContinuousCalibration;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ContinuousCalibrationStepdefs {

    private static final Pattern INPUT_IMAGE_FILE_NAME_PATTERN = Pattern
            .compile("(?<prefix>[^_]+)_(?<timestamp>\\d+)_(?<manualContextX>\\d+)_(?<manualContextY>\\d+).png");

    private final List<Pair<BufferedImage, Point>> inputData = new ArrayList<>();
    private final AtomicInteger validationCounter = new AtomicInteger();
    private final SvmState svmState;
    private final FeatureExtractorState featureExtractorState;

    public ContinuousCalibrationStepdefs(final SvmState svmState, final FeatureExtractorState featureExtractorState) {
        this.svmState = svmState;
        this.featureExtractorState = featureExtractorState;
    }

    @When("^continuous calibration is performed from ([^ ]+) images with a threshold of (\\d+) training and (\\d+) validation data$")
    public void calibrate(final Integer numberOfImages, final Integer trainingDataThreshold,
            final Integer validationDataThreshold) {
        final Calibration calibration = new Calibration(featureExtractorState.getFeatureExtractorFromEyePatches(),
                svmState.getHyperParameters());
        final ContinuousCalibration<BufferedImage> continuousCalibration = new ContinuousCalibration<>(Runnable::run,
                calibration, featureExtractorState.getFeatureExtractorFromImages(), trainingDataThreshold,
                validationDataThreshold);
        continuousCalibration.addValidationListener(unused -> validationCounter.incrementAndGet());

        inputData.forEach(entry -> continuousCalibration.addDatum(entry.getLeft(), entry.getRight().getX(),
                entry.getRight().getY()));
    }

    @Then("^calibration has been performed (\\d+) times$")
    public void assertCalibrationCount(final Integer expectedNumberOfCalibrations) {
        assertThat(validationCounter.get()).isEqualTo(expectedNumberOfCalibrations);
    }

    @Given("^images and manual contexts from directory ([^ ]+)$")
    public void loadImagesAndManualContexts(final String directoryName) {
        try {
            final URL directoryUrl = requireNonNull(getClass().getResource(directoryName),
                    "Directory not found on classpath: " + directoryName);
            loadImagesAndManualContexts(new File(directoryUrl.toURI()));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void loadImagesAndManualContexts(final File directory) throws IOException {
        stream(directory.listFiles()).sorted().forEach(this::loadImageAndManualContext);
    }

    private void loadImageAndManualContext(final File file) {
        try {
            final Matcher matcher = INPUT_IMAGE_FILE_NAME_PATTERN.matcher(file.getName());
            if (matcher.matches()) {
                final BufferedImage image = ImageIO.read(file);
                final int x = parseInt(matcher.group("manualContextX"));
                final int y = parseInt(matcher.group("manualContextY"));
                inputData.add(new ImmutablePair<>(image, new PointImpl(x, y)));
            }
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
