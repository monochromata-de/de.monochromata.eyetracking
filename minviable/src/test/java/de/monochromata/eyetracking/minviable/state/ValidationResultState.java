package de.monochromata.eyetracking.minviable.state;

import de.monochromata.eyetracking.webcam.calibration.ValidationResult;

public class ValidationResultState {

	private ValidationResult validationResult;

	public ValidationResult getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(final ValidationResult validationResult) {
		this.validationResult = validationResult;
	}
	
}
