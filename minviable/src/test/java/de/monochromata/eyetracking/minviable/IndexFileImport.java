package de.monochromata.eyetracking.minviable;

import static de.monochromata.eyetracking.minviable.io.index.IndexFileReading.getVersionedReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.io.index.VersionedIndexFileReader;

public interface IndexFileImport {

	boolean DO_NOT_IGNORE_FIRST_LINE = false;

	static Pair<List<EyePatches>, List<Point>> readEyePatchesAndManualContexts(final String filename) {
		return processResource(filename, reader -> {
			try {
				final Pair<List<EyePatches>, List<Point>> data = new ImmutablePair<>(new ArrayList<>(),
						new ArrayList<>());
				final VersionedIndexFileReader versionedReader = getVersionedReader(reader);
				processLinesInFile(reader, DO_NOT_IGNORE_FIRST_LINE,
						nextLine -> readEyePatchesAndManualContextsFromLine(data, nextLine, versionedReader));
				return data;
			} catch (final IOException ioe) {
				throw new UncheckedIOException(ioe);
			}
		});
	}

	static void readEyePatchesAndManualContextsFromLine(final Pair<List<EyePatches>, List<Point>> data,
			final String nextLine, final VersionedIndexFileReader versionedReader) {
		final String[] columns = nextLine.split("\\s+");
		addManualContext(data, columns, versionedReader);
		addEyePatches(data, columns, versionedReader);
	}

	static void addEyePatches(final Pair<List<EyePatches>, List<Point>> data, final String[] columns,
			final VersionedIndexFileReader versionedReader) {
		final EyePatches eyePatches = versionedReader.createEyePatches(columns);
		data.getLeft().add(eyePatches);
	}

	static void addManualContext(final Pair<List<EyePatches>, List<Point>> data, final String[] columns,
			final VersionedIndexFileReader versionedReader) {
		final Point point = versionedReader.getManualContextPoint(columns);
		data.getRight().add(point);
	}

	static <T> T processResource(final String filename, final Function<BufferedReader, T> fileConsumer) {

		final InputStream resource = IndexFileImport.class.getResourceAsStream(filename);
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
			return fileConsumer.apply(reader);
		} catch (final IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
	}

	static void processLinesInFile(final BufferedReader reader, final boolean ignoreFirstLine,
			final Consumer<String> lineProcessor) {
		try {
			if (ignoreFirstLine) {
				reader.readLine();
			}
			String nextLine = null;
			while ((nextLine = reader.readLine()) != null) {
				lineProcessor.accept(nextLine);
			}
		} catch (final IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
	}

}
