package de.monochromata.eyetracking.minviable;

import static de.monochromata.eyetracking.minviable.io.index.IndexFileIO.INPUT_IMAGE_FILE_NAME_PATTERN;
import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.minviable.io.ClmtrackrFileResult;
import de.monochromata.eyetracking.minviable.io.positions.PositionIndexReading;

public interface PositionIndexImport {

	static Pair<List<ClmtrackrResult>, List<Point>> readClmtrackrResultsAndManualContexts(final String filename) {
		final List<ClmtrackrResult> clmtrackrResults = PositionIndexReading.readResults(null, new File(filename))
				.collect(toList());
		final List<Point> manualContexts = createManualContexts(clmtrackrResults);
		return new ImmutablePair<>(clmtrackrResults, manualContexts);
	}

	static List<Point> createManualContexts(final List<ClmtrackrResult> clmtrackrResults) {
		return clmtrackrResults.stream().map(PositionIndexImport::createManualContext).collect(toList());
	}

	static Point createManualContext(final ClmtrackrResult result) {
		return createManualContext(((ClmtrackrFileResult) result).getImageFile().getName());
	}

	static Point createManualContext(final String imageFileName) {
		final Matcher matcher = INPUT_IMAGE_FILE_NAME_PATTERN.matcher(imageFileName);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Not an manual context image file name: " + imageFileName);
		}
		final int manualContextX = parseInt(matcher.group("manualContextX"));
		final int manualContextY = parseInt(matcher.group("manualContextY"));
		return new PointImpl(manualContextX, manualContextY);
	}

}
