package de.monochromata.eyetracking.minviable;

import static de.monochromata.eyetracking.minviable.IndexFileImport.readEyePatchesAndManualContexts;
import static de.monochromata.eyetracking.minviable.PositionIndexImport.readClmtrackrResultsAndManualContexts;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.state.InputState;
import io.cucumber.java.en.Given;

public class InputStepdefs {

    private final InputState eyePatchesAndManualContextState;

    public InputStepdefs(final InputState imageAndManualContextState) {
        eyePatchesAndManualContextState = imageAndManualContextState;
    }

    @Given("eye patches and manual contexts from {word}")
    public void eyePatchesAndManualContexts(final String filename) {
        final Pair<List<EyePatches>, List<Point>> data = readEyePatchesAndManualContexts(filename);
        eyePatchesAndManualContextState.setInputFilename(filename);
        eyePatchesAndManualContextState.setEyePatches(data.getLeft());
        eyePatchesAndManualContextState.setManualPoints(data.getRight());
    }

    @Given("clmtrackr results and manual contexts from {word}")
    public void clmtrackrResults(final String filename) {
        final Pair<List<ClmtrackrResult>, List<Point>> data = readClmtrackrResultsAndManualContexts(filename);
        eyePatchesAndManualContextState.setInputFilename(filename);
        eyePatchesAndManualContextState.setClmtrackrResults(data.getLeft());
        eyePatchesAndManualContextState.setManualPoints(data.getRight());
    }
}
