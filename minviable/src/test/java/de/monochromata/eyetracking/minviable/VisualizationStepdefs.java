package de.monochromata.eyetracking.minviable;

import static java.awt.Color.BLUE;
import static java.awt.Color.GREEN;
import static java.awt.Color.RED;
import static org.assertj.core.api.Assertions.assertThat;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.PointImpl;
import de.monochromata.eyetracking.minviable.state.FeatureExtractorState;
import de.monochromata.eyetracking.minviable.state.FileState;
import de.monochromata.eyetracking.minviable.state.ImageState;
import de.monochromata.eyetracking.minviable.state.VisualizerState;
import de.monochromata.eyetracking.minviable.stubs.TestableGraphics;
import de.monochromata.eyetracking.minviable.visualization.DatumVisualization;
import de.monochromata.eyetracking.minviable.visualization.FaceVisualizer;
import de.monochromata.eyetracking.minviable.visualization.GraphicsPainter;
import de.monochromata.eyetracking.minviable.visualization.ImageProcessingVisualizer;
import de.monochromata.eyetracking.minviable.visualization.Painter;
import de.monochromata.eyetracking.minviable.visualization.ScatterPlotVisualization;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class VisualizationStepdefs {

    private final JFrame frame = new JFrame();
    private final TestablePainter painter = new TestablePainter();
    private final List<Point> trainingData = new ArrayList<>();
    private final List<Point> testDataTruth = new ArrayList<>();
    private final List<Point> testDataPredictions = new ArrayList<>();

    private final ImageState imageState;
    private final VisualizerState visualizerState;
    private final FileState fileState;
    private final FeatureExtractorState featureExtractorState;

    private TestableGraphics graphics;
    private String imageFileName;
    private DatumVisualization visualization;

    public VisualizationStepdefs(final ImageState imageState, final VisualizerState visualizerState,
            final FileState fileState, final FeatureExtractorState featureExtractorState) {
        this.imageState = imageState;
        this.visualizerState = visualizerState;
        this.fileState = fileState;
        this.featureExtractorState = featureExtractorState;
    }

    @Given("^a FaceVisualizer$")
    public void faceVisualizer() {
        visualizerState.setVisualizer(new FaceVisualizer(frame, painter));
    }

    @Given("^an ImageProcessingVisualizer with a GraphicsPainter$")
    public void imageProcessingVisualizerWithGraphicsPainter() {
        visualizerState.setVisualizer(new ImageProcessingVisualizer(frame, new GraphicsPainter()));
    }

    @Given("^an ImageProcessingVisualizer$")
    public void imageProcessingVisualizer() {
        visualizerState.setVisualizer(new ImageProcessingVisualizer(frame, painter));
    }

    @Given("^three training data$")
    public void threeTrainingData() {
        trainingData.add(new PointImpl(200, 300));
        trainingData.add(new PointImpl(400, 600));
        trainingData.add(new PointImpl(600, 900));
    }

    @Given("^three test data truth$")
    public void threeTruths() {
        testDataTruth.add(new PointImpl(300, 300));
        testDataTruth.add(new PointImpl(500, 600));
        testDataTruth.add(new PointImpl(700, 900));
    }

    @Given("^three predictions from the test data$")
    public void threePredictions() {
        testDataPredictions.add(new PointImpl(300, 350));
        testDataPredictions.add(new PointImpl(500, 650));
        testDataPredictions.add(new PointImpl(700, 950));
    }

    @When("^the data is visualized$")
    public void visualizeData() {
        graphics = new TestableGraphics();
        new ScatterPlotVisualization(trainingData, testDataTruth, testDataPredictions).paintScatterPlot(graphics);
    }

    @When("^visualization is requested for line number (\\d+) and image (.*)$")
    public void requestVisualization(final Integer lineNumber, final String imageFileName) {
        this.imageFileName = imageFileName;
        visualization = new DatumVisualization(fileState.getImagesDirectory(),
                featureExtractorState.getFeatureExtractorFromImages());
        visualization.setDatumToShow(imageFileName);
    }

    @Then("^the buffered image is drawn onto the given frame$")
    public void assertBufferedImage() {
        assertImageIconWithImageExists(frame, imageState.getNextImageInThenPhase());
    }

    @Then("^the bounds of the detected eyes are drawn on the buffered image in the frame$")
    public void assertDetectedBounds() {
        assertThat(painter.leftEyeBounds).isEqualTo(new Rectangle(161, 105, 16, 5));
        assertThat(painter.rightEyeBounds).isEqualTo(new Rectangle(201, 104, 16, 5));
    }

    @Then("^the detected face is drawn on the buffered image in the frame$")
    public void assertDetectedFace() {
        assertThat(painter.positions).containsExactly(new Point2D.Double(146, 107), new Point2D.Double(145, 121),
                new Point2D.Double(147, 136), new Point2D.Double(150, 150), new Point2D.Double(157, 161),
                new Point2D.Double(166, 171), new Point2D.Double(178, 178), new Point2D.Double(190, 180),
                new Point2D.Double(203, 178), new Point2D.Double(214, 171), new Point2D.Double(223, 161),
                new Point2D.Double(229, 149), new Point2D.Double(232, 134), new Point2D.Double(234, 120),
                new Point2D.Double(232, 106), new Point2D.Double(223, 98), new Point2D.Double(216, 95),
                new Point2D.Double(206, 96), new Point2D.Double(198, 98), new Point2D.Double(156, 99),
                new Point2D.Double(162, 95), new Point2D.Double(172, 96), new Point2D.Double(180, 98),
                new Point2D.Double(161, 109), new Point2D.Double(169, 105), new Point2D.Double(177, 108),
                new Point2D.Double(169, 111), new Point2D.Double(169, 107), new Point2D.Double(217, 108),
                new Point2D.Double(209, 104), new Point2D.Double(201, 108), new Point2D.Double(209, 110),
                new Point2D.Double(209, 107), new Point2D.Double(189, 105), new Point2D.Double(181, 123),
                new Point2D.Double(177, 129), new Point2D.Double(181, 134), new Point2D.Double(190, 135),
                new Point2D.Double(200, 134), new Point2D.Double(203, 129), new Point2D.Double(199, 123),
                new Point2D.Double(190, 117), new Point2D.Double(184, 131), new Point2D.Double(197, 131),
                new Point2D.Double(173, 149), new Point2D.Double(180, 146), new Point2D.Double(186, 144),
                new Point2D.Double(190, 145), new Point2D.Double(194, 144), new Point2D.Double(200, 145),
                new Point2D.Double(206, 149), new Point2D.Double(202, 153), new Point2D.Double(197, 156),
                new Point2D.Double(190, 157), new Point2D.Double(183, 156), new Point2D.Double(177, 153),
                new Point2D.Double(182, 150), new Point2D.Double(190, 151), new Point2D.Double(198, 150),
                new Point2D.Double(198, 148), new Point2D.Double(190, 148), new Point2D.Double(182, 148),
                new Point2D.Double(190, 128), new Point2D.Double(164, 106), new Point2D.Double(174, 106),
                new Point2D.Double(173, 110), new Point2D.Double(165, 110), new Point2D.Double(213, 105),
                new Point2D.Double(204, 105), new Point2D.Double(204, 109), new Point2D.Double(213, 110));
    }

    @Then("^the bounds of the clmtrackr patches are drawn on a canvas in the frame$")
    public void assertBoundsOfClmtrackrPatches() {
        assertThat(painter.rectangles).containsExactly(new Rectangle(0, 0, 20, 22), new Rectangle(15, 24, 21, 21),
                new Rectangle(14, 35, 21, 21), new Rectangle(15, 46, 21, 21), new Rectangle(18, 56, 21, 21),
                new Rectangle(23, 65, 21, 21), new Rectangle(30, 72, 21, 21), new Rectangle(38, 77, 21, 21),
                new Rectangle(47, 79, 21, 21), new Rectangle(57, 77, 21, 21), new Rectangle(64, 71, 21, 21),
                new Rectangle(71, 64, 21, 21), new Rectangle(75, 55, 21, 21), new Rectangle(78, 44, 21, 21),
                new Rectangle(79, 34, 21, 21), new Rectangle(78, 23, 21, 21), new Rectangle(71, 17, 21, 21),
                new Rectangle(66, 15, 21, 21), new Rectangle(59, 15, 21, 21), new Rectangle(53, 17, 21, 21),
                new Rectangle(21, 18, 21, 21), new Rectangle(26, 15, 21, 21), new Rectangle(33, 16, 21, 21),
                new Rectangle(39, 17, 21, 21), new Rectangle(25, 25, 21, 21), new Rectangle(31, 22, 21, 21),
                new Rectangle(38, 25, 21, 21), new Rectangle(31, 27, 21, 21), new Rectangle(31, 24, 21, 21),
                new Rectangle(67, 24, 21, 21), new Rectangle(61, 21, 21, 21), new Rectangle(55, 25, 21, 21),
                new Rectangle(61, 26, 21, 21), new Rectangle(61, 24, 21, 21), new Rectangle(46, 23, 21, 21),
                new Rectangle(40, 36, 21, 21), new Rectangle(37, 41, 21, 21), new Rectangle(40, 45, 21, 21),
                new Rectangle(47, 46, 21, 21), new Rectangle(54, 44, 21, 21), new Rectangle(56, 41, 21, 21),
                new Rectangle(53, 36, 21, 21), new Rectangle(46, 32, 21, 21), new Rectangle(42, 43, 21, 21),
                new Rectangle(51, 43, 21, 21), new Rectangle(35, 56, 21, 21), new Rectangle(39, 53, 21, 21),
                new Rectangle(44, 52, 21, 21), new Rectangle(47, 52, 21, 21), new Rectangle(50, 52, 21, 21),
                new Rectangle(55, 53, 21, 21), new Rectangle(59, 55, 21, 21), new Rectangle(56, 59, 21, 21),
                new Rectangle(52, 62, 21, 21), new Rectangle(47, 63, 21, 21), new Rectangle(42, 62, 21, 21),
                new Rectangle(38, 60, 21, 21), new Rectangle(41, 58, 21, 21), new Rectangle(47, 59, 21, 21),
                new Rectangle(53, 58, 21, 21), new Rectangle(53, 55, 21, 21), new Rectangle(47, 55, 21, 21),
                new Rectangle(41, 55, 21, 21), new Rectangle(46, 40, 21, 21), new Rectangle(28, 23, 21, 21),
                new Rectangle(35, 23, 21, 21), new Rectangle(35, 26, 21, 21), new Rectangle(28, 26, 21, 21),
                new Rectangle(64, 22, 21, 21), new Rectangle(57, 22, 21, 21), new Rectangle(58, 25, 21, 21),
                new Rectangle(64, 25, 21, 21));
    }

    @Then("^all canvases are shown in the frame$")
    public void assertCanvases() {
        final long canvasCount = Arrays.stream(frame.getContentPane().getComponents())
                .filter(child -> child instanceof JLabel && ((JLabel) child).getIcon() != null).count();
        assertThat(canvasCount).isEqualTo(8);
    }

    @Then("^the frame is closed$")
    public void assertFrameClosed() {
        frame.dispose();
        assertThat(frame.isShowing()).isFalse();
    }

    @Then("^training data is painted in blue$")
    public void assertTrainingColor() {
        assertThat(graphics.getDrawnObjects().subList(0, 3)).containsExactly(
                new Object[] { "fillOval", 200 - 3, 300 - 3, 6, 6, BLUE },
                new Object[] { "fillOval", 400 - 3, 600 - 3, 6, 6, BLUE },
                new Object[] { "fillOval", 600 - 3, 900 - 3, 6, 6, BLUE });
    }

    @Then("^test data truth is painted in green$")
    public void assertTruthColor() {
        assertThat(graphics.getDrawnObjects().subList(3, 6)).containsExactly(
                new Object[] { "fillOval", 300 - 3, 300 - 3, 6, 6, GREEN },
                new Object[] { "fillOval", 500 - 3, 600 - 3, 6, 6, GREEN },
                new Object[] { "fillOval", 700 - 3, 900 - 3, 6, 6, GREEN });
    }

    @Then("^test data predictions are painted in red with$")
    public void assertPredictionsColor() {
        assertThat(graphics.getDrawnObjects().subList(6, 9)).containsExactly(
                new Object[] { "fillOval", 300 - 3, 350 - 3, 6, 6, RED },
                new Object[] { "fillOval", 500 - 3, 650 - 3, 6, 6, RED },
                new Object[] { "fillOval", 700 - 3, 950 - 3, 6, 6, RED });
    }

    @Then("^test data truth and predictions are connected by lines to visualize prediction error$")
    public void assertConnections() {
        assertThat(graphics.getDrawnObjects().subList(9, 12)).containsExactly(
                new Object[] { "drawLine", 300, 300, 300, 350, RED },
                new Object[] { "drawLine", 500, 600, 500, 650, RED },
                new Object[] { "drawLine", 700, 900, 700, 950, RED });
    }

    @Then("^the webcam shot is added to the panel at (\\d+)x(\\d+)px$")
    public void assertWebcam(final Integer arg1, final Integer arg2) {
        assertThat(((JLabel) visualization.getComponent(0)).getText()).isEqualTo(imageFileName + " (320x240px)");
        assertThat(((ImageIcon) ((JLabel) visualization.getComponent(1)).getIcon()).getImage())
                .isInstanceOf(BufferedImage.class);
    }

    @Then("^the grayscale version of the webcam shot is added to the panel at (\\d+)x(\\d+)px$")
    public void assertGrayscale(final Integer arg1, final Integer arg2) {
        assertThat(((JLabel) visualization.getComponent(2)).getText()).isEqualTo(imageFileName + " gray (320x240px)");
        assertThat(((ImageIcon) ((JLabel) visualization.getComponent(1)).getIcon()).getImage())
                .isInstanceOf(BufferedImage.class);
    }

    @Then("^the eye patch is added to the panel, scaled to (\\d+)x(\\d+)px$")
    public void assertPatch(final Integer width, final Integer height) {
        assertThat(((JLabel) visualization.getComponent(4)).getText())
                .isEqualTo("Left eye patch (" + width / 10 + "x" + height / 10 + "px)*10");
        assertThat(((ImageIcon) ((JLabel) visualization.getComponent(3)).getIcon()).getImage())
                .isInstanceOf(Image.class);
    }

    @Then("^the scaled eye patch is added to the panel, scaled to (\\d+)x(\\d+)px$")
    public void assertScaledPatch(final Integer width, final Integer height) {
        assertThat(((JLabel) visualization.getComponent(6)).getText())
                .isEqualTo("Left scaled eye patch (" + width / 10 + "x" + height / 10 + "px)*10");
        assertThat(((ImageIcon) ((JLabel) visualization.getComponent(5)).getIcon()).getImage())
                .isInstanceOf(Image.class);
    }

    @Then("^the eye patch with equalized histogram is added to the panel, scaled to (\\d+)x(\\d+)px$")
    public void assertEqualizedPatch(final Integer width, final Integer height) {
        assertThat(((JLabel) visualization.getComponent(8)).getText())
                .isEqualTo("Left eye patch (eq. hist.) (" + width / 10 + "x" + height / 10 + "px)*10");
        assertThat(((ImageIcon) ((JLabel) visualization.getComponent(7)).getIcon()).getImage())
                .isInstanceOf(Image.class);
    }

    private void assertImageIconWithImageExists(final JFrame frame, final BufferedImage image) {
        final boolean imageFound = imageIconWithImageExists(frame, image);
        assertThat(imageFound).isTrue();
    }

    private boolean imageIconWithImageExists(final Container container, final BufferedImage image) {
        for (final Component component : container.getComponents()) {
            if (isImageIconWith(component, image)) {
                return true;
            } else if (component instanceof Container && imageIconWithImageExists((Container) component, image)) {
                return true;
            }
        }
        return false;
    }

    private boolean isImageIconWith(final Component component, final BufferedImage image) {
        return component instanceof JLabel && ((JLabel) component).getIcon() != null
                && ((JLabel) component).getIcon() instanceof ImageIcon
                && ((ImageIcon) ((JLabel) component).getIcon()).getImage() == image;
    }

    private static class TestablePainter implements Painter {

        private List<Point2D> positions;
        private Rectangle leftEyeBounds;
        private Rectangle rightEyeBounds;
        private final List<Rectangle> rectangles = new ArrayList<>();

        @Override
        public void drawFace(final BufferedImage image, final List<Point2D> positions) {
            this.positions = positions;
        }

        @Override
        public void drawEyeBounds(final BufferedImage image, final Rectangle leftEyeBounds,
                final Rectangle rightEyeBounds) {
            this.leftEyeBounds = leftEyeBounds;
            this.rightEyeBounds = rightEyeBounds;
        }

        @Override
        public void drawRect(final Graphics2D graphics, final Rectangle rectangle) {
            rectangles.add(rectangle);
        }

    }

}
