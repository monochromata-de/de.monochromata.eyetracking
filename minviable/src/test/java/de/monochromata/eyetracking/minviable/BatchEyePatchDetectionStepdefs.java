package de.monochromata.eyetracking.minviable;

import static de.monochromata.eyetracking.minviable.io.index.IOExceptionConversion.convertIOExceptionToUnchecked;
import static java.util.Arrays.stream;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileWriter;

import de.monochromata.eyetracking.minviable.state.BackendState;
import de.monochromata.eyetracking.minviable.tools.HighQualityPositionsDetector;
import de.monochromata.eyetracking.minviable.tools.PositionsToEyePatches;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BatchEyePatchDetectionStepdefs {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private final BackendState backendState;
    private File inputDirectory;
    private File outputDirectory;
    private int padding;
    private File positionsFile;

    public BatchEyePatchDetectionStepdefs(final BackendState backendState) {
        this.backendState = backendState;
    }

    @Given("input directory {word}")
    public void inputDir(final String inputDirName) {
        inputDirectory = new File(inputDirName);
    }

    @Given("the input directory contains the file {word}")
    public void inputDirWithFile(final String fileInInputDir) {
        final File file = new File(inputDirectory, fileInInputDir);
        assertThat(file).exists();
    }

    @Given("positions file {word} in the output directory containing")
    public void positionsFile(final String positionsFileName, final String contents) {
        positionsFile = new File(outputDirectory, positionsFileName);
        createFile(positionsFile, contents);
    }

    @Given("output directory {word}")
    public void outputDir(final String outputDirName) {
        outputDirectory = new File(outputDirName);
        outputDirectory.mkdirs();
    }

    @Given("padding {int}")
    public void padding(final Integer padding) {
        this.padding = padding;
    }

    @When("^positions are detected$")
    public void detectPositions() {
        new HighQualityPositionsDetector(backendState.getBackend(), inputDirectory, outputDirectory).runBatch();
    }

    @When("^positions are converted to eye patches$")
    public void convertPositionsToEyePatches() {
        new PositionsToEyePatches(inputDirectory, positionsFile).runBatch();
    }

    @Then("the output directory contains the file {word}")
    public void assertIndexFileExists(final String nameOfIndexFile) {
        assertThat(new File(outputDirectory, nameOfIndexFile)).exists();
    }

    @Then("the index file {word} contains the lines")
    public void assertIndexFileContents(final String nameOfIndexFile, final String expectedLines) {
        final File indexFile = new File(outputDirectory, nameOfIndexFile);
        assertThat(indexFile).hasContent(expectedLines);
    }

    private void createFile(final File file, final String contents) {
        convertIOExceptionToUnchecked(() -> {
            file.createNewFile();
            writeToFile(file, contents);
        });
    }

    private void writeToFile(final File file, final String contents) {
        convertIOExceptionToUnchecked(() -> {
            final FileWriter writer = new FileWriter(file);
            writer.write(contents);
            writer.close();
        });
    }

    @After
    public void deleteOutputDirectoryIfPresent() {
        if (outputDirectory != null) {
            deleteOutputDirectory();
        }
    }

    private void deleteOutputDirectory() {
        deleteCsvFilesInOutputDirectory();
        outputDirectory.delete();
    }

    private void deleteCsvFilesInOutputDirectory() {
        final File[] filesInOutput = outputDirectory.listFiles(file -> file.getName().endsWith(".csv"));
        stream(filesInOutput).forEach(File::delete);
    }

}
