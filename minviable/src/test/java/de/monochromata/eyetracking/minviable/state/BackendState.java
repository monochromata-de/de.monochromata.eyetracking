package de.monochromata.eyetracking.minviable.state;

import de.monochromata.clmtrackr4j.ClmtrackrBackend;

public class BackendState {

	private ClmtrackrBackend backend;

	public ClmtrackrBackend getBackend() {
		return backend;
	}

	public void setBackend(ClmtrackrBackend backend) {
		this.backend = backend;
	}

}
