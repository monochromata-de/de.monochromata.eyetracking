package de.monochromata.eyetracking.minviable.state;

import java.awt.image.BufferedImage;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;

public class FeatureExtractorState {

	private FeatureExtractor<BufferedImage> featureExtractorFromImages;
	private FeatureExtractor<ClmtrackrResult> featureExtractorFromClmtrackrResult;
	private FeatureExtractor<EyePatches> featureExtractorFromEyePatches;

	public FeatureExtractor<BufferedImage> getFeatureExtractorFromImages() {
		return featureExtractorFromImages;
	}

	public void setFeatureExtractorFromImages(final FeatureExtractor<BufferedImage> featureExtractor) {
		featureExtractorFromImages = featureExtractor;
	}

	public FeatureExtractor<ClmtrackrResult> getFeatureExtractorFromClmtrackrResult() {
		return featureExtractorFromClmtrackrResult;
	}

	public void setFeatureExtractorFromClmtrackrResult(
			final FeatureExtractor<ClmtrackrResult> featureExtractorFromClmtrackrResult) {
		this.featureExtractorFromClmtrackrResult = featureExtractorFromClmtrackrResult;
	}

	public FeatureExtractor<EyePatches> getFeatureExtractorFromEyePatches() {
		return featureExtractorFromEyePatches;
	}

	public void setFeatureExtractorFromEyePatches(final FeatureExtractor<EyePatches> featureExtractor) {
		featureExtractorFromEyePatches = featureExtractor;
	}

}
