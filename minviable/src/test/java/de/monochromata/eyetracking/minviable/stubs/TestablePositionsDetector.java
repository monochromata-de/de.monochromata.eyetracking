package de.monochromata.eyetracking.minviable.stubs;

import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import de.monochromata.clmtrackr4j.ClmtrackrResult;

public class TestablePositionsDetector implements Function<BufferedImage, ClmtrackrResult> {

	private final Iterator<ClmtrackrResult> results;

	public TestablePositionsDetector(final List<ClmtrackrResult> results) {
		this.results = results.iterator();
	}

	@Override
	public ClmtrackrResult apply(final BufferedImage image) {
		return results.next();
	}

}
