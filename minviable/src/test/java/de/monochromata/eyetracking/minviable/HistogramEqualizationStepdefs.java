package de.monochromata.eyetracking.minviable;

import de.monochromata.eyetracking.minviable.state.FeaturesState;
import io.cucumber.java.en.When;

public class HistogramEqualizationStepdefs {

    private final FeaturesState features;

    public HistogramEqualizationStepdefs(final FeaturesState features) {
        this.features = features;
    }

    @When("^histogram equalization has been applied to each scaled eye data set$")
    public void assertApplied() {
        // TODO: Assert the qualitative properties of an image with an equalized
        // histogram
        // for each eye image
        features.getFeatures();
    }

}
