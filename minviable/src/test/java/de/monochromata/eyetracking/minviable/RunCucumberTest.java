package de.monochromata.eyetracking.minviable;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;

/**
 * A wrapper to run Cucumber tests with JUnit.
 */
@RunWith(Cucumber.class)
public class RunCucumberTest {

}
