package de.monochromata.eyetracking.minviable.state;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ImageState {

	private final List<BufferedImage> images = new ArrayList<>();
	private AtomicReference<Iterator<BufferedImage>> imagesIteratorInWhenPhase = new AtomicReference<>();
	private AtomicReference<Iterator<BufferedImage>> imagesIteratorInThenPhase = new AtomicReference<>();

	public BufferedImage getNextImageInWhenPhase() {
		return getNextImage(imagesIteratorInWhenPhase);
	}
	
	public BufferedImage getNextImageInThenPhase() {
		return getNextImage(imagesIteratorInThenPhase);
	}
	
	private BufferedImage getNextImage(final AtomicReference<Iterator<BufferedImage>> iteratorReference) {
		if(iteratorReference.get() == null) {
			iteratorReference.set(images.iterator());
		}
		return iteratorReference.get().next();
	}

	public void addImage(final BufferedImage image) {
		images.add(image);
	}
	
}
