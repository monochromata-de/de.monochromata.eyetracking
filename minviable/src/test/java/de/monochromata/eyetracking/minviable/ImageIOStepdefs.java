package de.monochromata.eyetracking.minviable;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UncheckedIOException;

import javax.imageio.ImageIO;

import de.monochromata.eyetracking.minviable.state.ImageState;
import io.cucumber.java.en.Given;

public class ImageIOStepdefs {

    private final ImageState imageState;

    public ImageIOStepdefs(final ImageState imageState) {
        this.imageState = imageState;
    }

    @Given("a buffered image from file {word}")
    public void image(final String imageFileName) {
        final BufferedImage image = readImageFile(imageFileName);
        imageState.addImage(image);
    }

    @Given("a buffered image from file {word} {int} times")
    public void repeatedImage(final String imageFileName, final Integer count) {
        final BufferedImage image = readImageFile(imageFileName);
        for (int i = 0; i < count; i++) {
            imageState.addImage(image);
        }
    }

    private BufferedImage readImageFile(final String imageFileName) {
        try {
            return ImageIO.read(this.getClass().getResourceAsStream(imageFileName));
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
