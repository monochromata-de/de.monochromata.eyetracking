package de.monochromata.eyetracking.minviable;

import static de.monochromata.clmtrackr4j.ClmtrackrBackend.createBackend;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.withPrecision;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.DoubleStream;

import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.clmtrackr4j.js.Document;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatchDetector;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.state.BackendState;
import de.monochromata.eyetracking.minviable.state.ImageState;
import de.monochromata.eyetracking.minviable.state.VisualizerState;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class EyePatchDetectionStepdefs {

    private final ImageState imageState;
    private final VisualizerState visualizerState;
    private final BackendState backendState;
    private final Runnable trackingLostListener = () -> trackingLost = true;
    private boolean highQuality = false;
    private boolean trackingLost = false;
    private final List<EyePatches> eyePatches = new ArrayList<>();
    private Iterator<EyePatches> eyePatchesIterator;
    private EyePatches currentEyePatches;
    private long detectionDurationMs;

    public EyePatchDetectionStepdefs(final ImageState imageState, final VisualizerState visualizerState,
            final BackendState backendState) {
        this.imageState = imageState;
        this.visualizerState = visualizerState;
        this.backendState = backendState;
    }

    @Given("^requesting high-quality eye patches$")
    public void highQuality() {
        highQuality = true;
    }

    @Given("a {word} backend")
    public void backend(final String backendType) {
        backendState.setBackend(createBackend(backendType));
    }

    @When("^eye patches are requested$")
    public void requestPatches() {
        Document.addTrackingLostListener(trackingLostListener);
        final EyePatchDetector detector = createEyePatchDetector();
        final long startMs = System.currentTimeMillis();
        requestEyePatches(detector);
        final long endMs = System.currentTimeMillis();
        detectionDurationMs = endMs - startMs;
    }

    @When("eye patches are requested {int} times")
    public void requestNPatches(final Integer count) {
        Document.addTrackingLostListener(trackingLostListener);
        final EyePatchDetector detector = createEyePatchDetector();
        for (int i = 0; i < count; i++) {
            requestEyePatches(detector);
        }
    }

    @Then("provide non-null eye patches {int} times")
    public void assertNonNullEyePatchesNTimes(final Integer count) {
        for (int i = 0; i < count; i++) {
            getAndAssertNextEyePatchesAsCurrentOnes();
        }
    }

    @Then("^provide null eye patches$")
    public void assertNullPatches() {
        currentEyePatches = getNextEyePatches();
        assertThat(currentEyePatches).isNull();
    }

    @Then("left eye is at x={int}, y={int}, width={int}, height={int}")
    public void assertLeftBounds(final Integer x, final Integer y, final Integer width, final Integer height) {
        assertEyeBounds(currentEyePatches.getLeftPatch(), x, y, width, height);
    }

    @Then("left eye has data {word}")
    public void assertLeftData(final String filename) {
        assertEyeData(currentEyePatches.getLeftPatch(), filename);
    }

    @Then("right eye is at x={int}, y={int}, width={int}, height={int}")
    public void assertRightBounds(final Integer x, final Integer y, final Integer width, final Integer height) {
        assertEyeBounds(currentEyePatches.getRightPatch(), x, y, width, height);
    }

    @Then("right eye has data {word}")
    public void assertRightData(final String filename) {
        assertEyeData(currentEyePatches.getRightPatch(), filename);
    }

    @Then("iterations={int}")
    public void assertIterations(final Integer iterations) {
        assertThat(currentEyePatches.getIterations()).isEqualTo(iterations);
    }

    @Then("score={double}")
    public void assertScore(final Double score) {
        assertThat(currentEyePatches.getScore()).isEqualTo(score);
    }

    @Then("convergence={double}")
    public void assertConvergence(final Double convergence) {
        assertThat(currentEyePatches.getConvergence()).isEqualTo(convergence);
    }

    @Then("^tracking is lost$")
    public void assertTrackingLost() {
        assertThat(trackingLost).isTrue();
    }

    @Then("detection took less than {int}ms")
    public void assertDetectionDuration(final Long expectedDurationMs) {
        assertThat(detectionDurationMs).isEqualTo(expectedDurationMs);
    }

    @Then("^provide non-null eye patches$")
    public void getAndAssertNextEyePatchesAsCurrentOnes() {
        currentEyePatches = getNextEyePatches();
        assertThat(currentEyePatches).isNotNull();
    }

    @After
    public void after() {
        Document.removeTrackingLostListener(trackingLostListener);
    }

    private void assertEyeBounds(final EyePatch eyePatch, final int x, final int y, final int width, final int height) {
        final String actualBounds = eyePatch.getX() + " " + eyePatch.getY() + " " + eyePatch.getWidth() + " "
                + eyePatch.getHeight();
        final String expectedBounds = x + " " + y + " " + width + " " + height;
        assertThat(actualBounds).isEqualTo(expectedBounds);
    }

    private void assertEyeData(final EyePatch eyePatch, final String dataFilename) {
        final double[] expectedData = getData(dataFilename);
        assertThat(eyePatch.getData())
                .describedAs(
                        "expecting eye patch " + asList(eyePatch.getData()) + " to contain " + asList(expectedData))
                .containsExactly(expectedData, withPrecision(0.01));
    }

    private List<Double> asList(final double[] data) {
        return DoubleStream.of(data).boxed().collect(toList());
    }

    private double[] getData(final String dataFilename) {
        final InputStream resource = this.getClass().getResourceAsStream(dataFilename);
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
            return stream(readFileIntoString(reader).split("\\s+")).filter(string -> !string.isEmpty())
                    .mapToDouble(Double::parseDouble).toArray();

        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    private String readFileIntoString(final BufferedReader reader) throws IOException {
        final StringBuilder stringBuilder = new StringBuilder();
        String nextLine = null;
        while ((nextLine = reader.readLine()) != null) {
            stringBuilder.append(nextLine + "\n");
        }
        final String allData = stringBuilder.toString();
        return allData;
    }

    private void requestEyePatches(final EyePatchDetector detector) {
        eyePatches.add(detector.apply(imageState.getNextImageInWhenPhase()));
    }

    private EyePatchDetector createEyePatchDetector() {
        final ClmtrackrPositionsDetector positionsDetector = createPositionsDetector();
        return new EyePatchDetector(positionsDetector, BoundsFunctions::getEyeBoundsFromEyeCorners);
    }

    private ClmtrackrPositionsDetector createPositionsDetector() {
        if (visualizerState.getVisualizer() == null) {
            return new ClmtrackrPositionsDetector(backendState.getBackend(), highQuality);
        }
        return new ClmtrackrPositionsDetector(backendState.getBackend(), visualizerState.getVisualizer(), highQuality);
    }

    private EyePatches getNextEyePatches() {
        if (eyePatchesIterator == null) {
            eyePatchesIterator = eyePatches.iterator();
        }
        return eyePatchesIterator.next();
    }

}
