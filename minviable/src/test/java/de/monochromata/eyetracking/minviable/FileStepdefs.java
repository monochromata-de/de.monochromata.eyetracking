package de.monochromata.eyetracking.minviable;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;

import de.monochromata.eyetracking.minviable.state.FileState;
import io.cucumber.java.en.Given;

public class FileStepdefs {

    private final FileState fileState;

    public FileStepdefs(final FileState fileState) {
        this.fileState = fileState;
    }

    @Given("^directory (.*) with image files$")
    public void imageDir(final String path) {
        final File directory = new File(path);
        assertThat(directory).exists().isDirectory().canRead();
        fileState.setImagesDirectory(directory);
    }

    @Given("^index file (.*)$")
    public void indexFile(final String indexFileName) {
        final File indexFile = new File(indexFileName);
        assertThat(indexFile).exists().isFile().canRead();
        fileState.setIndexFile(indexFile);
    }

}
