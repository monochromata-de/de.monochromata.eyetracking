package de.monochromata.eyetracking.minviable;

import static org.assertj.core.api.Assertions.assertThat;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import de.monochromata.clmtrackr4j.ClmtrackrPositionsDetector;
import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatchDetector;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.features.BinocularFeatureExtractor;
import de.monochromata.eyetracking.minviable.features.FaceModelParametersFeatureExtractor;
import de.monochromata.eyetracking.minviable.features.PositionsFeatureExtractor;
import de.monochromata.eyetracking.minviable.features.WebgazerFeatureExtractor;
import de.monochromata.eyetracking.minviable.state.BackendState;
import de.monochromata.eyetracking.minviable.state.EyeContextSourceState;
import de.monochromata.eyetracking.minviable.state.FeatureExtractorState;
import de.monochromata.eyetracking.minviable.state.FeaturesState;
import de.monochromata.eyetracking.minviable.state.InputState;
import de.monochromata.eyetracking.webcam.features.CombinedFeatureExtractor;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FeatureExtractionStepdefs {

    private final BackendState backendState;
    private final InputState inputState;
    private final EyeContextSourceState eyeContextSourceState;
    private final FeatureExtractorState featureExtractorState;
    private final FeaturesState featuresState;

    public FeatureExtractionStepdefs(final BackendState backendState, final InputState inputState,
            final EyeContextSourceState eyeContextSourceState, final FeatureExtractorState featureExtractorState,
            final FeaturesState featuresState) {
        this.backendState = backendState;
        this.inputState = inputState;
        this.eyeContextSourceState = eyeContextSourceState;
        this.featureExtractorState = featureExtractorState;
        this.featuresState = featuresState;
    }

    @Given("^a default feature extractor from images$")
    public void defaultFromImagesExtractor() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final EyePatchDetector eyePatchDetector = createEyePatchDetector();
        final FeatureExtractor<BufferedImage> featureExtractor = BinocularFeatureExtractor.ofImages(eyePatchDetector,
                paddingPx);
        featureExtractorState.setFeatureExtractorFromImages(featureExtractor);
    }

    @Given("^a default feature extractor from eye patches$")
    public void defaultFromPatchesExtractor() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final FeatureExtractor<EyePatches> featureExtractor = BinocularFeatureExtractor.ofEyePatches(paddingPx);
        featureExtractorState.setFeatureExtractorFromEyePatches(featureExtractor);
    }

    @Given("^a positions feature extractor")
    public void positionsExtractor() {
        final FeatureExtractor<ClmtrackrResult> featureExtractor = new PositionsFeatureExtractor();
        featureExtractorState.setFeatureExtractorFromClmtrackrResult(featureExtractor);
    }

    @Given("^a face model parameters feature extractor")
    public void faceExtractor() {
        final FeatureExtractor<ClmtrackrResult> featureExtractor = new FaceModelParametersFeatureExtractor();
        featureExtractorState.setFeatureExtractorFromClmtrackrResult(featureExtractor);
    }

    @Given("^a positions and face model parameters feature extractor")
    public void positionsFaceExtractor() {
        final FeatureExtractor<ClmtrackrResult> featureExtractor = new CombinedFeatureExtractor<>(
                new PositionsFeatureExtractor(), new FaceModelParametersFeatureExtractor());
        featureExtractorState.setFeatureExtractorFromClmtrackrResult(featureExtractor);
    }

    @Given("^a default and face model parameters feature extractor$")
    public void defaultFaceExtractor() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final FeatureExtractor<ClmtrackrResult> featureExtractor = new CombinedFeatureExtractor<>(
                BinocularFeatureExtractor.ofPositions(createMockEyePatchesExtractor(),
                        BoundsFunctions::getEyeBoundsFromPupilPosition, paddingPx),
                new FaceModelParametersFeatureExtractor());
        featureExtractorState.setFeatureExtractorFromClmtrackrResult(featureExtractor);
    }

    @Given("^a default and positions feature extractor$")
    public void defaultPositionsExtractor() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final FeatureExtractor<ClmtrackrResult> featureExtractor = new CombinedFeatureExtractor<>(
                BinocularFeatureExtractor.ofPositions(createMockEyePatchesExtractor(),
                        BoundsFunctions::getEyeBoundsFromPupilPosition, paddingPx),
                new PositionsFeatureExtractor());
        featureExtractorState.setFeatureExtractorFromClmtrackrResult(featureExtractor);
    }

    @Given("^a default, positions, and face model parameters feature extractor$")
    public void defaultPositionsFaceExtractor() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final FeatureExtractor<ClmtrackrResult> featureExtractor = new CombinedFeatureExtractor<>(
                BinocularFeatureExtractor.ofPositions(createMockEyePatchesExtractor(),
                        BoundsFunctions::getEyeBoundsFromPupilPosition, paddingPx),
                new PositionsFeatureExtractor(), new FaceModelParametersFeatureExtractor());
        featureExtractorState.setFeatureExtractorFromClmtrackrResult(featureExtractor);
    }

    @Given("^a monocular feature extractor from images$")
    public void extractorFromImages() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final EyePatchDetector eyePatchDetector = createEyePatchDetector();
        final FeatureExtractor<BufferedImage> featureExtractor = WebgazerFeatureExtractor.ofImages(eyePatchDetector,
                paddingPx);
        featureExtractorState.setFeatureExtractorFromImages(featureExtractor);
    }

    @Given("^a monocular feature extractor from eye patches$")
    public void extractorFromEyePatches() {
        final int paddingPx = eyeContextSourceState.getPaddingPx();
        final FeatureExtractor<EyePatches> featureExtractor = WebgazerFeatureExtractor.ofEyePatches(paddingPx);
        featureExtractorState.setFeatureExtractorFromEyePatches(featureExtractor);
    }

    @When("^features are requested$")
    public void requestFeatures() {
        final FeatureExtractor<EyePatches> featureExtractor = featureExtractorState.getFeatureExtractorFromEyePatches();
        final EyeFeatures features = featureExtractor.apply(inputState.getEyePatches().get(0));
        featuresState.setFeatures(features);
    }

    @Then("^there are two feature vectors, one per eye$")
    public void assertNumberOfFeatureVectors() {
        final EyeFeatures features = featuresState.getFeatures();
        assertThat(features).isInstanceOf(EyeFeatures.class);
        assertThat(features.getLeftEyeFeatures()).isNotEmpty();
        assertThat(features.getRightEyeFeatures()).isNotEmpty();
    }

    @Then("^each feature vector includes an eye patch, re-scaled to (\\d+) x (\\d+) pixels$")
    public void assertFeatureVectors(final Integer targetHeight, final Integer targetWidth) {
        final EyeFeatures features = featuresState.getFeatures();
        // TODO: Hm, these eye patches are actually way too small
        // for scaling
        // TODO: These values really don't fit the input
        // TODO: Visualize the results
        assertEyeVector(features.getLeftEyeFeatures(), targetWidth, targetHeight,
                ((BinocularFeatureExtractor) featureExtractorState.getFeatureExtractorFromEyePatches())::getEyeData,
                255.0, 85.0, 157.0, 204.0, 157.0, 85.0, 85.0, 85.0, 157.0, 157.0, 157.0, 85.0, 204.0, 230.0, 85.0, 4.0,
                85.0, 85.0, 157.0, 157.0, 157.0, 85.0, 255.0, 255.0, 85.0, 85.0, 85.0, 85.0, 157.0, 204.0, 157.0, 157.0,
                255.0, 255.0, 157.0, 85.0, 85.0, 157.0, 204.0, 204.0, 204.0, 204.0, 230.0, 255.0, 157.0, 85.0, 85.0,
                157.0, 204.0, 157.0, 230.0, 230.0, 230.0, 230.0, 204.0, 85.0, 85.0, 157.0, 204.0, 204.0);
        assertEyeVector(features.getRightEyeFeatures(), targetWidth, targetHeight,
                ((BinocularFeatureExtractor) featureExtractorState.getFeatureExtractorFromEyePatches())::getEyeData,
                238.0, 191.0, 140.0, 140.0, 72.0, 72.0, 72.0, 140.0, 140.0, 140.0, 217.0, 191.0, 140.0, 72.0, 72.0,
                13.0, 72.0, 140.0, 217.0, 140.0, 191.0, 191.0, 191.0, 140.0, 72.0, 13.0, 72.0, 140.0, 238.0, 140.0,
                191.0, 191.0, 217.0, 140.0, 72.0, 13.0, 72.0, 191.0, 238.0, 191.0, 140.0, 191.0, 238.0, 191.0, 72.0,
                72.0, 72.0, 217.0, 247.0, 247.0, 140.0, 191.0, 217.0, 217.0, 140.0, 72.0, 140.0, 238.0, 255.0, 255.0);
    }

    protected BiFunction<ClmtrackrResult, Function<List<Point2D>, Rectangle[]>, EyePatches> createMockEyePatchesExtractor() {
        final BiFunction<ClmtrackrResult, Function<List<Point2D>, Rectangle[]>, EyePatches> eyePatchesExtractor = new BiFunction<ClmtrackrResult, Function<List<Point2D>, Rectangle[]>, EyePatches>() {
            private Iterator<EyePatches> iterator;

            @Override
            public EyePatches apply(final ClmtrackrResult result,
                    final Function<List<Point2D>, Rectangle[]> boundsFunction) {
                // A new iterator is obtained when the old one is exhausted to
                // be able to traverse the list multiple times, i.e. to generate
                // feature sets for the left and the right eye.
                if (iterator == null || !iterator.hasNext()) {
                    iterator = inputState.getEyePatches().iterator();
                }
                return iterator.next();
            }
        };
        return eyePatchesExtractor;
    }

    protected EyePatchDetector createEyePatchDetector() {
        final ClmtrackrPositionsDetector positionsDetector = new ClmtrackrPositionsDetector(backendState.getBackend());
        // TODO: Make the bounds function configurable
        final EyePatchDetector eyePatchDetector = new EyePatchDetector(positionsDetector,
                BoundsFunctions::getEyeBoundsFromEyeCorners);
        return eyePatchDetector;
    }

    protected void assertEyeVector(final double[] extractedFeatures, final int targetWidth, final int targetHeight,
            final Function<double[], double[]> actualEyeVectorExtractor, final double... expectedEyeVector) {
        final int expectedEyeVectorLength = targetWidth * targetHeight;
        final double[] actualEyeVector = actualEyeVectorExtractor.apply(extractedFeatures);
        assertThat(actualEyeVector).hasSize(expectedEyeVectorLength);
        assertThat(actualEyeVector).containsExactly(expectedEyeVector);
    }
}
