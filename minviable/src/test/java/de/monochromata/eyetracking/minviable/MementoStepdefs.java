package de.monochromata.eyetracking.minviable;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;

import de.monochromata.eyetracking.minviable.state.InputState;
import de.monochromata.eyetracking.minviable.state.ValidationResultState;
import de.monochromata.eyetracking.webcam.calibration.EyeValidationResult;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MementoStepdefs {

    private final InputState eyePatchesState;
    private final ValidationResultState validationResultState;

    public MementoStepdefs(final InputState eyePatchesState, final ValidationResultState validationResultState) {
        this.eyePatchesState = eyePatchesState;
        this.validationResultState = validationResultState;
    }

    @When("^the mementos of the correction models are saved to disk$")
    public void saveCorrectionModelsMementos() {
        saveCorrectionModelMemento(validationResultState.getValidationResult().getLeftEyeResult(), "left");
        saveCorrectionModelMemento(validationResultState.getValidationResult().getLeftEyeResult(), "right");
    }

    @Then("^a file for the memento exists$")
    public void assertMementoFile() {
        assertThat(getMementoFile("left")).exists();
        assertThat(getMementoFile("right")).exists();
    }

    protected void saveCorrectionModelMemento(final EyeValidationResult result, final String eyePosition) {
        try {
            final File mementoFile = createMementoFile(eyePosition);
            try (final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(mementoFile))) {
                oos.writeObject(result.getPredictor().getMemento());
            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private File createMementoFile(final String eyePosition) throws Exception {
        final File mementoFile = getMementoFile(eyePosition);
        mementoFile.createNewFile();
        return mementoFile;
    }

    private File getMementoFile(final String eyePosition) {
        try {
            final String mementoFilename = createMementoFilename(eyePosition);
            final File parentFile = getParentFile();
            parentFile.mkdirs();
            final File mementoFile = new File(parentFile, mementoFilename);
            return mementoFile;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private File getParentFile() throws URISyntaxException {
        final File parentFile = new File(this.getClass().getResource(eyePatchesState.getInputFilename()).toURI())
                .getAbsoluteFile().getParentFile();
        return parentFile;
    }

    private String createMementoFilename(final String eyePosition) {
        final String inputFilename = eyePatchesState.getInputFilename();
        final int lastSlash = inputFilename.lastIndexOf(File.separatorChar);
        final String mementoFilename = (lastSlash == -1 ? inputFilename
                : inputFilename.substring(lastSlash + 1, inputFilename.length())).replace(".csv",
                        "_" + eyePosition + "Predictor.ser");
        return mementoFilename;
    }

}
