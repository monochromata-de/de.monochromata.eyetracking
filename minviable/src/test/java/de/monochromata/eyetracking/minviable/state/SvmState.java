package de.monochromata.eyetracking.minviable.state;

import libsvm.svm_parameter;

public class SvmState {
	
	private svm_parameter hyperParameters;

	public svm_parameter getHyperParameters() {
		return hyperParameters;
	}

	public void setHyperParameters(final svm_parameter hyperParameters) {
		this.hyperParameters = hyperParameters;
	}

}
