package de.monochromata.eyetracking.minviable;

import de.monochromata.eyetracking.minviable.state.SvmState;
import io.cucumber.java.en.Given;
import libsvm.svm_parameter;

public class SvmStepdefs {

    private final SvmState svmState;

    public SvmStepdefs(final SvmState svmState) {
        this.svmState = svmState;
    }

    @Given("^sample RBF hyper-parameters$")
    public void rbfHyperparam() {
        svmState.setHyperParameters(createSampleRBFHyperParameters());
    }

    @Given("^sample linear hyper-parameters$")
    public void linearHyperparam() {
        svmState.setHyperParameters(createSampleLinearHyperParameters());
    }

    @Given("^sample polynomial hyper-parameters$")
    public void polyHyperparam() {
        svmState.setHyperParameters(createSamplePolynomialHyperParameters());
    }

    protected svm_parameter createSampleRBFHyperParameters() {
        final svm_parameter hyperParameters = new svm_parameter();
        hyperParameters.svm_type = svm_parameter.EPSILON_SVR;
        hyperParameters.kernel_type = svm_parameter.RBF;
        hyperParameters.gamma = 1d / 4;
        hyperParameters.cache_size = 100;
        hyperParameters.eps = 1e-3;
        hyperParameters.C = 0.01d;
        return hyperParameters;
    }

    protected svm_parameter createSampleLinearHyperParameters() {
        final svm_parameter hyperParameters = new svm_parameter();
        hyperParameters.svm_type = svm_parameter.EPSILON_SVR;
        hyperParameters.cache_size = 100;
        hyperParameters.eps = 1e-3;
        hyperParameters.C = 0.1;
        return hyperParameters;
    }

    protected svm_parameter createSamplePolynomialHyperParameters() {
        final svm_parameter hyperParameters = new svm_parameter();
        hyperParameters.svm_type = svm_parameter.NU_SVR;
        hyperParameters.kernel_type = svm_parameter.POLY;
        // for polynomial
        hyperParameters.degree = 3;
        hyperParameters.gamma = 0.01;
        hyperParameters.coef0 = 1;
        // for Nu
        hyperParameters.C = 1;
        hyperParameters.nu = 1;
        // technical
        hyperParameters.cache_size = 100;
        hyperParameters.eps = 0.001;
        hyperParameters.shrinking = 1;
        hyperParameters.probability = 0;
        return hyperParameters;
    }

}
