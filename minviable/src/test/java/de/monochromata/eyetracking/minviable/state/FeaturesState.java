package de.monochromata.eyetracking.minviable.state;

import de.monochromata.eyetracking.webcam.features.EyeFeatures;

public class FeaturesState {

	private EyeFeatures features;

	public EyeFeatures getFeatures() {
		return features;
	}

	public void setFeatures(final EyeFeatures features) {
		this.features = features;
	}
	
}
