package de.monochromata.eyetracking.minviable.state;

import de.monochromata.eyetracking.webcam.stubs.NoopWebcam;

public class EyeContextSourceState {

	private NoopWebcam webcam;
	private int trainingSize;
	private int testSize;
	private int eyePatchMinimumHeight = 0;
	private int eyePatchMaximumHeight = Integer.MAX_VALUE;
	private int paddingPx;

	public NoopWebcam getWebcam() {
		return webcam;
	}

	public void setWebcam(final NoopWebcam webcam) {
		this.webcam = webcam;
	}

	public int getTrainingSize() {
		return trainingSize;
	}

	public void setTrainingSize(final int trainingSize) {
		this.trainingSize = trainingSize;
	}

	public int getTestSize() {
		return testSize;
	}

	public void setTestSize(final int testSize) {
		this.testSize = testSize;
	}

	public int getEyePatchMinimumHeight() {
		return eyePatchMinimumHeight;
	}

	public void setEyePatchMinimumHeight(final int eyePatchMinimumHeight) {
		this.eyePatchMinimumHeight = eyePatchMinimumHeight;
	}

	public int getEyePatchMaximumHeight() {
		return eyePatchMaximumHeight;
	}

	public void setEyePatchMaximumHeight(final int eyePatchMaximumHeight) {
		this.eyePatchMaximumHeight = eyePatchMaximumHeight;
	}

	public int getPaddingPx() {
		return paddingPx;
	}

	public void setPaddingPx(final int paddingPx) {
		this.paddingPx = paddingPx;
	}

}
