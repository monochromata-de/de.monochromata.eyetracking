package de.monochromata.eyetracking.minviable.state;

import de.monochromata.eyetracking.minviable.visualization.Visualizer;

public class VisualizerState {

	private Visualizer visualizer;

	public Visualizer getVisualizer() {
		return visualizer;
	}

	public void setVisualizer(Visualizer visualizer) {
		this.visualizer = visualizer;
	}
	
}
