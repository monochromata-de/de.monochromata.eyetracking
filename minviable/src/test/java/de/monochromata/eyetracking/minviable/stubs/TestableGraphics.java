package de.monochromata.eyetracking.minviable.stubs;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class TestableGraphics extends NoopGraphics {

	private final List<Object[]> drawnObjects = new ArrayList<>();
	
	private Color color;
	
	public List<Object[]> getDrawnObjects() {
		return drawnObjects;
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public void setColor(Color c) {
		color = c;
	}

	@Override
	public void fillOval(int x, int y, int width, int height) {
		drawnObjects.add(new Object[] { "fillOval", x, y, width, height, color });
	}

	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {
		drawnObjects.add(new Object[] { "drawLine", x1, y1, x2, y2, color });
	}

}
