package de.monochromata.eyetracking.minviable.state;

import java.awt.geom.Point2D;
import java.util.List;

public class PositionsState {

	private List<Point2D> positions;

	public List<Point2D> getPositions() {
		return positions;
	}

	public void setPositions(List<Point2D> positions) {
		this.positions = positions;
	}

}
