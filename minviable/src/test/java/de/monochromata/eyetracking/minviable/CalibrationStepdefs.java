package de.monochromata.eyetracking.minviable;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.eyetracking.Point;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.state.EyeContextSourceState;
import de.monochromata.eyetracking.minviable.state.FeatureExtractorState;
import de.monochromata.eyetracking.minviable.state.InputState;
import de.monochromata.eyetracking.minviable.state.PositionsState;
import de.monochromata.eyetracking.minviable.state.SvmState;
import de.monochromata.eyetracking.minviable.state.ValidationResultState;
import de.monochromata.eyetracking.webcam.calibration.Calibration;
import de.monochromata.eyetracking.webcam.calibration.ValidationResult;
import de.monochromata.eyetracking.webcam.features.EyeFeatures;
import de.monochromata.eyetracking.webcam.features.FeatureExtractor;
import de.monochromata.eyetracking.webcam.svm.Gaze;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CalibrationStepdefs {

    private final InputState inputState;
    private final SvmState svmState;
    private final EyeContextSourceState eyeContextSourceState;
    private final FeatureExtractorState featureExtractorState;
    private final ValidationResultState validationResult;

    public CalibrationStepdefs(final InputState inputState, final PositionsState positionsState,
            final SvmState svmState, final EyeContextSourceState eyeContextSourceState,
            final FeatureExtractorState featureExtractorState, final ValidationResultState validationResultState) {
        this.inputState = inputState;
        this.svmState = svmState;
        this.eyeContextSourceState = eyeContextSourceState;
        this.featureExtractorState = featureExtractorState;
        validationResult = validationResultState;
    }

    @When("the images and manual contexts are used for calibration")
    public void calibrateWithImagesAndManualContexts() {
        calibrate(this::getEyeDataFromEyePatches, featureExtractorState.getFeatureExtractorFromEyePatches());
    }

    @When("the clmtrackr results and manual contexts are used for calibration")
    public void calibrateWithClmtrackr() {
        calibrate(this::getEyeDataFromClmtrackrResults, featureExtractorState.getFeatureExtractorFromClmtrackrResult());
    }

    @Then("validation yields an error of x={double}px, y={double}px \\(left eye) and x={double}px, y={double}px \\(right eye)")
    public void assertValidation(final Double leftEyeErrorX, final Double leftEyeErrorY, final Double rightEyeErrorX,
            final Double rightEyeErrorY) {
        assertValidationResult(validationResult.getValidationResult(), leftEyeErrorX, leftEyeErrorY, rightEyeErrorX,
                rightEyeErrorY);
    }

    protected <I> void calibrate(final Function<Function<EyeFeatures, double[]>, List<Gaze>> dataAccessFunction,
            final FeatureExtractor<I> featureExtractor) {
        final List<Gaze> leftEyeData = dataAccessFunction.apply(EyeFeatures::getLeftEyeFeatures);
        final List<Gaze> rightEyeData = dataAccessFunction.apply(EyeFeatures::getRightEyeFeatures);
        final Calibration calibration = new Calibration(featureExtractor, svmState.getHyperParameters());
        final ValidationResult result = calibration.train(getTrainingData(leftEyeData), getValidationData(leftEyeData),
                getTrainingData(rightEyeData), getValidationData(rightEyeData));
        validationResult.setValidationResult(result);
    }

    protected List<Gaze> getTrainingData(final List<Gaze> input) {
        return input.subList(0, eyeContextSourceState.getTrainingSize());
    }

    protected List<Gaze> getValidationData(final List<Gaze> input) {
        return input.subList(eyeContextSourceState.getTrainingSize(),
                eyeContextSourceState.getTrainingSize() + eyeContextSourceState.getTestSize());
    }

    protected List<Gaze> getEyeDataFromEyePatches(final Function<EyeFeatures, double[]> featuresAccessor) {
        final FeatureExtractor<EyePatches> featureExtractor = featureExtractorState.getFeatureExtractorFromEyePatches();
        final List<EyePatches> eyePatches = inputState.getEyePatches();
        return createData(featuresAccessor, featureExtractor, eyePatches);
    }

    protected List<Gaze> getEyeDataFromClmtrackrResults(final Function<EyeFeatures, double[]> featuresAccessor) {
        final FeatureExtractor<ClmtrackrResult> featureExtractor = featureExtractorState
                .getFeatureExtractorFromClmtrackrResult();
        final List<ClmtrackrResult> eyePatches = inputState.getClmtrackrResults();
        return createData(featuresAccessor, featureExtractor, eyePatches);
    }

    protected <T> List<Gaze> createData(final Function<EyeFeatures, double[]> featuresAccessor,
            final FeatureExtractor<T> featureExtractor, final List<T> inputData) {
        final List<Point> manualPoints = inputState.getManualPoints();
        return createData(featureExtractor, featuresAccessor, inputData, manualPoints);
    }

    protected static <T> List<Gaze> createData(final FeatureExtractor<T> featureExtractor,
            final Function<EyeFeatures, double[]> featuresAccessor, final List<T> inputData,
            final List<Point> manualPoints) {
        return IntStream.range(0, inputData.size()).mapToObj(i -> {
            final EyeFeatures features = featureExtractor.apply(inputData.get(i));
            final Point point = manualPoints.get(i);
            return new Gaze(featuresAccessor.apply(features), point.getX(), point.getY());
        }).collect(toList());
    }

    protected void assertValidationResult(final ValidationResult result, final double errorLeftX,
            final double errorLeftY, final double errorRightX, final double errorRightY) {
        final String actualError = createErrorsString(result);
        final String expectedError = createErrorsString(errorLeftX, errorLeftY, errorRightX, errorRightY);
        assertThat(actualError).isEqualTo(expectedError);
    }

    protected String createErrorsString(final ValidationResult result) {
        return createErrorsString(result.getLeftEyeResult().getErrorX(), result.getLeftEyeResult().getErrorY(),
                result.getRightEyeResult().getErrorX(), result.getRightEyeResult().getErrorY());
    }

    protected String createErrorsString(final double errorLeftX, final double errorLeftY, final double errorRightX,
            final double errorRightY) {
        return format("errorLeftX=%1$.2f, errorLeftY=%2$.2f, errorRightX=%3$.2f, errorRightY=%4$.2f", errorLeftX,
                errorLeftY, errorRightX, errorRightY);
    }

}
