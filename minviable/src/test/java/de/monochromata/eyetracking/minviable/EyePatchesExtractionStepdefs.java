package de.monochromata.eyetracking.minviable;

import static de.monochromata.eyetracking.minviable.eyes.EyePatchesExtraction.createEyePatches;
import static de.monochromata.eyetracking.minviable.visualization.ImageVisualization.showImageInFrame;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR;
import static java.util.Arrays.stream;
import static org.assertj.core.api.Assertions.assertThat;

import de.monochromata.clmtrackr4j.ClmtrackrResult;
import de.monochromata.clmtrackr4j.ClmtrackrResultTransformation;
import de.monochromata.eyetracking.minviable.eyes.BoundsFunctions;
import de.monochromata.eyetracking.minviable.eyes.EyePatch;
import de.monochromata.eyetracking.minviable.eyes.EyePatches;
import de.monochromata.eyetracking.minviable.state.ImageState;
import de.monochromata.eyetracking.minviable.state.PositionsState;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class EyePatchesExtractionStepdefs {

    private final ImageState imageState;
    private final PositionsState positionsState;
    private double scalingFactor;
    private double rotationRadians;
    private double translationXPx;
    private double translationYPx;
    private EyePatches eyePatches;

    public EyePatchesExtractionStepdefs(final ImageState imageState, final PositionsState positionsState) {
        this.imageState = imageState;
        this.positionsState = positionsState;
    }

    @Given("scalingFactor={double}")
    public void scalingFactor(final Double scalingFactor) {
        this.scalingFactor = scalingFactor;
    }

    @Given("rotationRadians={double}")
    public void rotation(final Double rotationRadians) {
        this.rotationRadians = rotationRadians;
    }

    @Given("translationX={double}px")
    public void translationX(final Double translationXpx) {
        translationXPx = translationXpx;
    }

    @Given("translationY={double}px")
    public void translationY(final Double translationYpx) {
        translationYPx = translationYpx;
    }

    @When("^eye patches are extracted based on pupil position$")
    public void extractWithPupilPosition() {
        final ClmtrackrResult result = createResult();
        eyePatches = createEyePatches(result, BoundsFunctions::getEyeBoundsFromPupilPosition);
    }

    @When("^eye patches are extracted based on eye corners$")
    public void extractWithEyeCorners() {
        final ClmtrackrResult result = createResult();
        eyePatches = createEyePatches(result, BoundsFunctions::getEyeBoundsFromEyeCorners);
    }

    @When("^eye patches are extracted from a translated image based on pupil position$")
    public void extractWithPupilPositionFromTranslatedImage() {
        final ClmtrackrResult result = createResult();
        final ClmtrackrResult transformedResult = transformResult(result);
        eyePatches = createEyePatches(transformedResult, BoundsFunctions::getEyeBoundsFromPupilPosition);
    }

    @When("^eye patches are extracted from a translated image based on eye corners$")
    public void extractWithEyeCornersFromTranslatedImage() {
        final ClmtrackrResult result = createResult();
        final ClmtrackrResult transformedResult = transformResult(result);
        eyePatches = createEyePatches(transformedResult, BoundsFunctions::getEyeBoundsFromEyeCorners);
    }

    @Then("the {word} eye has bounds x={int} y={int} width={int} height={int} and data")
    public void assertPatch(final String eye, final Integer x, final Integer y, final Integer width,
            final Integer height, final String data) {
        assertEyePatch(x, y, width, height, data, getEyePatchFor(eye));
    }

    private ClmtrackrResult transformResult(final ClmtrackrResult result) {
        final long startMs = System.currentTimeMillis();
        final ClmtrackrResult transformedResult = ClmtrackrResultTransformation.transformResult(result, 3.0d,
                VALUE_INTERPOLATION_BILINEAR);
        final long endMs = System.currentTimeMillis();
        // TODO: Remove debug code
        System.err.println("Transformation took " + (endMs - startMs) + "ms");
        showImageInFrame(transformedResult.getImage(), 30);
        return transformedResult;
    }

    private ClmtrackrResult createResult() {
        final ClmtrackrResult result = new ClmtrackrResult(imageState.getNextImageInWhenPhase(),
                positionsState.getPositions(), 1, 0.0, 0.0, scalingFactor, rotationRadians, translationXPx,
                translationYPx);
        return result;
    }

    private void assertEyePatch(final Integer x, final Integer y, final Integer width, final Integer height,
            final String data, final EyePatch eyePatch) {
        final String actualBounds = "x=" + eyePatch.getX() + " y=" + eyePatch.getY() + " width=" + eyePatch.getWidth()
                + " height=" + eyePatch.getHeight();
        final String expectedBounds = "x=" + x + " y=" + y + " width=" + width + " height=" + height;
        assertThat(actualBounds).isEqualTo(expectedBounds);
        assertThat(eyePatch.getData()).isEqualTo(parseData(data));
    }

    private double[] parseData(final String data) {
        return stream(data.trim().split("\\s+")).mapToDouble(Double::parseDouble).toArray();
    }

    private EyePatch getEyePatchFor(final String eye) {
        switch (eye) {
        case "left":
            return eyePatches.getLeftPatch();
        case "right":
            return eyePatches.getRightPatch();
        default:
            throw new IllegalArgumentException("Invalid eye: " + eye);
        }
    }

}
