# A minimum viable webcam-based eye tracker for Java

[![pipeline status](https://gitlab.com/monochromata-de/de.monochromata.eyetracking/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/de.monochromata.eyetracking/commits/master)

Using a webcam, this project provides the most easy access to eye-tracking possible,
at low frequency and with low precision and low accuracy, though. The project
is devoted to continuous improvement of the quality of the tracker and shall
motivate a search for applications of ubiquitous low-quality eye tracking.

There are three parts:
* *minviable*: the minimum-viable eyetracker based on [auduno/clmtrackr](https://github.com/auduno/clmtrackr), drawing on ideas of [brownhci/WebGazer](https://github.com/brownhci/WebGazer) and other webcam-based eye trackers
* *webcam*: a minimal API for webcam access, with a default implementation based on [sarxos/webcam-capture](https://github.com/sarxos/webcam-capture/)
* *api*: a minimal eye-tracking API

## Resources

* It is available from a Maven repository at https://monochromata-de.gitlab.io/de.monochromata.eyetracking/m2/ .
* The Maven site is available at https://monochromata-de.gitlab.io/de.monochromata.eyetracking/ .
* An Eclipse p2 repository is available at https://monochromata-de.gitlab.io/de.monochromata.eyetracking/p2repo .

# Links

The project is used by
[de.monochromata.eclipse.eyetracking](https://gitlab.com/monochromata-de/de.monochromata.eclipse.eyetracking/). 

## License

LGPL