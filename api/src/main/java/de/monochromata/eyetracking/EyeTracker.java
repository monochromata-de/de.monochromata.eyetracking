package de.monochromata.eyetracking;

import java.util.function.Consumer;

import de.monochromata.event.Tracker;

public interface EyeTracker extends Tracker<Consumer<Point>> {

}
