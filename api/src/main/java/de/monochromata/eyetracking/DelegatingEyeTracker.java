package de.monochromata.eyetracking;

import java.util.function.Consumer;

import de.monochromata.event.AbstractTracker;

public class DelegatingEyeTracker extends AbstractTracker<Consumer<Point>> implements EyeTracker {

	private final Consumer<Point> forwardingListener = this::forward;
	private EyeTracker delegate;
	
	public EyeTracker getDelegate() {
		return delegate;
	}

	/**
	 * Applies the given delegate, replacing any previously registered delegate.
	 * <p>
	 * TODO: Secure with a permission check?
	 */
	public void setDelegate(final EyeTracker delegate) {
		if(this.delegate != null) {
			this.delegate.removeListener(forwardingListener);
		}
		this.delegate = delegate;
		if(delegate != null) {
			delegate.addListener(forwardingListener);
		}
	}
	
	protected void forward(final Point point) {
		notifyListeners(listener -> listener.accept(point));
	}

}
