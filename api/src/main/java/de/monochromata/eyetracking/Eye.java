package de.monochromata.eyetracking;

public enum Eye {
	LEFT, RIGHT
}