/**
 * Means of emitting events on screen updates and of re-constructing a model of
 * screen contents from these events.
 */
package de.monochromata.eyetracking.screen;