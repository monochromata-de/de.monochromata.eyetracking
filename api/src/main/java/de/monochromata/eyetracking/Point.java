package de.monochromata.eyetracking;

import java.io.Serializable;

public interface Point extends Serializable {

	public int getX();
	public int getY();
	
}
