package de.monochromata.eyetracking;

public interface ControllableEyeTracker extends EyeTracker {

	/**
	 * Sets this tracker as the selected one among a set of different trackers.
	 * Multiple subsequent invocations of this method using the same value have
	 * no effect.
	 * 
	 * @throws IllegalStateException
	 *             If {@link #getSelected()} and {@link #getTracking()} return
	 *             {@literal true} and {@literal false} is supplied to this
	 *             method. Tracking needs to be stopped before
	 */
	void setSelected(boolean selected);

	boolean getSelected();

	
	/**
	 * Starts/stops eye tracking. Multiple subsequent invocations of this method
	 * using the same value have no effect.
	 * 
	 * @throws IllegalStateException
	 *             If {@link #getSelected()} returns {@literal false} and
	 *             {@literal} true} is passed to this method.
	 */
	void setTracking(boolean track);

	boolean getTracking();
	
}
