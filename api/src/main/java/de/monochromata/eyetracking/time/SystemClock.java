package de.monochromata.eyetracking.time;

/**
 * Uses {@link System#currentTimeMillis()} to obtain the current time.
 */
public class SystemClock implements Clock {

	@Override
	public long currentTimeMillis() {
		return System.currentTimeMillis();
	}

}
