package de.monochromata.eyetracking.time;

/**
 * Abstracting a clock that returns the current time in milliseconds.
 */
@FunctionalInterface
public interface Clock {

	/**
	 * @return the current time in milliseconds, as per
	 *         {@link System#currentTimeMillis()}.
	 */
	public long currentTimeMillis();

}
